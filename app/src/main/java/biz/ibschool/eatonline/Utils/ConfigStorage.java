package biz.ibschool.eatonline.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import biz.ibschool.eatonline.Models.ApiSettings;


public class ConfigStorage {

    private static final String PREFS_NAME = "EATonlinePreferences";

    private static final String OPERATOR_PHONE = "operator_phone";
    private static final String OWNER_PHONE = "owner_phone";
    private static final String HOTLINE_PHONE = "hotline_phone";

    private SharedPreferences settings;

    public ConfigStorage(Context context) {
        this.settings = context.getSharedPreferences(PREFS_NAME, 0);
    }

    public void saveSettings(ApiSettings apiSettings) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(OPERATOR_PHONE, apiSettings.getOperator());
        editor.putString(OWNER_PHONE, apiSettings.getOwner());
        editor.putString(HOTLINE_PHONE, apiSettings.getHotline());
        editor.commit();
    }

    public String getOperatorPhone() {
        return settings.getString(OPERATOR_PHONE, "");
    }

    public String getOwnerPhone() {
        return settings.getString(OWNER_PHONE, "");
    }
}
