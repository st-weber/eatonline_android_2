package biz.ibschool.eatonline;

import android.util.Log;

import biz.ibschool.eatonline.Interfaces.ApiInterface;
import biz.ibschool.eatonline.Interfaces.Const;
import biz.ibschool.eatonline.Interfaces.LoadingTextInterface;
import biz.ibschool.eatonline.Models.ApiSettings;
import biz.ibschool.eatonline.Models.AppPageText;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiInteractor {

    private Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(Const.NEW_API_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    private ApiInterface apiInterface = retrofit.create(ApiInterface.class);
    private Realm realm;
    private DataManager dataManager = new DataManager();

    public ApiInteractor() {
        Realm.init(EatApplication.appContext);
        realm = Realm.getDefaultInstance();
    }

    public void loadSettings(final DataManager.RequestInterface requestInterface) {
        apiInterface.loadSettings().enqueue(new Callback<ApiSettings>() {
            @Override
            public void onResponse(Call<ApiSettings> call, Response<ApiSettings> response) {
                EatApplication.configStorage.saveSettings(response.body());
                requestInterface.onSuccess();
            }

            @Override
            public void onFailure(Call<ApiSettings> call, Throwable t) {
                requestInterface.onFailed();
            }
        });
    }

    public void loadPageText(String pageCode, final LoadingTextInterface loadingTextInterface) {
        if (pageCode.equals("about")) {
            apiInterface.loadAboutText().enqueue(new Callback<AppPageText>() {
                @Override
                public void onResponse(Call<AppPageText> call, Response<AppPageText> response) {



                    loadingTextInterface.onLoaded(response.body().getText());
                }
                @Override
                public void onFailure(Call<AppPageText> call, Throwable t) {
                    loadingTextInterface.onLoaded(t.toString());
                }
            });
        } else if (pageCode.equals("agreement")) {
            System.out.println("agreement");
            Log.e("agreement", "agreement");

            apiInterface.loadAgreementText().enqueue(new Callback<AppPageText>() {
                @Override
                public void onResponse(Call<AppPageText> call, Response<AppPageText> response) {

                    System.out.println(response.body());

                    loadingTextInterface.onLoaded(response.body().getText());
                }

                @Override
                public void onFailure(Call<AppPageText> call, Throwable t) {
                    loadingTextInterface.onLoaded(t.toString());
                }
            });
        } else if (pageCode.equals("partnership")) {
            apiInterface.loadPartnershipText().enqueue(new Callback<AppPageText>() {
                @Override
                public void onResponse(Call<AppPageText> call, Response<AppPageText> response) {
                    loadingTextInterface.onLoaded(response.body().getText());
                }

                @Override
                public void onFailure(Call<AppPageText> call, Throwable t) {
                    loadingTextInterface.onLoaded(t.toString());
                }
            });
        } else {
            Log.e("empty code", pageCode);
        }
    }

}
