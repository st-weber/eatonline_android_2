package biz.ibschool.eatonline;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;

/**
 * Created by michanic on 14.10.17.
 */

public class CookieStore implements CookieJar {

    private final HashMap<String, List<Cookie>> _cookieStore = new HashMap<>();

    @Override
    public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {

        List<Cookie> currentCookies = _cookieStore.get(url.host());
        List<Cookie> responseCookies = new ArrayList(cookies);

        if (currentCookies != null && responseCookies != null
                && responseCookies.size() > 0 && currentCookies.size() > 0) {
            for (Cookie currentCookie : currentCookies) {

                if (!responseCookies.contains(currentCookie) && (currentCookie.value()!=null && currentCookie.value()!="")) {
                    responseCookies.add(currentCookie);
                }
            }
        }

        _cookieStore.put(url.host(), responseCookies);
    }

    @Override
    public List<Cookie> loadForRequest(HttpUrl url) {

        List<Cookie> cookies = _cookieStore.get(url.host());
        List<Cookie> requestCookies = new ArrayList<>();

        if (cookies != null && cookies.size() > 0) {
            for (Cookie currentCookie : cookies) {
                if ((currentCookie.value()!=null && currentCookie.value()!="")) {
                    requestCookies.add(currentCookie);
                }
            }
        }

        return requestCookies != null ? requestCookies : new ArrayList<Cookie>();
    }

}
