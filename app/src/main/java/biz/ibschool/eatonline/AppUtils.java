package biz.ibschool.eatonline;

import android.content.Context;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import biz.ibschool.eatonline.Models.City;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import okhttp3.OkHttpClient;
import ru.tinkoff.decoro.Mask;
import ru.tinkoff.decoro.MaskImpl;
import ru.tinkoff.decoro.parser.UnderscoreDigitSlotsParser;
import ru.tinkoff.decoro.slots.PredefinedSlots;
import ru.tinkoff.decoro.slots.Slot;
import ru.tinkoff.decoro.watchers.FormatWatcher;
import ru.tinkoff.decoro.watchers.MaskFormatWatcher;

public class AppUtils {

    private float densityFactor = 1;
    public static final Point displaySize = new Point();
    private static final DisplayMetrics displayMetrics = new DisplayMetrics();

    public OkHttpClient buildOkHttpClient() {
        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
        return httpClientBuilder.cookieJar(new CookieStore()).build();
    }

    public void setSpinner(Context context, final Spinner spinner, String district) {

        ArrayList<String> areas = getAreas();

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, areas);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(arrayAdapter);
        spinner.setPrompt("Title");
        if (district != null) {
            for (int i = 0; i < arrayAdapter.getCount(); i++) {
                if (district.equals(arrayAdapter.getItem(i))) {
                    spinner.setSelection(i);
                    break;
                }
            }
        } else
            spinner.setSelection(0);
    }

    private ArrayList<String> getAreas() {

        char[] data = EatApplication.realm.where(City.class).equalTo("id", EatApplication.dataManager.getSettings().getCurrentCity().getId()).findFirst().getAreas().toCharArray();
        ArrayList<String> areas = new ArrayList<String>();
        int startStr = 0;
        for (int i = 0; i <= data.length - 1; i++) {
            if (data[i] == '|') {
                areas.add(areas.size(), new String(data, startStr, i - startStr));
                startStr = i + 1;
                i++;
            } else if (data.length - 1 == i) {
                areas.add(areas.size(), new String(data, startStr, i - startStr + 1));
            }
        }
        return areas;
    }

    public void getDisplaySizeAndDensity() {
        try {
            WindowManager manager = (WindowManager) EatApplication.appContext.getSystemService(Context.WINDOW_SERVICE);
            Display display = manager.getDefaultDisplay();
            if (display != null) {
                display.getSize(displaySize);
                display.getMetrics(displayMetrics);
                densityFactor = displayMetrics.density;
            }
        } catch (Exception e) {
            EatApplication.dataManager.sendError("getDisplaySizeAndDensity Exception: " + e.getMessage());
        }
    }

    public int pixelsToDp(float pixels) {
        return (int) Math.abs(pixels / densityFactor);
    }

    public int dpToPixels(int dp) {
        return (int) Math.abs(dp * densityFactor);
    }


    public FormatWatcher setPhoneMask(EditText phone) {

        MaskImpl mask = MaskImpl.createTerminated(PredefinedSlots.RUS_PHONE_NUMBER);
        FormatWatcher formatWatcher = new MaskFormatWatcher(mask);
        formatWatcher.installOn(phone);
        return formatWatcher;

    }

    public FormatWatcher setTimeMask(EditText time) {

        Slot[] slots = {
                PredefinedSlots.digit(),
                PredefinedSlots.digit(),
                PredefinedSlots.hardcodedSlot(':').withTags(Slot.RULES_DEFAULT),
                PredefinedSlots.digit(),
                PredefinedSlots.digit(),
        };
        FormatWatcher formatWatcher = new MaskFormatWatcher(
                MaskImpl.createTerminated(slots)
        );
        formatWatcher.installOn(time);
        return formatWatcher;

    }

    public String getPhoneFormattedString(String phone) {
        Mask inputMask = MaskImpl.createTerminated(PredefinedSlots.RUS_PHONE_NUMBER);
        inputMask.insertFront(phone);
        return inputMask.toString();
    }

    public String getPhoneUnformatted(String phone) {
        Mask inputMask = MaskImpl.createTerminated(PredefinedSlots.RUS_PHONE_NUMBER);
        inputMask.insertFront(phone);
        return inputMask.toUnformattedString();
    }

    public FormatWatcher setVerificationCode(EditText code) {
        Slot[] slots = new UnderscoreDigitSlotsParser().parseSlots("____");
        FormatWatcher formatWatcher = new MaskFormatWatcher(
                MaskImpl.createTerminated(slots)
        );
        formatWatcher.installOn(code);
        return formatWatcher;
    }

    public String getCurrentTimeFormatted(String format) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        String strDate = sdf.format(c.getTime());
        return strDate;
    }

}
