package biz.ibschool.eatonline;

import android.app.Application;
import android.content.Context;

import biz.ibschool.eatonline.Utils.ConfigStorage;
import io.realm.Realm;

public class EatApplication extends Application {

    public static volatile Realm realm;
    public static volatile DataManager dataManager;
    public static volatile AppUtils appUtils;
    public static volatile Context appContext;
    public static ConfigStorage configStorage;

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        realm = Realm.getDefaultInstance();
        appContext = getApplicationContext();
        appUtils = new AppUtils();
        appUtils.getDisplaySizeAndDensity();
        dataManager = new DataManager();
        configStorage = new ConfigStorage(appContext);

    }

    public static void setAppContext(Context appContext) {
        EatApplication.appContext = appContext;
    }
}
