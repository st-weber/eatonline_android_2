package biz.ibschool.eatonline.Interfaces;

import biz.ibschool.eatonline.Models.Action;

/**
 * Created by michanic on 08.09.17.
 */

public interface PlacePageActions {

    public void serviceChanged(int newService);

    public void showActionPressed(Action action);

    public void chooseTablePressed();

}
