package biz.ibschool.eatonline.Interfaces;

/**
 * Created by michanic on 14.04.17.
 */

public interface Const {

    String DOMAIN_NAME = "https://eatonline.ru";
    String API_URL = "https://eatonline.ru/mapi_2/";
    String NEW_API_URL = DOMAIN_NAME + "/api/";
    //String HOTLINE_PHONE = "88002009113";
    //String CONTROL_PHONE = "+79189944344";
    String PRICE_UNITS = "р.";
    String BY_CITY = " (по городу)";
}
