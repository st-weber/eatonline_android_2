package biz.ibschool.eatonline.Interfaces;


public interface OrderRequests {

    public void requestCheckingCodeResult(String resendDelay, String resendCurrent);

    public void checkCodeResult(String result);

}
