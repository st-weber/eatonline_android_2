package biz.ibschool.eatonline.Interfaces;

import biz.ibschool.eatonline.Models.GalleryImage;

import java.util.ArrayList;

public interface PhotoLoading {

    public void photoLoaded(ArrayList<GalleryImage> photo);

}
