package biz.ibschool.eatonline.Interfaces;
import biz.ibschool.eatonline.Models.Order;

/**
 * Created by michanic on 13.09.17.
 */

public interface OrderActions {

    public void checkoutCurrentOrder();

    public void orderStatusChecked();

    public void repeatOrder(Order order);

    public void orderDeleted();

}
