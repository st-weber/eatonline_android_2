package biz.ibschool.eatonline.Interfaces;

import biz.ibschool.eatonline.Models.Dish;
import biz.ibschool.eatonline.Models.Place;

/**
 * Created by michanic on 31.08.17.
 */

public interface FavouritePressed {

    public void switchPlaceFavourite(Place place);

    public void switchDishFavourite(Dish dish);

}
