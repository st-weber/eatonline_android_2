package biz.ibschool.eatonline.Interfaces;

import biz.ibschool.eatonline.Models.Action;
import biz.ibschool.eatonline.Models.ApiSettings;
import biz.ibschool.eatonline.Models.AppPageText;
import biz.ibschool.eatonline.Models.City;
import biz.ibschool.eatonline.Models.Dish;
import biz.ibschool.eatonline.Models.DishType;
import biz.ibschool.eatonline.Models.GalleryImage;
import biz.ibschool.eatonline.Models.OrderStatus;
import biz.ibschool.eatonline.Models.Place;
import biz.ibschool.eatonline.Models.RequestCodeResult;
import biz.ibschool.eatonline.Models.RequestResult;
import biz.ibschool.eatonline.Models.Table;

import java.util.List;

import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {

    @POST("locations.php")
    Call<RealmList<City>> loadCitiesJSON();

    @POST("dishes.php?req=types")
    Call<List<DishType>> loadDishTypesJSON();

    @POST("places.php?req=list")
    Call<List<Place>> loadPlacesListOfCityJSON(@Query("location") String cityID);

    @POST("actions.php")
    Call<List<Action>> loadActionsOfCityJSON(@Query("location") String cityID);

    @POST("dishes.php?req=list")
    Call<List<Dish>> loadDishesOfPlaceJSON(@Query("place") String placeID);

    @POST("tables.php")
    Call<List<Table>> loadTablesOfPlaceJSON(@Query("place") String placeID);

    @POST("places.php?req=gallery")
    Call<List<GalleryImage>> loadPhotoOfPlaceJSON(@Query("place") String placeID);

    // TEMP: debug=true
    @POST("orders.php?req=sent_code&debug=false")
    Call<RequestCodeResult> requestCheckingCodeJSON(@Query("phone") String phone);

    @POST("orders.php?req=check_code")
    Call<RequestResult> checkCodeJSON(@Query("phone") String phone, @Query("code") String code);

    @POST("orders.php?req=create&device=android")
    Call<RequestResult> checkoutOrder(@Query("token") String token,
                                      @Query("phone") String phone,
                                      @Query("place") String place,
                                      @Query("dishes") String dishes,
                                      @Query("counts") String counts,
                                      @Query("table") String table,
                                      @Query("time") String time,
                                      @Query("payment") String payment,
                                      @Query("name") String name,
                                      @Query("address") String address,
                                      @Query("comments") String comments,
                                      @Query("persons") String persons,
                                      @Query("change") String change,
                                      @Query("type") String type,
                                      @Query("deviceToken") String deviceToken);

    @POST("to_developers.php?device=android")
    Call<RequestResult> sendMessageTodevelopersJSON(@Query("contact") String contact, @Query("text") String text);

    @POST("orders.php?req=check_status")
    Call<OrderStatus> checkOrderStatus(@Query("order") String orderNumber);

    @POST("push.php?device=android")
    Call<RequestResult> sentToken(@Query("token") String token, @Query("action") String action, @Query("location") String location);

    @POST("errors.php?device=android")
    Call<RequestResult> sendError(@Query("text") String text);


    @GET("config.php?type=settings")
    Call<ApiSettings> loadSettings();

    @GET("config.php?type=about")
    Call<AppPageText> loadAboutText();

    @GET("config.php?type=agreement")
    Call<AppPageText> loadAgreementText();

    @GET("config.php?type=partnership")
    Call<AppPageText> loadPartnershipText();

}