package biz.ibschool.eatonline.Interfaces;

import biz.ibschool.eatonline.Models.Action;
import biz.ibschool.eatonline.Models.GalleryImage;

/**
 * Created by michanic on 14.09.17.
 */

public interface GalleryActions {

    public void actionPressed(Action action);

    public void interriorPressed(GalleryImage image);

}
