package biz.ibschool.eatonline.Interfaces;

import biz.ibschool.eatonline.Models.Dish;
import biz.ibschool.eatonline.Models.Table;

public interface DishActions {

    public void addPressed(Dish dish);

    public void plusPressed(Dish dish);

    public void minusPressed(Dish dish);

    public void deletePressed(Dish dish);

    public void dishDetailPressed(Dish dish);

    public void deletePressed(Table table);

}
