package biz.ibschool.eatonline.Enums;

public enum TabFragmentType {

    ACTIONS_LIST,
    PLACES_LIST,
    DISH_TYPES_LIST,
    FAVOURITE_PLACES_LIST,
    FAVOURITE_DISHES_LIST;

}
