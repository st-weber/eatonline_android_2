package biz.ibschool.eatonline.Fragments;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import biz.ibschool.eatonline.EatApplication;
import biz.ibschool.eatonline.Interfaces.Const;
import biz.ibschool.eatonline.Models.Order;
import biz.ibschool.eatonline.Models.Table;
import biz.ibschool.eatonline.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class DetailTableFragment extends Fragment {

    private Table table;
    private TextView orderButton;
    private TextView reservedView;
    private boolean reserved;
    private boolean placeClosed;
    public void setTable(Table table) {
        this.table = table;
    }

    public void setPlaceClosed (boolean placeClosed) {
        this.placeClosed = placeClosed;
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {

        View tableView = inflater.inflate(R.layout.detail_table_fragment, container, false);

        final ProgressBar imageProgress = (ProgressBar) tableView.findViewById(R.id.table_image_progress);
        ImageView tableImageView = (ImageView) tableView.findViewById(R.id.detail_table_image);
        TextView titleView = (TextView) tableView.findViewById(R.id.detail_table_title);
        TextView priceView = (TextView) tableView.findViewById(R.id.detail_table_price);
        orderButton = (TextView) tableView.findViewById(R.id.detail_table_order);
        reservedView = (TextView) tableView.findViewById(R.id.reserved);

        imageProgress.getIndeterminateDrawable().setColorFilter(
                Color.WHITE, android.graphics.PorterDuff.Mode.SRC_IN);

        imageProgress.setVisibility(View.VISIBLE);
        Picasso.with(getContext())
                .load(Const.DOMAIN_NAME + table.getImg())
                .into(tableImageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        imageProgress.setVisibility(View.INVISIBLE);
                    }
                    @Override
                    public void onError() {
                        imageProgress.setVisibility(View.INVISIBLE);
                    }
                });

        titleView.setText(table.getName());
        priceView.setText(table.getPrice() + " " + Const.PRICE_UNITS);
        Typeface dukeTypeface = Typeface.createFromAsset(getContext().getAssets(), "duke.ttf");
        if (dukeTypeface != null) {
            priceView.setTypeface(dukeTypeface);
        }

        checkReserved();

        orderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (reserved) {
                    EatApplication.dataManager.setTableToCurrentOrder(getContext(), null);
                } else {
                    EatApplication.dataManager.setTableToCurrentOrder(getContext(), table);
                }
            }
        });

        return tableView;
    }

    public void checkReserved() {
        if (orderButton != null) {

            Order currentOrder = EatApplication.dataManager.getCurrentOrder();
            if (currentOrder.getTable() != null) {
                if (currentOrder.getTable().getId() == table.getId()) {
                    reserved = true;
                    orderButton.setText("ОТМЕНИТЬ");
                    reservedView.setText("ЗАБРОНИРОВАН");
                    reservedView.setVisibility(View.VISIBLE);
                } else {
                    reserved = false;
                    orderButton.setText("ЗАБРОНИРОВАТЬ");
                    reservedView.setVisibility(View.GONE);
                }
            } else {
                reserved = false;
                orderButton.setText("ЗАБРОНИРОВАТЬ");
                reservedView.setVisibility(View.GONE);
            }
        }

        if (placeClosed) {
            reservedView.setText("ЗАВЕДЕНИЕ ЗАКРЫТО");
            reservedView.setVisibility(View.VISIBLE);
            orderButton.setVisibility(View.GONE);
        }

    }

}
