package biz.ibschool.eatonline.Fragments;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import biz.ibschool.eatonline.EatApplication;
import biz.ibschool.eatonline.Interfaces.Const;
import biz.ibschool.eatonline.Models.Dish;
import biz.ibschool.eatonline.Models.Order;
import biz.ibschool.eatonline.Models.OrderAmount;
import biz.ibschool.eatonline.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class DetailDishFragment extends Fragment {

    private Dish dish;
    private ImageView favouriteButton;
    private TextView orderButton;
    private TextView plusButton;
    private TextView minusButton;
    private int orderType;
    private boolean placeClosed;
    public void setDish(Dish dish) {
        this.dish = dish;
    }
    public void setOrderType(int orderType) {
        this.orderType = orderType;
    }

    public void setPlaceClosed(boolean placeClosed) {
        this.placeClosed = placeClosed;
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {

        View dishView = inflater.inflate(R.layout.detail_dish_fragment, container, false);

        final ProgressBar imageProgress = (ProgressBar) dishView.findViewById(R.id.dish_image_progress);
        ImageView dishImageView = (ImageView) dishView.findViewById(R.id.detail_dish_image);
        TextView titleView = (TextView) dishView.findViewById(R.id.detail_dish_title);
        TextView priceView = (TextView) dishView.findViewById(R.id.detail_dish_price);
        TextView weightView = (TextView) dishView.findViewById(R.id.detail_dish_weight);
        TextView detailWeightView = (TextView) dishView.findViewById(R.id.detail_dish_detweight);
        TextView descriptionView = (TextView) dishView.findViewById(R.id.detail_dish_description);
        favouriteButton = (ImageView) dishView.findViewById(R.id.dish_to_favourite_button);
        orderButton = (TextView) dishView.findViewById(R.id.detail_dish_order);
        plusButton = (TextView) dishView.findViewById(R.id.button_plus);
        minusButton = (TextView) dishView.findViewById(R.id.button_minus);
        TextView serviceView = (TextView) dishView.findViewById(R.id.wrong_service);

        imageProgress.getIndeterminateDrawable().setColorFilter(
                Color.WHITE, android.graphics.PorterDuff.Mode.SRC_IN);

        imageProgress.setVisibility(View.VISIBLE);
        Picasso.with(getContext())
                .load(Const.DOMAIN_NAME + dish.getBimg())
                .into(dishImageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        imageProgress.setVisibility(View.INVISIBLE);
                    }
                    @Override
                    public void onError() {
                        imageProgress.setVisibility(View.INVISIBLE);
                    }
                });

        titleView.setText(dish.getTitle());
        priceView.setText(dish.getPrice() + " " + Const.PRICE_UNITS);
        Typeface dukeTypeface = Typeface.createFromAsset(getContext().getAssets(), "duke.ttf");
        if (dukeTypeface != null) {
            priceView.setTypeface(dukeTypeface);
        }

        weightView.setText(String.valueOf((int) dish.getWeight() + " " + dish.getUnits()));
        detailWeightView.setText(dish.getDweight());
        descriptionView.setText(dish.getDescr());

        checkFavourite();

        favouriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EatApplication.dataManager.setDishToFavourite(dish, !dish.isFavourite());
                checkFavourite();
            }
        });

        orderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Order currentOrder = EatApplication.dataManager.getCurrentOrder();
                if (currentOrder.getAmounts().size() == 0) {
                    currentOrder.setType(orderType);
                }
                orderButton.setClickable(false);
                EatApplication.dataManager.currentOrderChangeDishAmount(dish, 1, getContext());

            }
        });
        plusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                plusButton.setClickable(false);
                EatApplication.dataManager.currentOrderChangeDishAmount(dish, EatApplication.dataManager.getCurrentOrderAmountForDish(dish) + 1, getContext());
            }
        });
        minusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                minusButton.setClickable(false);
                EatApplication.dataManager.currentOrderChangeDishAmount(dish, EatApplication.dataManager.getCurrentOrderAmountForDish(dish) - 1, getContext());
            }
        });

        boolean orderAllowed = true;
        switch (orderType) {
            case 0:
                orderAllowed = dish.getDel().equals("1");
                if (!orderAllowed) {
                    serviceView.setText("НЕ ДОСТАВЛЯЕТСЯ");
                }
                break;
            case 1:
                orderAllowed = dish.getTkw().equals("1");
                if (!orderAllowed) {
                    serviceView.setText("НЕ ПОДАЕТСЯ");
                }
                break;
            case 2:
                orderAllowed = dish.getRes().equals("1");
                if (!orderAllowed) {
                    serviceView.setText("НЕ ПОДАЕТСЯ");
                }
                break;
            default:
                orderAllowed = true;
                break;
        }


        if (placeClosed) {
            serviceView.setText("ЗАВЕДЕНИЕ ЗАКРЫТО");
        }

        if (placeClosed || !orderAllowed) {
            serviceView.setVisibility(View.VISIBLE);
            orderButton.setVisibility(View.GONE);
            plusButton.setVisibility(View.GONE);
            minusButton.setVisibility(View.GONE);
        } else {
            serviceView.setVisibility(View.GONE);
            orderButton.setVisibility(View.VISIBLE);
            checkCount();
        }

        return dishView;
    }

    private void checkFavourite() {
        if (dish.isFavourite()) {
            favouriteButton.setImageResource(R.drawable.ic_favourite_enabled);
        } else {
            favouriteButton.setImageResource(R.drawable.ic_favourite_disabled);
        }
    }

    public void checkCount() {

        Order currentOrder = EatApplication.dataManager.getCurrentOrder();

        if (currentOrder.getPlace() != null && currentOrder.getPlace().getId() == dish.getPlace().getId()) {

            OrderAmount dishAmount = currentOrder.getAmounts().where().equalTo("dish.id", dish.getId()).findFirst();

            if (dishAmount == null) {
                orderButton.setText("ЗАКАЗАТЬ");
                orderButton.setClickable(true);
                plusButton.setVisibility(View.GONE);
                minusButton.setVisibility(View.GONE);
            } else {
                orderButton.setText(dishAmount.getAmount() + " шт.");
                orderButton.setClickable(false);
                plusButton.setVisibility(View.VISIBLE);
                minusButton.setVisibility(View.VISIBLE);
                plusButton.setClickable(true);
                minusButton.setClickable(true);
            }

        } else {
            orderButton.setText("ЗАКАЗАТЬ");
            orderButton.setClickable(true);
            plusButton.setVisibility(View.GONE);
            minusButton.setVisibility(View.GONE);
        }

    }

}
