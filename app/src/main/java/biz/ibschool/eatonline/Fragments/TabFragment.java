package biz.ibschool.eatonline.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import biz.ibschool.eatonline.Activities.DetailImageActivity;
import biz.ibschool.eatonline.Activities.DishTypePlaces;
import biz.ibschool.eatonline.Activities.PlaceActivity;
import biz.ibschool.eatonline.Adapters.BaseAdapter.DishTypesListAdapter;
import biz.ibschool.eatonline.Adapters.BaseAdapter.PlacesListAdapter;
import biz.ibschool.eatonline.Adapters.BaseAdapter.SmallDishesListAdapter;
import biz.ibschool.eatonline.Adapters.BillRecyclerViewAdapter;
import biz.ibschool.eatonline.EatApplication;
import biz.ibschool.eatonline.Enums.TabFragmentType;
import biz.ibschool.eatonline.Interfaces.FavouritePressed;
import biz.ibschool.eatonline.Interfaces.GalleryActions;
import biz.ibschool.eatonline.Models.Action;
import biz.ibschool.eatonline.Models.Dish;
import biz.ibschool.eatonline.Models.DishType;
import biz.ibschool.eatonline.Models.GalleryImage;
import biz.ibschool.eatonline.Models.Place;
import biz.ibschool.eatonline.R;

import java.util.ArrayList;

public class TabFragment extends Fragment implements FavouritePressed, GalleryActions {

    public volatile String tabName = "";
    private TabFragmentType type;

    private ListView listView;
    private RecyclerView recyclerView;
    private PlacesListAdapter placesListAdapter;
    private SmallDishesListAdapter dishesListAdapter;

    private StaggeredGridLayoutManager staggeredGridLayoutManager;

    public TabFragment() {}

    public static TabFragment newInstance(TabFragmentType type) {
        TabFragment fragment = new TabFragment();
        fragment.type = type;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.tab_fragment, container, false);
        listView = (ListView) rootView.findViewById(R.id.tab_list_view);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.tab_recycler_view);
        //recyclerView.setHasFixedSize(true);

        staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);

        final ArrayList arrayList;
        BillRecyclerViewAdapter billRecyclerViewAdapter;
        DishTypesListAdapter dishTypesAdapter;

        if (type != null) {

            switch (type) {
                case ACTIONS_LIST:
                    arrayList = new ArrayList(EatApplication.realm.where(Action.class).findAll());
                    listView.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    billRecyclerViewAdapter = new BillRecyclerViewAdapter(arrayList, getContext(), this);
                    recyclerView.setAdapter(billRecyclerViewAdapter);

                    break;

                case PLACES_LIST:
                    arrayList = new ArrayList(EatApplication.realm.where(Place.class).findAll().sort("sort"));
                    listView.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    placesListAdapter = new PlacesListAdapter(getContext(), arrayList, this);
                    listView.setAdapter(placesListAdapter);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
                            Place place = (Place) arrayList.get(i);
                            Intent intent = new Intent(getActivity(), PlaceActivity.class);
                            intent.putExtra("placeId", place.getId());
                            intent.putExtra("tabInt", 0);
                            getActivity().startActivity(intent);

                        }
                    });
                    break;

                case DISH_TYPES_LIST:
                    arrayList = new ArrayList(EatApplication.realm.where(DishType.class).contains("image", "/upload").notEqualTo("places.title", "null").findAll().sort("sort"));
                    listView.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    dishTypesAdapter = new DishTypesListAdapter(getContext(), arrayList);
                    listView.setAdapter(dishTypesAdapter);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            DishType dishType = (DishType) arrayList.get(i);
                            Intent intent = new Intent(getActivity(), DishTypePlaces.class);
                            intent.putExtra("dishTypeId", dishType.getId());
                            getActivity().startActivity(intent);
                        }
                    });

                    break;
                case FAVOURITE_PLACES_LIST:

                    break;
                case FAVOURITE_DISHES_LIST:

                    break;

            }

        } else {

            switch (tabName) {

                case "Tab_favourites_places":  // Избранное - заведения
                    arrayList = new ArrayList(EatApplication.realm.where(Place.class).equalTo("favourite", true).findAll());
                    listView.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);

                    placesListAdapter = new PlacesListAdapter(getContext(), arrayList, this);
                    listView.setAdapter(placesListAdapter);

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {

                            Place place = (Place) arrayList.get(i);
                            Intent intent = new Intent(getActivity(), PlaceActivity.class);
                            intent.putExtra("placeId", place.getId());
                            getActivity().startActivity(intent);
                        }
                    });

                    break;

                case "Tab_favourites_dishes":  // Избранное - блюда
                    arrayList = new ArrayList(EatApplication.realm.where(Dish.class).equalTo("favourite", true).findAll());
                    listView.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);

                    dishesListAdapter = new SmallDishesListAdapter(getContext(), arrayList, this);
                    listView.setAdapter(dishesListAdapter);

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {

                            Dish dish = (Dish) arrayList.get(i);
                            Place place = dish.getPlace();
                            Intent intent = new Intent(getActivity(), PlaceActivity.class);
                            intent.putExtra("placeId", place.getId());
                            intent.putExtra("startDishId", dish.getId());
                            getActivity().startActivity(intent);
                        }
                    });

                    break;

                default:

                    break;
            }
        }

        return rootView;
    }


    @Override
    public void switchPlaceFavourite(Place place) {
        EatApplication.dataManager.setPlaceToFavourite(place, !place.isFavourite());
        placesListAdapter.notifyDataSetChanged();
    }

    @Override
    public void switchDishFavourite(Dish dish) {
        EatApplication.dataManager.setDishToFavourite(dish, !dish.isFavourite());
        dishesListAdapter.notifyDataSetChanged();
    }

    @Override
    public void actionPressed(Action action) {

        Intent imageActivity = new Intent(getContext(), DetailImageActivity.class);
        imageActivity.putExtra("imagePath", action.getImage());
        imageActivity.putExtra("pageTitle", action.getPlace().getTitle());
        imageActivity.putExtra("placeId", action.getPlace().getId());
        getContext().startActivity(imageActivity);

    }

    @Override
    public void interriorPressed(GalleryImage image) {

    }
}
