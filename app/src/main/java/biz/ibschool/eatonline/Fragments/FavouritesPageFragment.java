package biz.ibschool.eatonline.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import biz.ibschool.eatonline.Adapters.BasePagerAdapter;
import biz.ibschool.eatonline.EatApplication;
import biz.ibschool.eatonline.Models.Dish;
import biz.ibschool.eatonline.Models.Place;
import biz.ibschool.eatonline.R;

import java.util.ArrayList;

public class FavouritesPageFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.page_fragment, container, false);
        initViewPagerAndTabs(rootView);
        return rootView;
    }

    @SuppressWarnings("ConstantConditions")
    private void initViewPagerAndTabs(View rootView) {

        ViewPager pager = (ViewPager) rootView.findViewById(R.id.base_pager);
        FragmentManager fm = getActivity().getSupportFragmentManager();

        ArrayList<String> tabNames = new ArrayList<>();
        if (EatApplication.realm.where(Place.class).equalTo("favourite", true).findAll().size() > 0) {
            tabNames.add("Tab_favourites_places");
        }
        if (EatApplication.realm.where(Dish.class).equalTo("favourite", true).findAll().size() > 0) {
            tabNames.add("Tab_favourites_dishes");
        }

        pager.setAdapter(new BasePagerAdapter(fm, tabNames));

        //--> Tabs
        TabLayout tabs = (TabLayout) rootView.findViewById(R.id.base_tabs);
        tabs.setupWithViewPager(pager);

        //--> Adding tabs
        for (int i = 0; i < tabNames.size(); i++) {
            String tabName = tabNames.get(i);
            switch (tabName) {
                case "Tab_favourites_places":
                    tabs.getTabAt(i).setText("ЗАВЕДЕНИЯ");
                    break;
                case "Tab_favourites_dishes":
                    tabs.getTabAt(i).setText("БЛЮДА");
                    break;
                default:
                    tabs.getTabAt(i).setText("unknown");
                    break;
            }
        }
    }
}
