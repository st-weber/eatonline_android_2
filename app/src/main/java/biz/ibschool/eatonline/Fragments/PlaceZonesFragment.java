package biz.ibschool.eatonline.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import biz.ibschool.eatonline.Interfaces.Const;
import biz.ibschool.eatonline.Models.Place;
import biz.ibschool.eatonline.R;

public class PlaceZonesFragment extends Fragment {

    private Place place;

    public void setPlace(Place place) {
        this.place = place;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.place_zones_fragment, container, false);

        String url = Const.API_URL + "places.php?req=zones&place=" + place.getId();
        WebView webView = (WebView)rootView.findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);

        return rootView;

    }

}
