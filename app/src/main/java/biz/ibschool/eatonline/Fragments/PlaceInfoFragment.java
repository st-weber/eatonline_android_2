package biz.ibschool.eatonline.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import biz.ibschool.eatonline.Activities.DetailDishActivity;
import biz.ibschool.eatonline.Activities.DetailImageActivity;
import biz.ibschool.eatonline.Activities.SelectTable;
import biz.ibschool.eatonline.Adapters.Expandable.SubtypesExpandableListAdapter;
import biz.ibschool.eatonline.Adapters.Expandable.TypesExpandableListAdapter;
import biz.ibschool.eatonline.DataManager;
import biz.ibschool.eatonline.EatApplication;
import biz.ibschool.eatonline.Interfaces.DishActions;
import biz.ibschool.eatonline.Interfaces.FavouritePressed;
import biz.ibschool.eatonline.Interfaces.PlacePageActions;
import biz.ibschool.eatonline.Models.Action;
import biz.ibschool.eatonline.Models.Dish;
import biz.ibschool.eatonline.Models.DishType;
import biz.ibschool.eatonline.Models.Order;
import biz.ibschool.eatonline.Models.Place;
import biz.ibschool.eatonline.Models.Table;
import biz.ibschool.eatonline.R;

import java.util.ArrayList;
import java.util.HashMap;


public class PlaceInfoFragment extends Fragment implements FavouritePressed, DishActions, PlacePageActions, DataManager.DishActionsCallback {

    private Place place;
    private TypesExpandableListAdapter typesExpandableListAdapter;
    private HashMap<String, SubtypesExpandableListAdapter> subtypesExpandableListAdapters;
    public int currentService;
    private int startTypeId;
    private int startDishId;

    public void setPlace(Place place) {
        this.place = place;
    }

    public void setStartTypeId(int startTypeId) {
        this.startTypeId = startTypeId;
    }

    public void setStartDishId(int startDishId) {
        this.startDishId = startDishId;
    }

    @Override
    public void onResume() {
        super.onResume();
        EatApplication.dataManager.registerDishActionsListener(this);
        if (typesExpandableListAdapter != null) {
            typesExpandableListAdapter.refreshMenu();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        EatApplication.dataManager.registerDishActionsListener(null);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.place_info_fragment, container, false);

        subtypesExpandableListAdapters = new HashMap<String, SubtypesExpandableListAdapter>();

        ArrayList<DishType> placeDishTypes = new ArrayList(EatApplication.realm.where(DishType.class).contains("places.title", place.getTitle()).equalTo("parent", 0).findAll().sort("sort"));

        currentService = 0;
        Order currentOrder = EatApplication.dataManager.getCurrentOrder();
        if (currentOrder.getPlace() != null) {
            if (currentOrder.getPlace().getId() == place.getId()) {
                currentService = currentOrder.getType();
            } else {
                if (place.isReserv()) {
                    currentService = 2;
                }
                if (place.isTakeaway()) {
                    currentService = 1;
                }
                if (place.isDelivery()) {
                    currentService = 0;
                }
            }
        } else {
            if (place.isReserv()) {
                currentService = 2;
            }
            if (place.isTakeaway()) {
                currentService = 1;
            }
            if (place.isDelivery()) {
                currentService = 0;
            }
        }

        int openedType = -1;
        int groupPosition = 1;
        int startSubtypeId = 0;
        int scrollInDp = 0;



        Dish startDish = null;
        if (startDishId > 0) {
            startDish = EatApplication.realm.where(Dish.class).equalTo("id", startDishId).findFirst();
            if (startDish.getDishType().getParent() > 0) { // блюдо в подтипе
                startSubtypeId = startDish.getDishType().getId();
            }
        }

        for (DishType dishType : placeDishTypes) {

            groupPosition++;
            if (dishType.getId() == startTypeId) {
                openedType = groupPosition;
            }

            ArrayList<DishType> typeSubtypes = new ArrayList(EatApplication.realm.where(DishType.class).contains("places.title", place.getTitle()).equalTo("parent", dishType.getId()).findAll().sort("sort"));

            if (typeSubtypes.size() > 0) {
                SubtypesExpandableListAdapter subtypesExpandableListAdapter = new SubtypesExpandableListAdapter(typeSubtypes, place, getContext(), currentService);
                subtypesExpandableListAdapter.setFavouritePressedListener(this);
                subtypesExpandableListAdapter.setDishActionsListener(this);
                if (startSubtypeId > 0 && dishType.getId() == startDish.getDishType().getParent()) {
                    subtypesExpandableListAdapter.setCurrentOpenedSubtype(startSubtypeId);
                    subtypesExpandableListAdapter.setActiveDish(startDish);
                }
                subtypesExpandableListAdapters.put(String.valueOf(dishType.getId()), subtypesExpandableListAdapter);
            }

        }

        final ExpandableListView expandableListView = (ExpandableListView) rootView.findViewById(R.id.expandable_list_view);

        typesExpandableListAdapter = new TypesExpandableListAdapter(placeDishTypes, subtypesExpandableListAdapters, place, getContext(), currentService);
        if (startDish != null) {
            typesExpandableListAdapter.setActiveDish(startDish);
        }
        if (openedType > 0) {
            scrollInDp = (openedType - 2) * 44 + 225;
        }


        typesExpandableListAdapter.setCurrentOpenedTypeIndex(openedType);
        typesExpandableListAdapter.setFavouritePressedListener(this);
        typesExpandableListAdapter.setDishActionsListener(this);
        typesExpandableListAdapter.setPlaceActionsListener(this);
        expandableListView.setAdapter(typesExpandableListAdapter);

        final int finalScrollInDp = scrollInDp;
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int i) {
                //System.out.println("scrollInPx: " + EatApplication.appUtils.dpToPixels(finalScrollInDp));
                /*if (finalScrollInDp > 0) {
                    //expandableListView.scrollTo(0, EatApplication.appUtils.dpToPixels(finalScrollInDp));

                    expandableListView.post(new Runnable() {
                        public void run() {
                            expandableListView.scrollTo(0, EatApplication.appUtils.dpToPixels(finalScrollInDp));
                            expandableListView.invalidate();
                            //typesExpandableListAdapter.setCurrentOpenedTypeIndex(-1);
                            //typesExpandableListAdapter.refreshMenu();
                        }
                    });
                }*/
            }
        });

        return rootView;

    }

    @Override
    public void switchPlaceFavourite(Place place) {

    }

    @Override
    public void switchDishFavourite(Dish dish) {
        EatApplication.dataManager.setDishToFavourite(dish, !dish.isFavourite());
        typesExpandableListAdapter.refreshMenu();
    }

    @Override
    public void addPressed(final Dish dish) {
        Order currentOrder = EatApplication.dataManager.getCurrentOrder();
        if (currentOrder.getAmounts().size() == 0) {
            currentOrder.setType(currentService);
        }
        EatApplication.dataManager.currentOrderChangeDishAmount(dish, 1, getContext());
    }

    @Override
    public void plusPressed(Dish dish) {
        EatApplication.dataManager.currentOrderChangeDishAmount(dish, EatApplication.dataManager.getCurrentOrderAmountForDish(dish) + 1, getContext());
    }

    @Override
    public void minusPressed(Dish dish) {
        EatApplication.dataManager.currentOrderChangeDishAmount(dish, EatApplication.dataManager.getCurrentOrderAmountForDish(dish) - 1, getContext());
    }

    @Override
    public void deletePressed(Dish dish) {
        EatApplication.dataManager.currentOrderChangeDishAmount(dish, 0, getContext());
    }

    @Override
    public void dishDetailPressed(Dish dish) {
        Intent intent = new Intent(getContext(), DetailDishActivity.class);
        intent.putExtra("dishID", dish.getId());
        intent.putExtra("orderType", currentService);
        startActivity(intent);
    }

    @Override
    public void deletePressed(Table table) {

    }

    @Override
    public void serviceChanged(int newService) {
        currentService = newService;
    }

    @Override
    public void showActionPressed(Action action) {
        Intent imageActivity = new Intent(getContext(), DetailImageActivity.class);
        imageActivity.putExtra("imagePath", action.getImage());
        imageActivity.putExtra("pageTitle", action.getPlace().getTitle());
        getContext().startActivity(imageActivity);
    }

    @Override
    public void chooseTablePressed() {
        Intent selectTable = new Intent(getContext(), SelectTable.class);
        selectTable.putExtra("placeID", place.getId());
        getContext().startActivity(selectTable);
    }

    @Override
    public void dishAmountChanged(Dish dish) {
        typesExpandableListAdapter.refreshMenu();
    }

}
