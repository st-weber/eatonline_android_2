package biz.ibschool.eatonline;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import biz.ibschool.eatonline.Interfaces.ApiInterface;
import biz.ibschool.eatonline.Interfaces.Const;
import biz.ibschool.eatonline.Interfaces.OrderActions;
import biz.ibschool.eatonline.Interfaces.OrderRequests;
import biz.ibschool.eatonline.Interfaces.PhotoLoading;
import biz.ibschool.eatonline.Models.Action;
import biz.ibschool.eatonline.Models.ApiSettings;
import biz.ibschool.eatonline.Models.City;
import biz.ibschool.eatonline.Models.Dish;
import biz.ibschool.eatonline.Models.DishType;
import biz.ibschool.eatonline.Models.GalleryImage;
import biz.ibschool.eatonline.Models.Order;
import biz.ibschool.eatonline.Models.OrderAmount;
import biz.ibschool.eatonline.Models.OrderStatus;
import biz.ibschool.eatonline.Models.Place;
import biz.ibschool.eatonline.Models.RequestCodeResult;
import biz.ibschool.eatonline.Models.RequestResult;
import biz.ibschool.eatonline.Models.Settings;
import biz.ibschool.eatonline.Models.Table;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import io.realm.Case;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DataManager extends FirebaseInstanceIdService {

    private Gson gson = new GsonBuilder().create();

    private Retrofit retrofit = new Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(EatApplication.appUtils.buildOkHttpClient())
            .baseUrl(Const.API_URL)
            .build();
    private ApiInterface apiInterface = retrofit.create(ApiInterface.class);

    private PhotoLoading photoLoading;
    private OrderRequests orderRequests;

    private int currentHour = -1;
    private int currentMinute = -1;

    private int loadedPlacesCount;
    private ArrayList<Place> loadedPlaces;
    private int placesCount;
    private int newOrderType = -1;
    private HashMap<String, DishType> loadedDishTypes;


    public interface RequestInterface {

        void onSuccess();

        void onFailed();

    }

    public interface PlacesLoadingInterface {
        void placeInfoLoaded(Place place, int placeNumber);
    }

    public void setNewOrderType(int newOrderType) {
        this.newOrderType = newOrderType;
    }

    public void registerPhotoLoadingSubscriber(PhotoLoading activity) {
        photoLoading = activity;
    }

    public void registerOrderRequestsSubscriber(OrderRequests activity) {
        orderRequests = activity;
    }

    public int getPlacesCount() {
        int placesCount = EatApplication.realm.where(Place.class).findAll().size();
        return placesCount;
    }

    public void loadCities(final RequestInterface requestInterface) {

        apiInterface.loadCitiesJSON().enqueue(
                new Callback<RealmList<City>>() {
                    @Override
                    public void onResponse(Call<RealmList<City>> call, Response<RealmList<City>> response) {

                        EatApplication.realm.beginTransaction();
                        EatApplication.realm.copyToRealmOrUpdate(response.body());
                        EatApplication.realm.commitTransaction();

                        try {
                            requestInterface.onSuccess();

                        } catch (Exception ex) {
                            sendError("loadCitiesJSON Exception: " + ex.getMessage());
                            requestInterface.onFailed();
                        }
                    }

                    @Override
                    public void onFailure(Call<RealmList<City>> call, Throwable t) {
                        sendError("loadCities onFailure: " + t.getMessage());
                        requestInterface.onFailed();
                    }
                }
        );
    }

    public void loadDishTypes(final RequestInterface requestInterface) {
        apiInterface.loadDishTypesJSON().enqueue(
                new Callback<List<DishType>>() {
                    @Override
                    public void onResponse(Call<List<DishType>> call, Response<List<DishType>> response) {

                        if (!EatApplication.realm.where(DishType.class).findAll().isEmpty()) {
                            System.out.println("update dish types");
                            HashMap<String, DishType> loadedDishTypes = new HashMap<String, DishType>();

                            for (DishType dishType : EatApplication.realm.where(DishType.class).findAll()) {
                                loadedDishTypes.put(String.valueOf(dishType.getId()), dishType);
                                System.out.println("loaded: " + dishType.getName() + " " + dishType.getId());
                            }

                            for (DishType dishType : response.body()) {

                                if (loadedDishTypes.get(String.valueOf(dishType.getId())) != null) {
                                    System.out.println("contains " + dishType.getName() + " " + dishType.getId());

                                    if (dishType.getSubtypes() != null) {
                                        for (DishType subType : dishType.getSubtypes()) {

                                            if (loadedDishTypes.get(String.valueOf(subType.getId())) == null) {
                                                //System.out.println("add subtype " + subType.getName() + " " + subType.getId());
                                                EatApplication.realm.beginTransaction();
                                                EatApplication.realm.copyToRealmOrUpdate(subType);
                                                EatApplication.realm.commitTransaction();
                                            }
                                        }
                                    }


                                } else {
                                    System.out.println("not contains " + dishType.getName() + " " + dishType.getId());
                                    EatApplication.realm.beginTransaction();
                                    EatApplication.realm.copyToRealmOrUpdate(dishType);
                                    EatApplication.realm.commitTransaction();
                                }

                                /*if (!loadedDishTypes.containsKey(String.valueOf(dishType.getId()))) {

                                    EatApplication.realm.beginTransaction();
                                    EatApplication.realm.copyToRealmOrUpdate(dishType);
                                    EatApplication.realm.commitTransaction();

                                } else {

                                }*/
                            }

                        } else {

                            ArrayList<DishType> dishTypes = new ArrayList<DishType>(response.body());

                            EatApplication.realm.beginTransaction();
                            EatApplication.realm.copyToRealmOrUpdate(dishTypes);
                            EatApplication.realm.commitTransaction();

                        }

                        try {
                            requestInterface.onSuccess();
                        } catch (Exception ex) {
                            sendError("loadDishTypesJSON Exception: " + ex.getMessage());
                            requestInterface.onFailed();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<DishType>> call, Throwable t) {
                        sendError("loadDishTypes onFailure: " + t.getMessage());
                        requestInterface.onFailed();
                    }
                }
        );
    }

    public void loadPlacesList(final RequestInterface requestInterface) {

        final String cityID = Integer.toString(EatApplication.dataManager.getSettings().getCurrentCity().getId());

        apiInterface.loadPlacesListOfCityJSON(cityID).enqueue(
                new Callback<List<Place>>() {
                    @Override
                    public void onResponse(Call<List<Place>> call, Response<List<Place>> response) {
                        // загружаем список заведений в базу

                        HashMap<String, Place> loadedPlaces = new HashMap<String, Place>();
                        HashMap<String, Place> newPlaces = new HashMap<String, Place>();

                        for (Place place : EatApplication.realm.where(Place.class).findAll()) {
                            loadedPlaces.put(String.valueOf(place.getId()), place);
                        }
                        ArrayList<Place> placesResponse = new ArrayList<Place>(response.body());
                        for (Place newPlace : placesResponse) {
                            Place loadedPlace = loadedPlaces.get(String.valueOf(newPlace.getId()));
                            if (loadedPlace != null) { // заведение уже есть в базе

                                if (!loadedPlace.getUpdated().equals(newPlace.getUpdated())) {
                                    loadedPlace.setIsUpdated(false);
                                }

                                EatApplication.realm.beginTransaction();
                                loadedPlace.setAddress(newPlace.getAddress());
                                loadedPlace.setBack(newPlace.getBack());
                                loadedPlace.setOphour(newPlace.getOphour());
                                loadedPlace.setClhour(newPlace.getClhour());
                                loadedPlace.setCoordinates(newPlace.getCoordinates());
                                loadedPlace.setDfrom(newPlace.getDfrom());
                                loadedPlace.setDprice(newPlace.getDprice());
                                loadedPlace.setFootNote(newPlace.getFootNote());
                                loadedPlace.setKitchens(newPlace.getKitchens());
                                loadedPlace.setLogo(newPlace.getLogo());
                                loadedPlace.setMorder(newPlace.getMorder());
                                loadedPlace.setPayment(newPlace.getPayment());
                                loadedPlace.setSber(newPlace.getSber());
                                loadedPlace.setCterminal(newPlace.getCterminal());
                                loadedPlace.setOutput(newPlace.getOutput());
                                loadedPlace.setDelivery(newPlace.getDelivery());
                                loadedPlace.setTakeaway(newPlace.getTakeaway());
                                loadedPlace.setReserv(newPlace.getReserv());
                                loadedPlace.setTitle(newPlace.getTitle());
                                loadedPlace.setType(newPlace.getType());
                                loadedPlace.setWtime(newPlace.getWtime());
                                loadedPlace.setUpdated(newPlace.getUpdated());
                                EatApplication.realm.commitTransaction();

                            } else { // заведения нет в базе
                                EatApplication.realm.beginTransaction();
                                EatApplication.realm.copyToRealm(newPlace);
                                EatApplication.realm.commitTransaction();
                                newPlace.setIsUpdated(false);
                            }
                            newPlaces.put(String.valueOf(newPlace.getId()), newPlace);
                        }

                        for (Place oldPlace : EatApplication.realm.where(Place.class).findAll()) { // удаляем старые заведения

                            if (!newPlaces.containsKey(String.valueOf(oldPlace.getId()))) {
                                System.out.println("delete " + oldPlace.getTitle());
                                EatApplication.realm.beginTransaction();
                                oldPlace.deleteFromRealm();
                                EatApplication.realm.commitTransaction();
                            }

                        }

                        try {
                            requestInterface.onSuccess();
                        } catch (Exception ex) {
                            sendError("loadPlacesListOfCityJSON Exception: " + ex.getMessage());
                            requestInterface.onFailed();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Place>> call, Throwable t) {
                        sendError("loadPlacesList of city " + cityID + " onFailure: " + t.getMessage());
                        requestInterface.onFailed();
                    }
                }
        );
    }

    public void loadActions(final RequestInterface requestInterface) {

        final String cityID = Integer.toString(EatApplication.dataManager.getSettings().getCurrentCity().getId());

        apiInterface.loadActionsOfCityJSON(cityID).enqueue(
                new Callback<List<Action>>() {
                    @Override
                    public void onResponse(Call<List<Action>> call, Response<List<Action>> response) {

                        HashMap<String, Action> loadedActions = new HashMap<String, Action>();
                        for (Action action : EatApplication.realm.where(Action.class).findAll()) {
                            loadedActions.put(String.valueOf(action.getId()), action);
                        }

                        ArrayList<Action> actionsResponse = new ArrayList<Action>(response.body());
                        for (Action newAction : actionsResponse) {
                            Action loadedAction = loadedActions.get(String.valueOf(newAction.getId()));
                            if (loadedAction == null) { // нет в базе
                                EatApplication.realm.beginTransaction();
                                EatApplication.realm.copyToRealm(newAction);
                                EatApplication.realm.commitTransaction();

                                Place place = EatApplication.realm.where(Place.class).equalTo("id", newAction.getPlaceID()).findFirst();
                                if (place != null) {
                                    EatApplication.realm.beginTransaction();

                                    System.out.println("newAction " + newAction.getTitle());
                                    System.out.println("place " + place.getTitle());

                                    newAction.setPlace(place);
                                    place.setAction(newAction);
                                    EatApplication.realm.commitTransaction();
                                }
                            } else {
                                loadedActions.remove(String.valueOf(loadedAction.getId()));
                            }
                            //newPlaces.put(String.valueOf(newPlace.getId()), newPlace);
                        }

                        for (Action oldAction: loadedActions.values()) {
                            System.out.println("delete " + oldAction.getTitle());
                            EatApplication.realm.beginTransaction();
                            oldAction.deleteFromRealm();
                            EatApplication.realm.commitTransaction();
                        }

                        //EatApplication.realm.beginTransaction();
                        //EatApplication.realm.copyToRealmOrUpdate(response.body());
                        //EatApplication.realm.commitTransaction();

                        /*
                        RealmResults<Action> actions = EatApplication.realm.where(Action.class).findAll();
                        EatApplication.realm.beginTransaction();
                        for (Action action : actions) {
                            Place place = EatApplication.realm.where(Place.class).equalTo("id", action.getPlaceID()).findFirst();
                            System.out.println("action: " + action.getTitle());
                            System.out.println("place: " + place.getTitle());
                            action.setPlace(place);
                            place.setAction(action);
                        }
                        EatApplication.realm.commitTransaction();*/

                        try {
                            requestInterface.onSuccess();
                        } catch (Exception ex) {
                            sendError("loadActionsOfCityJSON Exception: " + ex.getMessage());
                            requestInterface.onFailed();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Action>> call, Throwable t) {
                        sendError("loadActions of city " + cityID + " onFailure: " + t.getMessage());
                        requestInterface.onFailed();
                    }
                }
        );
    }


    private void loadNextPlace(RequestInterface requestInterface, PlacesLoadingInterface placesLoadingInterface) {

        final RequestInterface finalRequestInterface = requestInterface;
        final PlacesLoadingInterface finalPlacesLoadingInterface = placesLoadingInterface;

        if (loadedPlacesCount >= placesCount) {

            randomizePlaces();
            if (getSettings().getToken() == null) {
                onTokenRefresh();
            }
            try {
                finalRequestInterface.onSuccess();
            } catch (Exception ex) {
                sendError("checkLoadedPlacesCount Exception: " + ex.getMessage());
                finalRequestInterface.onFailed();
            }

        } else {

            final Place place = loadedPlaces.get(loadedPlacesCount);

            if (!place.getIsUpdated()) {

                System.out.println("update place menu: " + place.getTitle());

                apiInterface.loadDishesOfPlaceJSON(Integer.toString(place.getId())).enqueue(
                        new Callback<List<Dish>>() {
                            @Override
                            public void onResponse(Call<List<Dish>> call, Response<List<Dish>> response) {
                                ArrayList<Dish> dishes = new ArrayList<Dish>(response.body());

                                EatApplication.realm.beginTransaction();
                                ArrayList<DishType> placeDishTypes = new ArrayList(EatApplication.realm.where(DishType.class).contains("places.title", place.getTitle()).findAll());
                                for (DishType dishType : placeDishTypes) {
                                    dishType.getPlaces().remove(place);
                                }
                                place.getDishes().deleteAllFromRealm();
                                place.getDishTypes().clear();
                                EatApplication.realm.commitTransaction();

                                EatApplication.realm.beginTransaction();
                                for (Dish dish : dishes) {
                                    if (dish.getType().isEmpty()) {
                                        sendError("dish empty Type" + dish.getTitle() + " - " + dish.getId());
                                    } else {

                                        dish.setPlace(place);
                                        place.getDishes().add(dish);

                                        DishType dishType = loadedDishTypes.get(String.valueOf(dish.getType()));
                                        dish.setDishType(dishType);
                                        try {
                                            dishType.getDishes().add(dish);
                                            if (!place.getDishTypes().contains(dishType)) {
                                                place.getDishTypes().add(dishType);
                                                dishType.getPlaces().add(place);
                                            }
                                            if (dishType.getParent() != 0) {
                                                DishType parentType = loadedDishTypes.get(String.valueOf(dishType.getParent()));
                                                if (!place.getDishTypes().contains(parentType)) {
                                                    place.getDishTypes().add(parentType);
                                                    parentType.getPlaces().add(place);
                                                }
                                            }
                                        } catch (Throwable t) {
                                            sendError(t.getMessage() + " dishType: " + dishType.getName() + " dish: " + dish.getId());
                                        }
                                    }
                                }
                                EatApplication.realm.copyToRealmOrUpdate(dishes);
                                EatApplication.realm.commitTransaction();

                                if (place.isReserv()) {
                                    apiInterface.loadTablesOfPlaceJSON(Integer.toString(place.getId())).enqueue(
                                            new Callback<List<Table>>() {
                                                @Override
                                                public void onResponse(Call<List<Table>> call, Response<List<Table>> response) {
                                                    ArrayList<Table> tables = new ArrayList<Table>(response.body());
                                                    EatApplication.realm.beginTransaction();
                                                    place.getTables().deleteAllFromRealm();
                                                    for (Table table : tables) {
                                                        table.setPlace(place);
                                                        place.getTables().add(table);
                                                    }
                                                    EatApplication.realm.copyToRealmOrUpdate(tables);
                                                    EatApplication.realm.commitTransaction();

                                                    place.setIsUpdated(true);
                                                    try {
                                                        finalPlacesLoadingInterface.placeInfoLoaded(place, loadedPlacesCount);
                                                    } catch (Exception ex) {
                                                        sendError("placeInfoLoaded Exception: " + ex.getMessage());
                                                        finalRequestInterface.onFailed();
                                                    }

                                                    loadedPlacesCount++;
                                                    loadNextPlace(finalRequestInterface, finalPlacesLoadingInterface);
                                                    //checkLoadedPlacesCount(requestInterface);
                                                }

                                                @Override
                                                public void onFailure(Call<List<Table>> call, Throwable t) {
                                                    sendError("loadTablesOfPlace " + place.getTitle() + " onFailure: " + t.getMessage());
                                                    finalRequestInterface.onFailed();
                                                }
                                            }
                                    );
                                } else {
                                    place.setIsUpdated(true);
                                    try {
                                        finalPlacesLoadingInterface.placeInfoLoaded(place, loadedPlacesCount);
                                    } catch (Exception ex) {
                                        sendError("placeInfoLoaded Exception: " + ex.getMessage());
                                        finalRequestInterface.onFailed();
                                    }

                                    loadedPlacesCount++;
                                    loadNextPlace(finalRequestInterface, finalPlacesLoadingInterface);
                                    //checkLoadedPlacesCount(requestInterface);
                                }
                            }

                            @Override
                            public void onFailure(Call<List<Dish>> call, Throwable t) {
                                sendError("loadDishesOfPlace " + place.getTitle() + " id:" + place.getId() + " onFailure: " + t.getMessage());
                                finalRequestInterface.onFailed();
                            }
                        }
                );
            } else {
                loadedPlacesCount++;
                loadNextPlace(finalRequestInterface, finalPlacesLoadingInterface);
                //checkLoadedPlacesCount(requestInterface);
            }
        }
    }


    public void loadPlacesInfo(final RequestInterface requestInterface, final PlacesLoadingInterface placesLoadingInterface) {
        // перебираем все загруженные заведения
        // загружаем их блюда и столики
        loadedPlaces = new ArrayList<Place>(EatApplication.realm.where(Place.class).findAll());
        loadedPlacesCount = 0;
        placesCount = loadedPlaces.size();

        loadedDishTypes = new HashMap<String, DishType>();
        for (DishType dishType : EatApplication.realm.where(DishType.class).findAll()) {
            loadedDishTypes.put(String.valueOf(dishType.getId()), dishType);
        }
        loadNextPlace(requestInterface, placesLoadingInterface);
    }

    public void loadPhoto(final String placeID) {
        apiInterface.loadPhotoOfPlaceJSON(placeID).enqueue(
                new Callback<List<GalleryImage>>() {
                    @Override
                    public void onResponse(Call<List<GalleryImage>> call, Response<List<GalleryImage>> response) {
                        try {
                            ArrayList<GalleryImage> photo = new ArrayList<GalleryImage>(response.body());
                            photoLoading.photoLoaded(photo);
                        } catch (Throwable t) {
                            sendError("photoLoading failed: " + t.getMessage());
                        }

                    }

                    @Override
                    public void onFailure(Call<List<GalleryImage>> call, Throwable t) {
                        sendError("loadPhotoOfPlace " + placeID + " failed: " + t.getMessage());
                    }
                }
        );
    }

    public void requestCheckingCode(final RequestInterface requestInterface) {
        apiInterface.requestCheckingCodeJSON(getSettings().getPersonalPhone()).enqueue(
                new Callback<RequestCodeResult>() {
                    @Override
                    public void onResponse(Call<RequestCodeResult> call, Response<RequestCodeResult> response) {
                        RequestCodeResult result = response.body();

                        //System.out.println("code: " + result.getCode());

                        if (orderRequests != null) {
                            orderRequests.requestCheckingCodeResult(result.getResend_delay(), result.getResend_current());
                        }
                    }

                    @Override
                    public void onFailure(Call<RequestCodeResult> call, Throwable t) {
                        sendError("requestCheckingCodeJSON failed: " + t.getMessage());
                        requestInterface.onFailed();
                    }
                }
        );
    }

    public void checkCode(final String code, final RequestInterface requestInterface) {
        apiInterface.checkCodeJSON(getSettings().getPersonalPhone(), code).enqueue(
                new Callback<RequestResult>() {
                    @Override
                    public void onResponse(Call<RequestResult> call, Response<RequestResult> response) {
                        if (orderRequests != null) {
                            orderRequests.checkCodeResult(response.body().getResult());
                        }
                    }

                    @Override
                    public void onFailure(Call<RequestResult> call, Throwable t) {
                        sendError("checkCode failed: " + t.getMessage());
                        requestInterface.onFailed();
                    }
                }
        );
    }

    public void checkoutOrder(String checkoutToken, String time, String comments, String persons, String change, String payment, final RequestInterface requestInterface) {
        final Order currentOrder = getCurrentOrder();
        if (currentOrder.getPlace() != null) {

            String dishIDs = "";
            String dishCounts = "";
            String token = getSettings().getToken();
            for (OrderAmount amount : currentOrder.getAmounts()) {
                dishIDs += "|" + amount.getDish().getId();
                dishCounts += "|" + amount.getAmount();
            }
            if (dishIDs.length() > 0) {
                dishIDs = dishIDs.substring(1, dishIDs.length());
            }
            if (dishCounts.length() > 0) {
                dishCounts = dishCounts.substring(1, dishCounts.length());
            }
            String tableID = "";
            if (currentOrder.getTable() != null) {
                tableID = String.valueOf(currentOrder.getTable().getId());
            }

            Settings settings = getSettings();

            String address = settings.getCurrentCity().getName();
            if (settings.getPersonalDistrict().length() > 0) {
                address = settings.getPersonalDistrict();
            }
            if (settings.getPersonalStreet().length() > 0) {
                address += ", ул." + settings.getPersonalStreet();
            }
            if (settings.getPersonalHouse().length() > 0) {
                address += ", д." + settings.getPersonalHouse();
            }
            if (settings.getPersonalPorch().length() > 0) {
                address += ", под." + settings.getPersonalPorch();
            }
            if (settings.getPersonalFloor().length() > 0) {
                address += ", эт." + settings.getPersonalFloor();
            }
            if (settings.getPersonalRoom().length() > 0) {
                address += ", кв." + settings.getPersonalRoom();
            }
            if (settings.getPersonalIntecrom().length() > 0) {
                address += ", домофон." + settings.getPersonalIntecrom();
            }

            apiInterface.checkoutOrder(checkoutToken,
                    EatApplication.appUtils.getPhoneFormattedString(settings.getPersonalPhone()),
                    String.valueOf(currentOrder.getPlace().getId()),
                    dishIDs,
                    dishCounts,
                    tableID,
                    time,
                    payment,
                    settings.getPersonalName(),
                    address,
                    comments,
                    persons,
                    change,
                    String.valueOf(currentOrder.getType()),
                    token).enqueue(new Callback<RequestResult>() {
                @Override
                public void onResponse(Call<RequestResult> call, Response<RequestResult> response) {
                    if (response.body().getResult().equals("fail")) {
                        sendError("checkoutOrder server fail");
                    } else {
                        EatApplication.realm.beginTransaction();
                        currentOrder.setStatus(1);
                        currentOrder.setNumber(Integer.parseInt(response.body().getResult()));
                        EatApplication.realm.commitTransaction();

                        ArrayList arrayListOrders = new ArrayList(EatApplication.realm.where(Order.class)
                                .isNotNull("place")
                                .findAll()
                                .sort("status"));

                        System.out.println("checkout completed: " + arrayListOrders.size());

                        requestInterface.onSuccess();
                    }
                }

                @Override
                public void onFailure(Call<RequestResult> call, Throwable t) {
                    sendError("checkoutOrder failed: " + t.getMessage());
                    requestInterface.onFailed();
                }
            });

        }
    }


    public void sentMessageToDevelopers(String phone, String text, final RequestInterface requestInterface) {
        apiInterface.sendMessageTodevelopersJSON(phone, text).enqueue(
                new Callback<RequestResult>() {
                    @Override
                    public void onResponse(Call<RequestResult> call, Response<RequestResult> response) {
                        if (response.body().getResult().equals("sented")) {
                            requestInterface.onSuccess();
                        } else {
                            sendError("sendMessageTodevelopersJSON result: " + response.body().getResult());
                            requestInterface.onFailed();
                        }
                    }

                    @Override
                    public void onFailure(Call<RequestResult> call, Throwable t) {
                        sendError("sentMessageToDevelopers failed: " + t.getMessage());
                        requestInterface.onFailed();
                    }
                }
        );
    }

    public ArrayList<City> sort(RealmResults<City> array) {
        ArrayList arrayList = new ArrayList();

        for (int i = 0; i < array.where().equalTo("par", 0).findAll().size(); i++) {
            arrayList.add(array.where().equalTo("par", 0).findAll().sort("sort", Sort.ASCENDING).get(i));
            for (int j = 0; j < array.size(); j++) {
                if (array.sort("sort", Sort.ASCENDING).get(j).getPar() == array.where().equalTo("par", 0).findAll().sort("sort", Sort.ASCENDING).get(i).getId())
                    arrayList.add(array.sort("sort", Sort.ASCENDING).get(j));
            }
        }
        return arrayList;
    }

    public void randomizePlaces() {
        RealmResults<Place> places = EatApplication.realm.where(Place.class).findAll();
        Random random = new Random();
        EatApplication.realm.beginTransaction();
        for (Place place : places) {
            place.setSort(random.nextInt(places.size()));
        }
        EatApplication.realm.commitTransaction();
    }

    public ArrayList<Place> searchPlacesBy(String query) {

        ArrayList<Place> places = new ArrayList<Place>(EatApplication.realm.where(Place.class)
                .contains("title", query, Case.INSENSITIVE)
                .findAll());

        return places;
    }

    public ArrayList<Dish> searchDishesBy(String query) {

        ArrayList<Dish> dishes = new ArrayList<Dish>(EatApplication.realm.where(Dish.class)
                .contains("title", query, Case.INSENSITIVE)
                .findAll());

        return dishes;
    }

    public int getSummOrder(Order order) {
        int summ = 0;
        ArrayList<OrderAmount> arrayList = new ArrayList<OrderAmount>(order.getAmounts());

        int orderType = getCurrentOrder().getType();
        for (OrderAmount amount : order.getAmounts()) {
            switch (orderType) {
                case 0:
                    if (amount.getDish().getDel().equals("1"))
                        summ += amount.getAmount() * amount.getDish().getPrice();
                    break;
                case 1:
                    if (amount.getDish().getTkw().equals("1"))
                        summ += amount.getAmount() * amount.getDish().getPrice();
                    break;
                case 2:
                    if (amount.getDish().getRes().equals("1"))
                        summ += amount.getAmount() * amount.getDish().getPrice();
                    break;
                default:
                    break;
            }
        }

        if (getCurrentOrder().getTable() != null) {
            summ += getCurrentOrder().getTable().getPrice();
        }

        return summ;
    }

    public void refreshTime() {
        currentHour = -1;
        currentMinute = -1;
    }

    public int getCurrentHour() {
        if (currentHour < 0) {
            long date = System.currentTimeMillis();
            SimpleDateFormat sdf = new SimpleDateFormat("H");
            String dateString = sdf.format(date);
            currentHour = Integer.parseInt(dateString);
        }
        return currentHour;
    }

    public int getCurrentMinutes() {
        if (currentMinute < 0) {
            long date = System.currentTimeMillis();
            SimpleDateFormat sdf = new SimpleDateFormat("m");
            String dateString = sdf.format(date);
            currentMinute = Integer.parseInt(dateString);
        }
        return currentMinute;
    }

    public int getCurrentHour(long time) {
        if (currentHour < 0) {
            long date = time;
            SimpleDateFormat sdf = new SimpleDateFormat("H");
            String dateString = sdf.format(date);
            currentHour = Integer.parseInt(dateString);
        }
        return currentHour;
    }

    public int getCurrentMinutes(long time) {
        if (currentMinute < 0) {
            long date = time;
            SimpleDateFormat sdf = new SimpleDateFormat("m");
            String dateString = sdf.format(date);
            currentMinute = Integer.parseInt(dateString);
        }
        return currentMinute;
    }

    public boolean isPlaceClosed(Place place) {

        int currentHour = getCurrentHour();

        //System.out.println(place + " " + );

        if (place.getOphour() < place.getClhour()) { // работает до полуночи
            if ((place.getOphour() <= currentHour) && (place.getClhour() > currentHour)) {
                return false;
            } else {
                return true;
            }
        } else if (place.getOphour() > place.getClhour()) { // работает после полуночи
            if ((place.getOphour() <= currentHour) || (place.getClhour() > currentHour)) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }


    public Settings getSettings() {
        Settings settings;
        if (EatApplication.realm.where(Settings.class).findAll().isEmpty()) {
            EatApplication.realm.beginTransaction();
            settings = EatApplication.realm.createObject(Settings.class);
            settings.setPersonsNumber(1);
            settings.setReceiveNotifications(true);
            EatApplication.realm.copyToRealm(settings);
            EatApplication.realm.commitTransaction();
        } else {
            settings = EatApplication.realm.where(Settings.class).findFirst();
        }
        return settings;
    }

    public Order getCurrentOrder() {
        Order order;
        if (EatApplication.realm.where(Order.class).equalTo("status", 0).findAll().isEmpty()) {
            EatApplication.realm.beginTransaction();
            order = EatApplication.realm.createObject(Order.class);
            order.setStatus(0);
            EatApplication.realm.copyToRealm(order);
            EatApplication.realm.commitTransaction();
        } else {
            order = EatApplication.realm.where(Order.class).equalTo("status", 0).findFirst();
        }
        return order;
    }

    public int getActionsCount() {
        return EatApplication.realm.where(Action.class).findAll().size();
    }


    public void setPlaceToFavourite(Place place, boolean favourite) {
        EatApplication.realm.beginTransaction();
        place.setFavourite(favourite);
        EatApplication.realm.commitTransaction();

        if (favouritesCountCallback != null) {
            favouritesCountCallback.favouritesCountChanged();
        }
    }

    public void setDishToFavourite(Dish dish, boolean favourite) {
        EatApplication.realm.beginTransaction();
        dish.setFavourite(favourite);
        EatApplication.realm.commitTransaction();

        if (favouritesCountCallback != null) {
            favouritesCountCallback.favouritesCountChanged();
        }
    }


    public int getFavouritesCount() {
        int count = 0;
        count += EatApplication.realm.where(Place.class).equalTo("favourite", true).findAll().size();
        count += EatApplication.realm.where(Dish.class).equalTo("favourite", true).findAll().size();
        return count;
    }

    public int getCurrentOrderAmountForDish(Dish dish) {
        OrderAmount orderAmount = getCurrentOrder().getAmounts().where().equalTo("dish.id", dish.getId()).findFirst();
        if (orderAmount != null) {
            return orderAmount.getAmount();
        } else {
            return 0;
        }
    }

    public void currentOrderChangeDishAmount(final Dish dish, int amount, Context context) {

        Order currentOrder = getCurrentOrder();

        if (amount <= 0) {
            EatApplication.realm.beginTransaction();
            OrderAmount orderAmount = currentOrder.getAmounts().where().equalTo("dish.id", dish.getId()).findFirst();
            if (orderAmount != null) {
                orderAmount.deleteFromRealm();
            }
            EatApplication.realm.commitTransaction();

            if (currentOrder.getAmounts().size() == 0 && currentOrder.getTable() == null) {
                currentOrder.setPlace(null);
            }

            if (dishActionsCallback != null) {
                dishActionsCallback.dishAmountChanged(dish);
            }

            if (basketCountCallback != null) {
                basketCountCallback.basketCountChanged();
            }

        } else if (currentOrder.getPlace() != null) {

            if (currentOrder.getPlace().getId() != dish.getPlace().getId()) {

                showDishOtherPlaceDialog(context, dish, amount);

            } else {

                OrderAmount orderAmount = currentOrder.getAmounts().where().equalTo("dish.id", dish.getId()).findFirst();

                if (orderAmount == null) {
                    EatApplication.realm.beginTransaction();
                    OrderAmount newAmount = EatApplication.realm.createObject(OrderAmount.class);
                    newAmount.setOrder(currentOrder);
                    newAmount.setDish(dish);
                    newAmount.setAmount(amount);
                    currentOrder.getAmounts().add(newAmount);
                    dish.setOrderAmount(newAmount);
                    EatApplication.realm.commitTransaction();

                } else {
                    EatApplication.realm.beginTransaction();
                    orderAmount.setAmount(amount);
                    EatApplication.realm.commitTransaction();
                }

                if (dishActionsCallback != null) {
                    dishActionsCallback.dishAmountChanged(dish);
                }

                if (basketCountCallback != null) {
                    basketCountCallback.basketCountChanged();
                }
            }

        } else {
            currentOrder.setPlace(dish.getPlace());

            EatApplication.realm.beginTransaction();
            OrderAmount orderAmount = EatApplication.realm.createObject(OrderAmount.class);
            orderAmount.setOrder(currentOrder);
            orderAmount.setDish(dish);
            orderAmount.setAmount(amount);
            currentOrder.getAmounts().add(orderAmount);
            dish.setOrderAmount(orderAmount);
            EatApplication.realm.commitTransaction();

            if (dishActionsCallback != null) {
                dishActionsCallback.dishAmountChanged(dish);
            }

            if (basketCountCallback != null) {
                basketCountCallback.basketCountChanged();
            }
        }
    }

    public void setTableToCurrentOrder(Context context, Table table) {

        Order currentOrder = getCurrentOrder();

        if (table == null) {
            EatApplication.realm.beginTransaction();
            currentOrder.setTable(null);
            EatApplication.realm.commitTransaction();

            if (currentOrder.getAmounts().size() == 0) {
                currentOrder.setPlace(null);
            }

        } else if (currentOrder.getPlace() != null) {

            if (currentOrder.getPlace().getId() != table.getPlace().getId()) {
                showTableOtherPlaceDialog(context, table);
            } else {
                EatApplication.realm.beginTransaction();
                currentOrder.setTable(table);
                EatApplication.realm.commitTransaction();
                currentOrder.setType(2);
            }

        } else {
            EatApplication.realm.beginTransaction();
            currentOrder.setTable(table);
            EatApplication.realm.commitTransaction();
            currentOrder.setType(2);
            currentOrder.setPlace(table.getPlace());
        }

        if (tableActionsCallback != null) {
            tableActionsCallback.tableReserveChanged();
        }
        if (basketCountCallback != null) {
            basketCountCallback.basketCountChanged();
        }
    }

    public void repeatOrder(Order order) {

        Order currentOrder = getCurrentOrder();

        EatApplication.realm.beginTransaction();
        currentOrder.getAmounts().deleteAllFromRealm();

        for (OrderAmount amount : order.getAmounts()) {
            OrderAmount newAmount = EatApplication.realm.createObject(OrderAmount.class);
            newAmount.setOrder(currentOrder);
            newAmount.setDish(amount.getDish());
            newAmount.setAmount(amount.getAmount());
            currentOrder.getAmounts().add(newAmount);
        }
        currentOrder.setTable(order.getTable());
        EatApplication.realm.commitTransaction();
        currentOrder.setPlace(order.getPlace());

        if (basketCountCallback != null) {
            basketCountCallback.basketCountChanged();
        }
    }

    public void checkOrderStatus(final Order order, final OrderActions orderActions, final RequestInterface requestInterface) {

        apiInterface.checkOrderStatus(String.valueOf(order.getNumber())).enqueue(new Callback<OrderStatus>() {
            @Override
            public void onResponse(Call<OrderStatus> call, Response<OrderStatus> response) {
                EatApplication.realm.beginTransaction();
                order.setStatus(response.body().getStatusInt());
                EatApplication.realm.commitTransaction();
                orderActions.orderStatusChecked();
            }

            @Override
            public void onFailure(Call<OrderStatus> call, Throwable t) {
                requestInterface.onFailed();
            }
        });

    }


    public Drawable buildCounterDrawable(boolean dark) {

        Order currentOrder = getCurrentOrder();

        int count = currentOrder.getAmounts().size();
        if (currentOrder.getTable() != null) {
            count++;
        }

        LayoutInflater inflater = LayoutInflater.from(EatApplication.appContext);
        final View view = inflater.inflate(R.layout.basket_icon_layout, null);
        FrameLayout basketFrame = (FrameLayout) view.findViewById(R.id.basket_frame);

        ImageView basketIcon = (ImageView) view.findViewById(R.id.basket_icon_image);
        ImageView badgeBack = (ImageView) view.findViewById(R.id.badge_background);
        TextView countText = (TextView) view.findViewById(R.id.count);

        badgeBack.setImageResource((dark) ? R.drawable.ic_badge_dark : R.drawable.ic_badge_red);
        countText.setTextColor(ContextCompat.getColor(EatApplication.appContext, (dark) ? R.color.colorDarkBackground : R.color.colorPrimary));

        if (count == 0) {
            badgeBack.setVisibility(View.GONE);
            countText.setVisibility(View.GONE);
            basketIcon.setAlpha((float) 0.5);
        } else {
            badgeBack.setVisibility(View.VISIBLE);
            countText.setVisibility(View.VISIBLE);
            countText.setText("" + count);
            basketIcon.setAlpha((float) 1);
        }

        basketFrame.measure(0, 0);
        basketFrame.layout(0, 0, basketFrame.getMeasuredWidth(), basketFrame.getMeasuredHeight());

        basketFrame.setDrawingCacheEnabled(true);
        basketFrame.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        Bitmap bitmap = Bitmap.createBitmap(basketFrame.getDrawingCache());
        basketFrame.setDrawingCacheEnabled(false);

        return new BitmapDrawable(EatApplication.appContext.getResources(), bitmap);
    }


    private DishActionsCallback dishActionsCallback;

    public interface DishActionsCallback {
        void dishAmountChanged(Dish dish);
    }

    public void registerDishActionsListener(DishActionsCallback dishActionsCallback) {
        this.dishActionsCallback = dishActionsCallback;
    }


    private void showDishOtherPlaceDialog(Context context, final Dish dish, final int amount) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setMessage("Вы действительно хотите сделать заказ в данном заведении?\nВсе добавленные в корзину блюда из другого заведения будут удалены.");

        alertDialogBuilder.setPositiveButton("Да", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                Order currentOrder = getCurrentOrder();

                EatApplication.realm.beginTransaction();
                currentOrder.getAmounts().deleteAllFromRealm();
                EatApplication.realm.commitTransaction();

                if (newOrderType >= 0) {
                    currentOrder.setType(newOrderType);
                    newOrderType = -1;
                } else {
                    currentOrder.setType(0);
                }

                EatApplication.realm.beginTransaction();
                OrderAmount orderAmount = EatApplication.realm.createObject(OrderAmount.class);
                orderAmount.setOrder(currentOrder);
                orderAmount.setDish(dish);
                orderAmount.setAmount(amount);
                currentOrder.getAmounts().add(orderAmount);
                dish.setOrderAmount(orderAmount);
                EatApplication.realm.commitTransaction();

                currentOrder.setPlace(dish.getPlace());

                dialogInterface.dismiss();

                if (dishActionsCallback != null) {
                    dishActionsCallback.dishAmountChanged(dish);
                }
                if (basketCountCallback != null) {
                    basketCountCallback.basketCountChanged();
                }
            }
        });

        alertDialogBuilder.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog alertDialog = (AlertDialog) alertDialogBuilder.create();
        alertDialog.show();
    }

    private void showTableOtherPlaceDialog(Context context, final Table table) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setMessage("Вы действительно хотите сделать заказ в данном заведении?\nВсе добавленные в корзину блюда из другого заведения будут удалены.");

        alertDialogBuilder.setPositiveButton("Да", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                Order currentOrder = getCurrentOrder();
                EatApplication.realm.beginTransaction();
                currentOrder.getAmounts().deleteAllFromRealm();
                currentOrder.setTable(table);
                EatApplication.realm.commitTransaction();

                currentOrder.setPlace(table.getPlace());
                currentOrder.setType(2);

                dialogInterface.dismiss();

                if (tableActionsCallback != null) {
                    tableActionsCallback.tableReserveChanged();
                }
                if (basketCountCallback != null) {
                    basketCountCallback.basketCountChanged();
                }
            }
        });

        alertDialogBuilder.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog alertDialog = (AlertDialog) alertDialogBuilder.create();
        alertDialog.show();
    }


    private TableActionsCallback tableActionsCallback;

    public interface TableActionsCallback {
        public void tableReserveChanged();
    }

    public void registerTableActionsListener(TableActionsCallback tableActionsCallback) {
        this.tableActionsCallback = tableActionsCallback;
    }


    private BasketCountCallback basketCountCallback;

    public interface BasketCountCallback {
        void basketCountChanged();
    }

    public void registerBasketCountListener(BasketCountCallback basketCountCallback) {
        this.basketCountCallback = basketCountCallback;
    }

    private FavouritesCountCallback favouritesCountCallback;

    public interface FavouritesCountCallback {
        void favouritesCountChanged();
    }

    public void registerFavouritesCountListener(FavouritesCountCallback favouritesCountCallback) {
        this.favouritesCountCallback = favouritesCountCallback;
    }

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        //System.out.println("onTokenRefresh: " + refreshedToken);

        EatApplication.realm.beginTransaction();
        getSettings().setToken(refreshedToken);
        EatApplication.realm.commitTransaction();

        sentDeviceToken(refreshedToken);
    }

    public void sentDeviceToken(String token) {

        apiInterface.sentToken(token, "subscribe", String.valueOf(getSettings().getCurrentCity().getId())).enqueue(
                new Callback<RequestResult>() {
                    @Override
                    public void onResponse(Call<RequestResult> call, Response<RequestResult> response) {
                        System.out.println("tokenSent, result: " + response.body().getResult());
                    }

                    @Override
                    public void onFailure(Call<RequestResult> call, Throwable t) {
                        sendError("sentToken failed " + t.getMessage());
                    }
                });
    }

    public void sendError(String errorText) {
        System.out.println("sendError " + errorText);
        apiInterface.sendError(errorText).enqueue(new Callback<RequestResult>() {
            @Override
            public void onResponse(Call<RequestResult> call, Response<RequestResult> response) {
                System.out.println("errror sented");
            }

            @Override
            public void onFailure(Call<RequestResult> call, Throwable t) {
                System.out.println("sendError onFailure: " + t.getMessage());
            }
        });
    }

}
