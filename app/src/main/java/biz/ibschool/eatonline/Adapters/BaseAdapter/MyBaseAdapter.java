package biz.ibschool.eatonline.Adapters.BaseAdapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import biz.ibschool.eatonline.EatApplication;
import biz.ibschool.eatonline.Interfaces.Const;
import biz.ibschool.eatonline.Interfaces.FavouritePressed;
import biz.ibschool.eatonline.Models.Dish;
import biz.ibschool.eatonline.Models.Place;
import biz.ibschool.eatonline.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class MyBaseAdapter extends BaseAdapter {

    protected Context context;
    protected FavouritePressed favouritePressed;


    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        return null;
    }


    public void setDukeTypeface(TextView textView) {
        Typeface dukeTypeface;
        dukeTypeface = Typeface.createFromAsset(context.getAssets(), "duke.ttf");
        if (dukeTypeface != null) {
            textView.setTypeface(dukeTypeface);
        }
    }

    public void loadImage(String url, ImageView imageView, final ProgressBar progressBar) {

        progressBar.setVisibility(View.VISIBLE);
        Picasso.with(context)
                .load(Const.DOMAIN_NAME + url)
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                    @Override
                    public void onError() {
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                });

    }

    public void setButtonToFavourite(ImageView favouriteButton, boolean favourite) {
        if (favourite) {
            favouriteButton.setImageResource(R.drawable.ic_favourite_enabled);
        } else {
            favouriteButton.setImageResource(R.drawable.ic_favourite_disabled);
        }
    }

    private String getTimeForOpen(int openHour) {
        String beforeOpenText;
        int hoursForOpen = openHour + 23 - EatApplication.dataManager.getCurrentHour();
        if (hoursForOpen > 23) {
            hoursForOpen -= 24;
        }
        int minutes = 59 - EatApplication.dataManager.getCurrentMinutes();

        if (hoursForOpen > 0) {
            beforeOpenText = "До открытия\n" + hoursForOpen + "ч. " + minutes + "мин.";
        } else {
            beforeOpenText = "До открытия\n" + minutes + "мин.";
        }
        return beforeOpenText;
    }

    public void fillPlaceCell(View view, final Place place) {

        TextView titleTextView = (TextView) view.findViewById(R.id.place_cell_title);
        ImageView logoImageView = (ImageView) view.findViewById(R.id.place_cell_logo);
        ImageView actionImageView = (ImageView) view.findViewById(R.id.place_action_marker);
        ProgressBar logoProgress = (ProgressBar) view.findViewById(R.id.place_logo_progress);


        ImageView workTimeIcon = (ImageView) view.findViewById(R.id.place_cell_work_time_icon);
        TextView workTimeTextView = (TextView) view.findViewById(R.id.place_cell_work_time);

        ImageView kitchensIcon = (ImageView) view.findViewById(R.id.place_cell_kitchens_icon);
        TextView kitchensTextView = (TextView) view.findViewById(R.id.place_cell_kitchens);

        ImageView paymentIcon = (ImageView) view.findViewById(R.id.place_cell_payment_icon);
        TextView paymentTextView = (TextView) view.findViewById(R.id.place_cell_payment);

        ImageView deliveryIcon = (ImageView) view.findViewById(R.id.place_cell_delivery_icon);
        TextView deliveryTextView = (TextView) view.findViewById(R.id.place_cell_delivery);

        ImageView favouriteButton = (ImageView) view.findViewById(R.id.place_to_favourite_button);

        //View closedFadeView = (View) view.findViewById(R.id.closed_fade_view);
        TextView closedTextView = (TextView) view.findViewById(R.id.closed_text);

        titleTextView.setText(place.getTitle());
        setDukeTypeface(titleTextView);
        loadImage(place.getLogo(), logoImageView, logoProgress);
        workTimeTextView.setText(place.getWtime());
        kitchensTextView.setText(place.getKitchens());

        String paymentString = "Наличными";
        if (place.isCourierPayment()) {
            paymentString += " / картой курьеру";
        }
        if (place.isSberPayment()) {
            paymentString += " / сбербанк онлайн";
        }
        paymentTextView.setText(paymentString);

        setButtonToFavourite(favouriteButton, place.isFavourite());

        favouriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                favouritePressed.switchPlaceFavourite(place);
            }
        });

        if (place.getAction() != null) {
            actionImageView.setVisibility(View.VISIBLE);
            switch (place.getAction().getType()) {
                case 33:
                    actionImageView.setImageResource(R.drawable.ic_marker_action);
                    break;
                case 34:
                    actionImageView.setImageResource(R.drawable.ic_marker_sale);
                    break;
                case 62:
                    actionImageView.setImageResource(R.drawable.ic_marker_news);
                    break;
            }
        } else {
            actionImageView.setVisibility(View.INVISIBLE);
        }

        String deliveryString = "бесплатно";
        if (place.getDprice() > 0) {
            deliveryString = place.getDprice() + Const.PRICE_UNITS;
        } else if (place.getDfrom() > 0) {
            deliveryString = "бесплатно, при заказе от " + place.getDfrom() + Const.PRICE_UNITS;
        }
        deliveryTextView.setText("Доставка: " + deliveryString + Const.BY_CITY);

        boolean fade = false;
        if (place.isOutput()) {
            closedTextView.setVisibility(View.VISIBLE);
            closedTextView.setText("В заведении \nвыходной день");
            fade = true;
        } else if (EatApplication.dataManager.isPlaceClosed(place)) {
            closedTextView.setVisibility(View.VISIBLE);
            closedTextView.setText(getTimeForOpen(place.getOphour()));
            fade = true;
        } else {
            closedTextView.setVisibility(View.INVISIBLE);
        }

        if (fade) {
            logoImageView.setAlpha((float) 0.3);
            actionImageView.setAlpha((float) 0.3);
            titleTextView.setAlpha((float) 0.3);

            workTimeIcon.setAlpha((float) 0.3);
            workTimeTextView.setAlpha((float) 0.3);
            kitchensIcon.setAlpha((float) 0.3);
            kitchensTextView.setAlpha((float) 0.3);
            paymentIcon.setAlpha((float) 0.3);
            paymentTextView.setAlpha((float) 0.3);
            deliveryIcon.setAlpha((float) 0.3);
            deliveryTextView.setAlpha((float) 0.3);
            favouriteButton.setAlpha((float) 0.3);
        } else {
            logoImageView.setAlpha((float) 1);
            actionImageView.setAlpha((float) 1);
            titleTextView.setAlpha((float) 1);

            workTimeIcon.setAlpha((float) 1);
            workTimeTextView.setAlpha((float) 1);
            kitchensIcon.setAlpha((float) 1);
            kitchensTextView.setAlpha((float) 1);
            paymentIcon.setAlpha((float) 1);
            paymentTextView.setAlpha((float) 1);
            deliveryIcon.setAlpha((float) 1);
            deliveryTextView.setAlpha((float) 1);
            favouriteButton.setAlpha((float) 1);
        }

    }

    public void fillSmallDishCell(View view, final Dish dish) {

        ImageView dishImageView = (ImageView) view.findViewById(R.id.dish_small_cell_image);
        ProgressBar dishImageProgress = (ProgressBar) view.findViewById(R.id.dish_image_progress);
        TextView dishTitleText =  (TextView) view.findViewById(R.id.dish_small_cell_title);
        TextView placeTitleText =  (TextView) view.findViewById(R.id.dish_small_cell_place);
        TextView weightText =  (TextView) view.findViewById(R.id.dish_small_cell_weight);
        TextView priceText =  (TextView) view.findViewById(R.id.dish_small_cell_price);
        ImageView favouriteButton = (ImageView) view.findViewById(R.id.dish_to_favourite_button);

        loadImage(dish.getImg(), dishImageView, dishImageProgress);
        dishTitleText.setText(dish.getTitle());
        placeTitleText.setText(dish.getPlace().getType() + " " + dish.getPlace().getTitle());
        weightText.setText(String.valueOf((int) dish.getWeight() + " " + dish.getUnits()));
        priceText.setText(dish.getPrice() + " " + Const.PRICE_UNITS);
        setDukeTypeface(priceText);
        setButtonToFavourite(favouriteButton, dish.isFavourite());

        favouriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                favouritePressed.switchDishFavourite(dish);
            }
        });

    }

}
