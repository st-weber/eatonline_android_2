package biz.ibschool.eatonline.Adapters.BaseAdapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import biz.ibschool.eatonline.Interfaces.FavouritePressed;
import biz.ibschool.eatonline.Models.Dish;
import biz.ibschool.eatonline.R;

import java.util.ArrayList;

public class SmallDishesListAdapter extends MyBaseAdapter {

    private ArrayList<Dish> dishes;

    public SmallDishesListAdapter(Context context, ArrayList<Dish> dishes, FavouritePressed favouritePressed) {
        this.context = context;
        this.dishes = dishes;
        this.favouritePressed = favouritePressed;
    }

    @Override
    public int getCount() {
        return dishes.size();
    }

    @Override
    public Object getItem(int i) {
        return dishes.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {

        View view = convertView;
        if (view == null) {
            view = View.inflate(viewGroup.getContext(), R.layout.cell_small_dish, null);
        }
        fillSmallDishCell(view, dishes.get(i));

        return view;
    }

}
