package biz.ibschool.eatonline.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import biz.ibschool.eatonline.EatApplication;
import biz.ibschool.eatonline.Fragments.DetailDishFragment;
import biz.ibschool.eatonline.Fragments.DetailTableFragment;
import biz.ibschool.eatonline.Fragments.PlaceInfoFragment;
import biz.ibschool.eatonline.Fragments.PlaceZonesFragment;
import biz.ibschool.eatonline.Fragments.TabFragment;
import biz.ibschool.eatonline.Models.Dish;
import biz.ibschool.eatonline.Models.Place;
import biz.ibschool.eatonline.Models.Table;

import java.util.ArrayList;


public class BasePagerAdapter extends FragmentStatePagerAdapter {

    //--> decide how many page will be
    private final Fragment[] mFragments;
    private String pageClass;
    //private Place transferPlace;

    public BasePagerAdapter(ArrayList<Dish> dishes, FragmentManager fm, int orderType) {
        super(fm);
        mFragments = new Fragment[dishes.size()];

        boolean placeClosed = false;
        if (dishes.size() > 0) {
            Place place = dishes.get(0).getPlace();
            placeClosed = EatApplication.dataManager.isPlaceClosed(place) || place.isOutput();
        }

        int i = 0;
        for (Dish dish : dishes) {
            DetailDishFragment dishFragment = new DetailDishFragment();
            dishFragment.setDish(dish);
            dishFragment.setOrderType(orderType);
            dishFragment.setPlaceClosed(placeClosed);
            mFragments[i] = dishFragment;
            i++;
        }
    }

    public BasePagerAdapter(ArrayList<Table> tables, FragmentManager fm) {
        super(fm);
        mFragments = new Fragment[tables.size()];

        boolean placeClosed = false;
        if (tables.size() > 0) {
            Place place = tables.get(0).getPlace();
            placeClosed = EatApplication.dataManager.isPlaceClosed(place) || place.isOutput();
        }

        int i = 0;
        for (Table table : tables) {
            DetailTableFragment tableFragment = new DetailTableFragment();
            tableFragment.setTable(table);
            tableFragment.setPlaceClosed(placeClosed);
            mFragments[i] = tableFragment;
            i++;
        }
    }

    public BasePagerAdapter(FragmentManager fm, ArrayList<String> tabNames) {
        super(fm);
        mFragments = new Fragment[tabNames.size()];
        int i = 0;
        for (String tabName : tabNames) {
            TabFragment tabFragment = new TabFragment();
            tabFragment.tabName = tabName;
            mFragments[i] = tabFragment;
            i++;
        }
    }

    public BasePagerAdapter(FragmentManager fm, Place place, int startTypeId, int startDishId) {
        super(fm);
        mFragments = new Fragment[2];

        PlaceInfoFragment placeInfoFragment = new PlaceInfoFragment();
        placeInfoFragment.setPlace(place);
        placeInfoFragment.setStartTypeId(startTypeId);
        placeInfoFragment.setStartDishId(startDishId);
        mFragments[0] = placeInfoFragment;

        PlaceZonesFragment placeZonesFragment = new PlaceZonesFragment();
        placeZonesFragment.setPlace(place);
        mFragments[1] = placeZonesFragment;

    }

    @Override
    public Fragment getItem(int position) {
        return mFragments[position];
    }

    @Override
    public int getCount() {
        return mFragments.length;
    }

}