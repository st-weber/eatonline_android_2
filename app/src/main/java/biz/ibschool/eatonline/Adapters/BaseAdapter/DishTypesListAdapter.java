package biz.ibschool.eatonline.Adapters.BaseAdapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import biz.ibschool.eatonline.Models.DishType;
import biz.ibschool.eatonline.R;

import java.util.ArrayList;

/**
 * Created by michanic on 17.04.17.
 */

public class DishTypesListAdapter extends MyBaseAdapter {

    private ArrayList<DishType> dishTypes;

    public DishTypesListAdapter(Context context, ArrayList<DishType> dishTypes) {
        this.context = context;
        this.dishTypes = dishTypes;
    }

    @Override
    public int getCount() {
        return dishTypes.size();
    }

    @Override
    public DishType getItem(int i) {
        return dishTypes.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        View view = convertView;
        if (view == null) {
            view = View.inflate(viewGroup.getContext(), R.layout.cell_dish_type, null);
        }
        DishType dishType = getItem(i);

        ImageView dishImageView = (ImageView) view.findViewById(R.id.dish_type_image);
        ProgressBar imageProgress = (ProgressBar) view.findViewById(R.id.dish_type_progress);
        TextView titleView = (TextView) view.findViewById(R.id.dish_type_title);
        TextView placesNumView = (TextView) view.findViewById(R.id.dish_type_places_num);

        loadImage(getItem(i).getImage(), dishImageView, imageProgress);
        titleView.setText(dishType.getName());

        System.out.println(dishType.getName() + dishType.getImage());
        setDukeTypeface(titleView);
        placesNumView.setText(dishType.getPlaces().size() + " заведений");

        return view;
    }
}
