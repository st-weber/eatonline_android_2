package biz.ibschool.eatonline.Adapters.BaseAdapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import biz.ibschool.eatonline.EatApplication;
import biz.ibschool.eatonline.Models.City;
import biz.ibschool.eatonline.R;

import java.util.ArrayList;

public class CitiesListAdapter extends MyBaseAdapter {

    ArrayList<City> cities;

    public CitiesListAdapter(Context context, ArrayList<City> cities) {
        this.context = context;
        this.cities = cities;
    }

    @Override
    public int getCount() {
        return cities.size();
    }

    @Override
    public City getItem(int position) {
        return cities.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup ViewGroup) {

        View view = convertView;
        if (view == null) {
            view = View.inflate(ViewGroup.getContext(), R.layout.cell_city, null);
        }

        City city = getItem(position);

        TextView cityTitleView = (TextView) view.findViewById(R.id.city_title);
        ImageView checkedImage = (ImageView) view.findViewById(R.id.city_checked);

        if (city.getPar() == 0) {
            cityTitleView.setText(city.getName());
            cityTitleView.setTextColor(ContextCompat.getColor(context, R.color.colorDarkText));
            cityTitleView.setTextSize(16);
        } else {
            cityTitleView.setText("   " + city.getName());
            cityTitleView.setTextColor(ContextCompat.getColor(context, R.color.colorListText));
            cityTitleView.setTextSize(14);
        }

        if (EatApplication.dataManager.getSettings().getCurrentCity() != null)
            checkedImage.setVisibility((city.getId() == EatApplication.dataManager.getSettings().getCurrentCity().getId()) ? View.VISIBLE : View.INVISIBLE);

        return view;
    }

}
