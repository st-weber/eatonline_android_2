package biz.ibschool.eatonline.Adapters.BaseAdapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import biz.ibschool.eatonline.EatApplication;
import biz.ibschool.eatonline.Interfaces.Const;
import biz.ibschool.eatonline.Interfaces.DishActions;
import biz.ibschool.eatonline.Models.Dish;
import biz.ibschool.eatonline.Models.Order;
import biz.ibschool.eatonline.Models.OrderAmount;
import biz.ibschool.eatonline.Models.Table;
import biz.ibschool.eatonline.R;

import java.util.ArrayList;

public class BasketListAdapter extends MyBaseAdapter {

    ArrayList<OrderAmount> amounts;
    public DishActions dishActions;

    public BasketListAdapter(Context context, ArrayList<OrderAmount> orderAmounts) {
        this.context = context;
        this.amounts = orderAmounts;
    }

    public void setDishActionsListener(DishActions dishActions) {
        this.dishActions = dishActions;
    }

    @Override
    public int getCount() {
        if (EatApplication.dataManager.getCurrentOrder().getTable() == null) {
            return amounts.size();
        } else {
            return amounts.size() + 1;
        }
    }

    @Override
    public OrderAmount getItem(int position) {
        return amounts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Order currentOrder = EatApplication.dataManager.getCurrentOrder();

        if (position == getCount() - 1 && currentOrder.getTable() != null) {

            View view = View.inflate(parent.getContext(), R.layout.cell_table, null);

            TextView title = (TextView) view.findViewById(R.id.cell_basket_title);
            TextView price = (TextView) view.findViewById(R.id.cell_basket_price);
            final ImageView trash = (ImageView) view.findViewById(R.id.cell_basket_trash);
            View fadeView = (View) view.findViewById(R.id.dish_is_unavailable);
            TextView notAvailable = (TextView) view.findViewById(R.id.wrong_service);

            final Table table = currentOrder.getTable();
            title.setText(table.getName());
            price.setText(table.getPrice() + " р.");
            setDukeTypeface(price);

            if (currentOrder.getType() == 2) {
                fadeView.setVisibility(View.GONE);
                notAvailable.setVisibility(View.GONE);
            } else {
                fadeView.setVisibility(View.VISIBLE);
                notAvailable.setVisibility(View.VISIBLE);
            }

            trash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    trash.setClickable(false);
                    EatApplication.dataManager.setTableToCurrentOrder(context, null);
                }
            });

            return view;

        } else {

            View view = View.inflate(parent.getContext(), R.layout.cell_basket, null);

            OrderAmount amount = getItem(position);
            final Dish dish = amount.getDish();

            TextView title = (TextView) view.findViewById(R.id.cell_basket_title);
            TextView weight = (TextView) view.findViewById(R.id.cell_basket_weight);
            TextView price = (TextView) view.findViewById(R.id.cell_basket_price);
            final TextView minusButton = (TextView) view.findViewById(R.id.button_minus);
            final TextView plusButton = (TextView) view.findViewById(R.id.button_plus);
            TextView count = (TextView) view.findViewById(R.id.cell_count);
            final ImageView trash = (ImageView) view.findViewById(R.id.cell_basket_trash);
            View dishUnavailable = (View) view.findViewById(R.id.dish_is_unavailable);
            TextView serviceView = (TextView) view.findViewById(R.id.wrong_service);

            title.setText(amount.getDish().getTitle());
            weight.setText(String.valueOf((int) amount.getDish().getWeight() * amount.getAmount() + " " + amount.getDish().getUnits()));
            setDukeTypeface(price);
            price.setText(amount.getAmount() * amount.getDish().getPrice() + " " + Const.PRICE_UNITS);
            count.setText(amount.getAmount() + " шт.");

            plusButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    plusButton.setClickable(false);
                    dishActions.plusPressed(dish);
                }
            });
            minusButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    minusButton.setClickable(false);
                    dishActions.minusPressed(dish);
                }
            });
            trash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    trash.setClickable(false);
                    dishActions.deletePressed(dish);
                }
            });

            boolean orderAllowed = true;
            switch (currentOrder.getType()) {
                case 0:
                    orderAllowed = dish.getDel().equals("1");
                    if (!orderAllowed) {
                        serviceView.setText("НЕ ДОСТАВЛЯЕТСЯ");
                    }
                    break;
                case 1:
                    orderAllowed = dish.getTkw().equals("1");
                    if (!orderAllowed) {
                        serviceView.setText("НЕ ПОДАЕТСЯ");
                    }
                    break;
                case 2:
                    orderAllowed = dish.getRes().equals("1");
                    if (!orderAllowed) {
                        serviceView.setText("НЕ ПОДАЕТСЯ");
                    }
                    break;
                default:
                    orderAllowed = true;
                    break;
            }

            if (!orderAllowed) {
                dishUnavailable.setVisibility(View.VISIBLE);
                serviceView.setVisibility(View.VISIBLE);
                count.setVisibility(View.GONE);
                plusButton.setVisibility(View.GONE);
                minusButton.setVisibility(View.GONE);
            } else {
                dishUnavailable.setVisibility(View.GONE);
                serviceView.setVisibility(View.GONE);
                count.setVisibility(View.VISIBLE);
                plusButton.setVisibility(View.VISIBLE);
                minusButton.setVisibility(View.VISIBLE);
            }

            return view;
        }
    }
}
