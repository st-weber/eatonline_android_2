package biz.ibschool.eatonline.Adapters.BaseAdapter;

import android.content.Context;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import biz.ibschool.eatonline.DataManager;
import biz.ibschool.eatonline.EatApplication;
import biz.ibschool.eatonline.Interfaces.Const;
import biz.ibschool.eatonline.Interfaces.OrderActions;
import biz.ibschool.eatonline.Models.Order;
import biz.ibschool.eatonline.R;

import java.util.ArrayList;

public class OrdersListAdapter extends MyBaseAdapter {

    private ArrayList<Order> orders;
    private OrderActions orderActions;

    public OrdersListAdapter(Context context, ArrayList<Order> orders, OrderActions orderActions) {
        this.context = context;
        this.orders = orders;
        this.orderActions = orderActions;
    }

    public void updateOrders (ArrayList<Order> orders) {
        this.orders = orders;
    }

    @Override
    public int getCount() {
        return orders.size();
    }

    @Override
    public Object getItem(int position) {
        return orders.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, final View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            view = View.inflate(parent.getContext(), R.layout.cell_order, null);
        }

        TextView title = (TextView) view.findViewById(R.id.title);
        TextView status = (TextView) view.findViewById(R.id.status);
        TextView price = (TextView) view.findViewById(R.id.price);
        TextView time = (TextView) view.findViewById(R.id.time);
        final ImageView trash = (ImageView) view.findViewById(R.id.cell_delete);
        final ProgressBar checkProgress = (ProgressBar) view.findViewById(R.id.progress);
        final Button button = (Button) view.findViewById(R.id.button);
        button.setClickable(true);

        final Order order = orders.get(position);

        if (order.getStatus() == 0) {
            title.setText(order.getPlace().getType() + " " + order.getPlace().getTitle());
        } else {
            title.setText("№ " + order.getNumber() + " - " + order.getPlace().getType() + " " + order.getPlace().getTitle());
        }

        trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EatApplication.realm.beginTransaction();
                order.deleteFromRealm();
                EatApplication.realm.commitTransaction();
                orderActions.orderDeleted();
            }
        });


        price.setText(EatApplication.dataManager.getSummOrder(order) + " " + Const.PRICE_UNITS);
        setDukeTypeface(price);
        time.setText(order.getDate());

        checkProgress.setVisibility(View.GONE);
        checkProgress.getIndeterminateDrawable().setColorFilter(
                Color.WHITE, android.graphics.PorterDuff.Mode.SRC_IN);

        switch (order.getStatus()) {
            case 0:
                status.setText("не оформлен");
                status.setTextColor(ContextCompat.getColor(context, R.color.colorStatusGray));

                time.setVisibility(View.GONE);
                button.setText("ОФОРМИТЬ");

                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        orderActions.checkoutCurrentOrder();
                    }
                });
                break;
            case 1:
                status.setText("обрабатывается");
                status.setTextColor(ContextCompat.getColor(context, R.color.colorStatusBlue));

                time.setVisibility(View.VISIBLE);
                button.setText("ПРОВЕРИТЬ");

                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        button.setText("");
                        button.setClickable(false);
                        checkProgress.setVisibility(View.VISIBLE);

                        CountDownTimer timer = new CountDownTimer(1000, 1000) {
                            @Override
                            public void onTick(long millisUntilFinished) {

                            }
                            @Override
                            public void onFinish() {
                                EatApplication.dataManager.checkOrderStatus(order, orderActions, new DataManager.RequestInterface() {
                                    @Override
                                    public void onSuccess() {

                                    }
                                    @Override
                                    public void onFailed() {
                                        if (button != null) {
                                            button.setText("ПРОВЕРИТЬ");
                                            button.setClickable(true);
                                        }
                                    }
                                });
                            }
                        }.start();
                    }
                });
                break;
            case 2:
                status.setText("принят");
                status.setTextColor(ContextCompat.getColor(context, R.color.colorStatusGreen));

                time.setVisibility(View.VISIBLE);
                button.setText("ПОВТОРИТЬ");

                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        orderActions.repeatOrder(order);
                    }
                });
                break;
            case 3:
                status.setText("отменен");
                status.setTextColor(ContextCompat.getColor(context, R.color.colorStatusRed));

                time.setVisibility(View.VISIBLE);
                button.setText("ПОВТОРИТЬ");

                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        orderActions.repeatOrder(order);
                    }
                });
                break;
        }
        return view;

    }
}
