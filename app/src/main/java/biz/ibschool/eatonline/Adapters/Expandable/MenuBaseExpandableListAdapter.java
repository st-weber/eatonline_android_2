package biz.ibschool.eatonline.Adapters.Expandable;

import android.content.Context;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import biz.ibschool.eatonline.EatApplication;
import biz.ibschool.eatonline.Interfaces.Const;
import biz.ibschool.eatonline.Interfaces.DishActions;
import biz.ibschool.eatonline.Interfaces.FavouritePressed;
import biz.ibschool.eatonline.Models.Dish;
import biz.ibschool.eatonline.Models.Order;
import biz.ibschool.eatonline.Models.OrderAmount;
import biz.ibschool.eatonline.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * Created by michanic on 29.08.17.
 */

public class MenuBaseExpandableListAdapter extends BaseExpandableListAdapter {

    public Context context;
    public FavouritePressed favouritePressed;
    public DishActions dishActions;
    public int currentService;
    protected boolean placeClosed;
    protected Dish activeDish = null;

    public void setActiveDish(Dish activeDish) {
        this.activeDish = activeDish;
    }

    public void setDukeTypeface(TextView textView) {
        Typeface dukeTypeface;
        dukeTypeface = Typeface.createFromAsset(context.getAssets(), "duke.ttf");
        if (dukeTypeface != null) {
            textView.setTypeface(dukeTypeface);
        }
    }

    public void setFavouritePressedListener(FavouritePressed favouritePressed) {
        this.favouritePressed = favouritePressed;
    }

    public void setDishActionsListener(DishActions dishActions) {
        this.dishActions = dishActions;
    }

    public void loadImage(String url, ImageView imageView, final ProgressBar progressBar) {

        progressBar.setVisibility(View.VISIBLE);
        Picasso.with(context)
                .load(Const.DOMAIN_NAME + url)
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        progressBar.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onError() {
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                });

    }

    public void setButtonToFavourite(ImageView favouriteButton, boolean favourite) {
        if (favourite) {
            favouriteButton.setImageResource(R.drawable.ic_favourite_enabled);
        } else {
            favouriteButton.setImageResource(R.drawable.ic_favourite_disabled);
        }
    }


    private void checkCount(Dish dish, TextView orderButton, TextView plusButton, TextView minusButton) {

        Order currentOrder = EatApplication.dataManager.getCurrentOrder();

        if (currentOrder.getPlace() != null && currentOrder.getPlace().getId() == dish.getPlace().getId()) {
            OrderAmount dishAmount = currentOrder.getAmounts().where().equalTo("dish.id", dish.getId()).findFirst();
            if (dishAmount == null) {
                orderButton.setText("ЗАКАЗАТЬ");
                orderButton.setClickable(true);
                plusButton.setVisibility(View.GONE);
                minusButton.setVisibility(View.GONE);
            } else {
                orderButton.setText(dishAmount.getAmount() + " шт.");
                orderButton.setClickable(false);
                plusButton.setVisibility(View.VISIBLE);
                minusButton.setVisibility(View.VISIBLE);
            }

        } else {
            orderButton.setText("ЗАКАЗАТЬ");
            orderButton.setClickable(true);
            plusButton.setVisibility(View.GONE);
            minusButton.setVisibility(View.GONE);
        }
    }

    public void fillMenuDishCell(View view, final Dish dish) {

        ImageView dishImageView = (ImageView) view.findViewById(R.id.cell_dish_basket_image);
        ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.cell_dish_progress);
        TextView titleView = (TextView) view.findViewById(R.id.cell_dish_basket_title);
        TextView detailsView = (TextView) view.findViewById(R.id.cell_dish_details);

        TextView weightView = (TextView) view.findViewById(R.id.cell_dish_basket_weight);
        TextView weightDetailsView = (TextView) view.findViewById(R.id.cell_dish_basket_weight_details);
        TextView priceView = (TextView) view.findViewById(R.id.cell_dish_basket_price);
        final ImageView favouriteButton = (ImageView) view.findViewById(R.id.dish_to_favourite_button);
        final TextView orderButton = (TextView) view.findViewById(R.id.cell_dish_order);
        final TextView plusButton = (TextView) view.findViewById(R.id.button_plus);
        final TextView minusButton = (TextView) view.findViewById(R.id.button_minus);
        TextView serviceView = (TextView) view.findViewById(R.id.wrong_service);

        loadImage(dish.getImg(), dishImageView, progressBar);
        titleView.setText(dish.getTitle());
        detailsView.setText(dish.getDescr());

        weightView.setText(String.valueOf((int) dish.getWeight() + " " + dish.getUnits()));
        weightDetailsView.setText(dish.getDweight());

        RelativeLayout.LayoutParams weightParams = (RelativeLayout.LayoutParams) weightDetailsView.getLayoutParams();
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        float dp = 14f;
        if (dish.getDweight() != null) {
            weightParams.height = (int) (metrics.density * dp);
        } else {
            weightParams.height = 0;
        }
        weightDetailsView.setLayoutParams(weightParams);

        priceView.setText(dish.getPrice() + " " + Const.PRICE_UNITS);
        setDukeTypeface(priceView);
        setButtonToFavourite(favouriteButton, dish.isFavourite());

        favouriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                favouriteButton.setClickable(false);
                favouritePressed.switchDishFavourite(dish);
            }
        });

        orderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderButton.setClickable(false);
                dishActions.addPressed(dish);
            }
        });
        plusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                plusButton.setClickable(false);
                dishActions.plusPressed(dish);
            }
        });
        minusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                minusButton.setClickable(false);
                dishActions.minusPressed(dish);
            }
        });

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dishActions.dishDetailPressed(dish);
            }
        });

        boolean orderAllowed = true;
        switch (currentService) {
            case 0:
                orderAllowed = dish.getDel().equals("1");
                if (!orderAllowed) {
                    serviceView.setText("НЕ ДОСТАВЛЯЕТСЯ");
                }
                break;
            case 1:
                orderAllowed = dish.getTkw().equals("1");
                if (!orderAllowed) {
                    serviceView.setText("НЕ ПОДАЕТСЯ");
                }
                break;
            case 2:
                orderAllowed = dish.getRes().equals("1");
                if (!orderAllowed) {
                    serviceView.setText("НЕ ПОДАЕТСЯ");
                }
                break;
            default:
                orderAllowed = true;
                break;
        }

        if (placeClosed) {
            serviceView.setText("ЗАКРЫТО");
        }

        if (placeClosed || !orderAllowed) {
            serviceView.setVisibility(View.VISIBLE);
            orderButton.setVisibility(View.GONE);
            plusButton.setVisibility(View.GONE);
            minusButton.setVisibility(View.GONE);
        } else {
            serviceView.setVisibility(View.GONE);
            orderButton.setVisibility(View.VISIBLE);
            checkCount(dish, orderButton, plusButton, minusButton);
        }
    }

    @Override
    public int getGroupCount() {
        return 0;
    }

    @Override
    public int getChildrenCount(int i) {
        return 0;
    }

    @Override
    public Object getGroup(int i) {
        return null;
    }

    @Override
    public Object getChild(int i, int i1) {
        return null;
    }

    @Override
    public long getGroupId(int i) {
        return 0;
    }

    @Override
    public long getChildId(int i, int i1) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        return null;
    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        return null;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }

}
