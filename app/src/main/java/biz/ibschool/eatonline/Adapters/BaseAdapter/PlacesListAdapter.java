package biz.ibschool.eatonline.Adapters.BaseAdapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import biz.ibschool.eatonline.Interfaces.FavouritePressed;
import biz.ibschool.eatonline.Models.Place;
import biz.ibschool.eatonline.R;

import java.util.ArrayList;

public class PlacesListAdapter extends MyBaseAdapter {

    private ArrayList<Place> places;

    public PlacesListAdapter(Context context, ArrayList<Place> places, FavouritePressed favouritePressed) {
        this.favouritePressed = favouritePressed;
        this.context = context;
        this.places = places;
    }

    @Override
    public int getCount() {
        return places.size();
    }

    @Override
    public Place getItem(int i) {
        return places.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {

        View view = convertView;
        if (view == null) {
            view = View.inflate(viewGroup.getContext(), R.layout.cell_place, null);
        }
        fillPlaceCell(view, getItem(i));

        return view;
    }



}
