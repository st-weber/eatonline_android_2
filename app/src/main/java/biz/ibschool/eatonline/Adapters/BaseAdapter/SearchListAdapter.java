package biz.ibschool.eatonline.Adapters.BaseAdapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import biz.ibschool.eatonline.Interfaces.FavouritePressed;
import biz.ibschool.eatonline.Models.Dish;
import biz.ibschool.eatonline.Models.Place;
import biz.ibschool.eatonline.R;

import java.util.ArrayList;

public class SearchListAdapter extends MyBaseAdapter {

    private ArrayList resultList = new ArrayList();
    private int placesCount;

    public SearchListAdapter(Context context, FavouritePressed favouritePressed) {
        this.context = context;
        this.resultList = new ArrayList();
        this.placesCount = 0;
        this.favouritePressed = favouritePressed;
    }

    public void updateData(ArrayList<Place> places, ArrayList<Dish> dishes) {
        this.placesCount = places.size();

        resultList = new ArrayList();
        resultList.addAll(places);
        resultList.addAll(dishes);
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    public boolean itemIsPlace(int position) {
        return (position < placesCount);
    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Override
    public Object getItem(int position) {
        return resultList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view;

        if (itemIsPlace(position)) {

            view = View.inflate(parent.getContext(), R.layout.cell_place, null);
            fillPlaceCell(view, (Place) resultList.get(position));

        } else {

            view = View.inflate(parent.getContext(), R.layout.cell_small_dish, null);
            fillSmallDishCell(view, (Dish) resultList.get(position));

        }
        return view;

    }
}
