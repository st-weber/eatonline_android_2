package biz.ibschool.eatonline.Adapters.Expandable;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import biz.ibschool.eatonline.Activities.AddressPickerActivity;
import biz.ibschool.eatonline.EatApplication;
import biz.ibschool.eatonline.Interfaces.Const;
import biz.ibschool.eatonline.Interfaces.PlacePageActions;
import biz.ibschool.eatonline.Models.Dish;
import biz.ibschool.eatonline.Models.DishType;
import biz.ibschool.eatonline.Models.Order;
import biz.ibschool.eatonline.Models.Place;
import biz.ibschool.eatonline.R;

import java.util.ArrayList;
import java.util.HashMap;

public class TypesExpandableListAdapter extends MenuBaseExpandableListAdapter
{
    private ArrayList<DishType> types;
    private Place place;
    private LayoutInflater lInflater;
    private HashMap<String, SubtypesExpandableListAdapter> subtypesExpandableListAdapters;
    private PlacePageActions placePageActions;
    private int currentOpenedType;
    private ExpandableListView expandableParent;
    private boolean startWithType = false;


    public TypesExpandableListAdapter(ArrayList<DishType> dishTypes, HashMap<String, SubtypesExpandableListAdapter> subtypesExpandableListAdapters, Place place, Context context, int currentService) {
        types = dishTypes;
        this.subtypesExpandableListAdapters = subtypesExpandableListAdapters;
        this.place = place;
        this.context = context;
        this.currentService = currentService;
        lInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.placeClosed = EatApplication.dataManager.isPlaceClosed(place) || place.isOutput();
    }

    public void setCurrentOpenedTypeIndex(int currentOpenedType) {
        this.currentOpenedType = currentOpenedType;
        this.startWithType = (currentOpenedType > 0);
    }

    public void setPlaceActionsListener(PlacePageActions placePageActions) {
        this.placePageActions = placePageActions;
    }


    @Override
    public Object getChild(int arg0, int arg1)
    {
        return arg1;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition)
    {
        //
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent)
    {

        if (groupPosition == 1) {
            View view = lInflater.inflate(R.layout.cell_choose_table, parent, false);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    placePageActions.chooseTablePressed();
                }
            });
            return view;
        } else {
            DishType dishType = types.get(groupPosition - 2);

            if (subtypesExpandableListAdapters.containsKey(String.valueOf(dishType.getId()))) { // если есть подтипы

                CustExpandableListAdapter secondLevelAdapter = new CustExpandableListAdapter(context);
                SubtypesExpandableListAdapter subtypesExpandableListAdapter = subtypesExpandableListAdapters.get(String.valueOf(dishType.getId()));
                secondLevelAdapter.setAdapter(subtypesExpandableListAdapter);
                secondLevelAdapter.setGroupIndicator(null);
                return secondLevelAdapter;

            } else { // если нет подтипов

                View view = lInflater.inflate(R.layout.cell_menu_dish, parent, false);

                ArrayList dishes = new ArrayList(place.getDishes().where().equalTo("type", String.valueOf(dishType.getId())).findAll().sort("sort"));
                Dish dish = (Dish) dishes.get(childPosition);
                fillMenuDishCell(view, dish);

                return view;

            }
        }
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        if (groupPosition == 0) {
            return 0;
        } else if (groupPosition == 1) {
            return 1;
        } else {
            DishType dishType = types.get(groupPosition - 2);
            if (subtypesExpandableListAdapters.containsKey(String.valueOf(dishType.getId()))) {
                return 1;
            } else { // если нет подтипов
                ArrayList dishes = new ArrayList(place.getDishes().where().equalTo("type", String.valueOf(dishType.getId())).findAll().sort("sort"));
                return dishes.size(); // вернем количество блюд родительского типа
            }
        }
    }

    @Override
    public Object getGroup(int groupPosition)
    {
        //
        return groupPosition;
    }

    @Override
    public int getGroupCount()
    {
        return types.size() + 2;
    }

    @Override
    public long getGroupId(int groupPosition)
    {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             final View convertView, ViewGroup parent)
    {
        expandableParent = (ExpandableListView)parent;

        if (groupPosition == 0) {

            View infoView = View.inflate(context, R.layout.place_info_cell, null);

            CardView logoCard = (CardView) infoView.findViewById(R.id.logo_card);

            ProgressBar backProgress = (ProgressBar) infoView.findViewById(R.id.back_progress);
            TextView workTimeText = (TextView) infoView.findViewById(R.id.info_work_time_text);
            TextView kitchensText = (TextView) infoView.findViewById(R.id.info_kitchen_text);
            TextView addressText = (TextView) infoView.findViewById(R.id.info_address_text);
            TextView deliveryText = (TextView) infoView.findViewById(R.id.info_delivery_text);
            Button deliveryButton = (Button) infoView.findViewById(R.id.info_delivery_button);
            TextView minimalText = (TextView) infoView.findViewById(R.id.info_minimal_text);
            TextView paymentText = (TextView) infoView.findViewById(R.id.info_payment_text);
            ImageView logoImageView = (ImageView) infoView.findViewById(R.id.place_logo_image);
            ProgressBar logoProgressBar = (ProgressBar) infoView.findViewById(R.id.place_logo_progress);
            ImageView actionImageView = (ImageView) infoView.findViewById(R.id.place_action_marker);
            ImageView backImageView = (ImageView) infoView.findViewById(R.id.place_back_image);

            workTimeText.setText(place.getWtime());
            kitchensText.setText(place.getKitchens());
            addressText.setText(place.getAddress());

            String deliveryPrice = "бесплатно";
            if (place.getDprice() > 0) {
                deliveryPrice = "от " + place.getDprice() + Const.PRICE_UNITS;
            } else {
                if (place.getDfrom() > 0) {
                    deliveryPrice = "бесплатно, при заказе от " + place.getDfrom() + Const.PRICE_UNITS;
                }
            }
            deliveryText.setText("Стоимость доставки: " + deliveryPrice);

            deliveryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, AddressPickerActivity.class);
                    intent.putExtra("placeId", place.getId());
                    context.startActivity(intent);
                    //startActivity(intent);
                }
            });

            minimalText.setText("Мин. заказ: от " + place.getMorder() + Const.PRICE_UNITS);


            String paymentString = "Наличными";
            if (place.isCourierPayment()) {
                paymentString += " / картой курьеру";
            }
            if (place.isSberPayment()) {
                paymentString += " / сбербанк онлайн";
            }
            paymentText.setText(paymentString);

            loadImage(place.getLogo(), logoImageView, logoProgressBar);

            if (place.getAction() != null) {
                actionImageView.setVisibility(View.VISIBLE);
                switch (place.getAction().getType()) {
                    case 33:
                        actionImageView.setImageResource(R.drawable.ic_marker_action);
                        break;
                    case 34:
                        actionImageView.setImageResource(R.drawable.ic_marker_sale);
                        break;
                    case 62:
                        actionImageView.setImageResource(R.drawable.ic_marker_news);
                        break;
                }

                logoCard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        placePageActions.showActionPressed(place.getAction());
                    }
                });

            } else {
                actionImageView.setVisibility(View.INVISIBLE);
            }

            loadImage(place.getBack(), backImageView, backProgress);


            final Order currentOrder = EatApplication.dataManager.getCurrentOrder();

            if (currentOrder.getPlace() != null) {
                if (currentOrder.getPlace().getId() == place.getId()) {
                    currentService = currentOrder.getType();
                } else {
                    currentOrder.setType(currentService);
                }
            }

            if (currentService == 2) {
                expandableParent.expandGroup(1, false);
            }

            if (currentOpenedType > 1) {
                //System.out.println("expandableParent.expandGroup " + currentOpenedType);
                expandableParent.expandGroup(currentOpenedType, false);
            }

            return infoView;

        } else if (groupPosition == 1) {

            View view = lInflater.inflate(R.layout.empty_cell, parent, false);
            return view;

        } else {

            View view = lInflater.inflate(R.layout.cell_menu_type, parent, false);

                    DishType dishType = types.get(groupPosition - 2);

            ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.image_progress);
            ImageView dishImageView = (ImageView) view.findViewById(R.id.cell_dish_type_image);
            TextView titleView = (TextView) view.findViewById(R.id.cell_dish_type_title);
            ImageView arrow = (ImageView) view.findViewById(R.id.cell_dish_type_arrow);

            progressBar.getIndeterminateDrawable().setColorFilter(Color.rgb(66, 66, 66), android.graphics.PorterDuff.Mode.SRC_IN);
            loadImage(dishType.getIcon(), dishImageView, progressBar);
            titleView.setText(dishType.getName());

            if (isExpanded) {
                arrow.setImageResource(R.drawable.ic_expand_less);
            } else {
                arrow.setImageResource(R.drawable.ic_expand_more);
            }

            return view;
        }

    }

    @Override
    public void onGroupExpanded(int groupPosition) {

        //System.out.println("TypesExpandableListAdapter onGroupExpanded " + groupPosition);

        if (!startWithType) {
            if (groupPosition > 1) {
                if (currentOpenedType > 0) {
                    expandableParent.collapseGroup(currentOpenedType);
                }
                currentOpenedType = groupPosition;
            }
        } else {
            if (groupPosition == currentOpenedType) {
                startWithType = false;
                if (activeDish != null) {
                    if (activeDish.getDishType().getParent() > 0) { // блюдо в подтипе
                        //SubtypesExpandableListAdapter subtypesExpandableListAdapter = subtypesExpandableListAdapters.get(String.valueOf(activeDish.getDishType().getParent()));
                        //subtypesExpandableListAdapter.expandStartSubtype();
                        activeDish = null;
                    }
                }

            }
        }
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        if (currentOpenedType == groupPosition) {
            currentOpenedType = -1;
        }
    }

    @Override
    public boolean hasStableIds()
    {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition)
    {
        return true;
    }

    private void checkChooseTableCell() {
        if (currentService == 2) {
            expandableParent.expandGroup(1, true);
        } else {
            expandableParent.collapseGroup(1);
        }
    }

    public void refreshMenu () {

        if (currentOpenedType > 1) {
            DishType dishType = types.get(currentOpenedType - 2);
            if (subtypesExpandableListAdapters.containsKey(String.valueOf(dishType.getId()))) { // если есть подтипы
                for (SubtypesExpandableListAdapter subtypesExpandableListAdapter : subtypesExpandableListAdapters.values()) {
                    subtypesExpandableListAdapter.setCurrentService(currentService);
                }
                SubtypesExpandableListAdapter openedSubtypesAdapter = subtypesExpandableListAdapters.get(String.valueOf(dishType.getId()));
                openedSubtypesAdapter.notifyDataSetChanged();
            } else {
                notifyDataSetChanged();
            }
        } else {
            notifyDataSetChanged();
        }

    }

}