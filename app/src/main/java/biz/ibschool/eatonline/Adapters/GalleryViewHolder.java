package biz.ibschool.eatonline.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import biz.ibschool.eatonline.R;

/**
 * Created by michanic on 14.09.17.
 */

public class GalleryViewHolder extends RecyclerView.ViewHolder {

    public View view;
    public ImageView image;
    public ImageView actionMarker;
    public ProgressBar progressBar;

    public GalleryViewHolder(View itemView) {
        super(itemView);

        view = itemView;
        image = (ImageView) itemView.findViewById(R.id.cell_image);
        actionMarker = (ImageView) itemView.findViewById(R.id.action_marker);
        progressBar = (ProgressBar) itemView.findViewById(R.id.progress);

    }

}
