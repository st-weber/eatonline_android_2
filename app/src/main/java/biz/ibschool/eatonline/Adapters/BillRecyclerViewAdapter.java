package biz.ibschool.eatonline.Adapters;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import biz.ibschool.eatonline.EatApplication;
import biz.ibschool.eatonline.Interfaces.Const;
import biz.ibschool.eatonline.Interfaces.GalleryActions;
import biz.ibschool.eatonline.Models.Action;
import biz.ibschool.eatonline.Models.GalleryImage;
import biz.ibschool.eatonline.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by michanic on 14.09.17.
 */

public class BillRecyclerViewAdapter extends RecyclerView.Adapter<GalleryViewHolder>  {

    private Context context;
    private ArrayList<Action> actions;
    private ArrayList<GalleryImage> galleryImages;
    private GalleryActions galleryActions;

    public BillRecyclerViewAdapter(ArrayList<Action> actions, Context context, GalleryActions galleryActions) {
        this.context = context;
        this.actions = actions;
        this.galleryActions = galleryActions;
    }

    public BillRecyclerViewAdapter(Context context, ArrayList<GalleryImage> galleryImages, GalleryActions galleryActions) {
        this.context = context;
        this.galleryImages = galleryImages;
        this.galleryActions = galleryActions;
    }


    @Override
    public GalleryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(parent.getContext(), R.layout.cell_action, null);
        GalleryViewHolder viewHolder = new GalleryViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final GalleryViewHolder holder, int position) {

        int width = (Resources.getSystem().getDisplayMetrics().widthPixels - EatApplication.appUtils.dpToPixels(15)) / 2;
        int height = width;
        String url = "";

        if (actions != null) {
            final Action action = actions.get(position);
            url = action.getImage();
            height = (int) (width * (float)action.getImageHeight() / (float)action.getImageWidth());

            switch (action.getType()) {
                case 33:
                    holder.actionMarker.setImageResource(R.drawable.ic_marker_action);
                    break;
                case 34:
                    holder.actionMarker.setImageResource(R.drawable.ic_marker_sale);
                    break;
                case 62:
                    holder.actionMarker.setImageResource(R.drawable.ic_marker_news);
                    break;
            }

            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    galleryActions.actionPressed(action);
                }
            });

        } else {
            final GalleryImage galleryImage = galleryImages.get(position);
            url = galleryImage.getLink();
            height = (int) (width * (float)galleryImage.getHeight() / (float)galleryImage.getWidth());

            holder.actionMarker.setVisibility(View.GONE);

            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    galleryActions.interriorPressed(galleryImage);
                }
            });
        }

        holder.image.getLayoutParams().width = width;
        holder.image.getLayoutParams().height = height;

        holder.progressBar.setVisibility(View.VISIBLE);
        Picasso.with(context)
                .load(Const.DOMAIN_NAME + url)
                .into(holder.image, new Callback() {
                    @Override
                    public void onSuccess() {
                        holder.progressBar.setVisibility(View.INVISIBLE);
                    }
                    @Override
                    public void onError() {
                        holder.progressBar.setVisibility(View.INVISIBLE);
                    }
                });
    }

    @Override
    public int getItemCount() {
        if (actions != null) {
            return actions.size();
        } else {
            return galleryImages.size();
        }
    }
}
