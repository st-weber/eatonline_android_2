package biz.ibschool.eatonline.Adapters.Expandable;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import biz.ibschool.eatonline.EatApplication;
import biz.ibschool.eatonline.Models.Dish;
import biz.ibschool.eatonline.Models.DishType;
import biz.ibschool.eatonline.Models.Place;
import biz.ibschool.eatonline.R;

import java.util.ArrayList;

public class SubtypesExpandableListAdapter extends biz.ibschool.eatonline.Adapters.Expandable.MenuBaseExpandableListAdapter {

    private ArrayList<DishType> subtypes;
    private Place place;
    private LayoutInflater lInflater;
    protected int currentOpenedSubtype;
    private ExpandableListView expandableParent;
    protected boolean startWithSubtype = false;

    public SubtypesExpandableListAdapter(ArrayList<DishType> subtypes, Place place, Context context, int currentService) {

        this.subtypes = subtypes;
        this.place = place;
        this.context = context;
        lInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        currentOpenedSubtype = -1;
        this.currentService = currentService;
        this.placeClosed = EatApplication.dataManager.isPlaceClosed(place) || place.isOutput();
    }

    public void setCurrentService(int currentService) {
        this.currentService = currentService;
    }

    public void setCurrentOpenedSubtype(int openedSubtypeID) {
        if (openedSubtypeID > 0) {
            int index = 0;
            for (DishType subtype : subtypes) {
                if (subtype.getId() == openedSubtypeID) {
                    this.currentOpenedSubtype = index;
                }
                index++;
            }
            this.startWithSubtype = true;
        }
    }

    public Object getChild(int groupPosition, int childPosition)
    {
        return childPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition)
    {
        return 1;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent)
    {

        View view = convertView;
        if (view == null || (TextView) view.findViewById(R.id.cell_dish_basket_title) == null) {
            view = lInflater.inflate(R.layout.cell_menu_dish, parent, false);
        }

        DishType subtype = subtypes.get(groupPosition);
        ArrayList dishes = new ArrayList(place.getDishes().where().equalTo("type", String.valueOf(subtype.getId())).findAll().sort("sort"));
        Dish dish = (Dish) dishes.get(childPosition);

        fillMenuDishCell(view, dish);

        return view;

    }

    @Override
    public int getChildrenCount(int groupPosition)
    {
        DishType subtype = subtypes.get(groupPosition);
        ArrayList dishes = new ArrayList(place.getDishes().where().equalTo("type", String.valueOf(subtype.getId())).findAll().sort("sort"));
        return dishes.size();
    }

    @Override
    public Object getGroup(int groupPosition)
    {
        return groupPosition;
    }

    @Override
    public int getGroupCount()
    {
        return subtypes.size();
    }

    @Override
    public long getGroupId(int groupPosition)
    {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent)
    {
        expandableParent = (ExpandableListView)parent;

        DishType subtype = subtypes.get(groupPosition);
        View view = convertView;
        if (view == null || (TextView) view.findViewById(R.id.cell_dish_type_title) == null) {
            view = lInflater.inflate(R.layout.cell_menu_subtype, parent, false);
        }

        TextView titleView = (TextView) view.findViewById(R.id.cell_dish_type_title);
        titleView.setText(subtype.getName());

        ImageView arrow = (ImageView) view.findViewById(R.id.cell_dish_type_arrow);
        if (isExpanded) {
            arrow.setImageResource(R.drawable.ic_expand_less);
        } else {
            arrow.setImageResource(R.drawable.ic_expand_more);
        }

        if (startWithSubtype && currentOpenedSubtype >= 0) {
            expandableParent.expandGroup(currentOpenedSubtype, false);
        }

        return view;
    }


    @Override
    public void onGroupExpanded(int groupPosition) {
        if (!startWithSubtype) {
            if (currentOpenedSubtype >= 0) {
                expandableParent.collapseGroup(currentOpenedSubtype);
            }
            currentOpenedSubtype = groupPosition;
        } else {
            if (groupPosition == currentOpenedSubtype) {
                startWithSubtype = false;
            }
        }
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        if (currentOpenedSubtype == groupPosition) {
            currentOpenedSubtype = -1;
        }
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void refreshData() {
        notifyDataSetChanged();
    }

}
