package biz.ibschool.eatonline.Models;

import com.google.gson.annotations.Expose;

public class AppPageText {

    @Expose
    private String text;

    public String getText() {
        return text;
    }
}

