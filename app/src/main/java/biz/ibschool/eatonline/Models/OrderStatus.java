package biz.ibschool.eatonline.Models;

public class OrderStatus {
    private String status;
    public int getStatusInt() {
        if (status.equals("N")) { // обрабатывается
            return 1;
        } else if (status.equals("P")) {  // принят
            return 2;
        } else if (status.equals("C")) { // отменен
            return 3;
        } else {
            return 0;
        }
    }
}
