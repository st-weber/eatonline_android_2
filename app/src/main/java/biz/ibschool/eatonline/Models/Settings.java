package biz.ibschool.eatonline.Models;

import biz.ibschool.eatonline.EatApplication;

import io.realm.RealmObject;

public class Settings extends RealmObject {

    private String personalDistrict;
    private String personalFloor;
    private String personalHouse;
    private String personalIntecrom;
    private String personalName;
    private String personalPhone;
    private String personalPorch;
    private String personalRoom;
    private String personalStreet;
    private int personsNumber;
    private String token;
    private boolean receiveNotifications;

    // связи
    private City currentCity;

    public String getPersonalDistrict() {
        return (personalDistrict != null) ? personalDistrict : "";
    }

    public void setPersonalDistrict(String personalDistrict) {
        this.personalDistrict = personalDistrict;
    }

    public String getPersonalFloor() {
        return (personalFloor != null) ? personalFloor : "";
    }

    public void setPersonalFloor(String personalFloor) {
        this.personalFloor = personalFloor;
    }

    public String getPersonalHouse() {
        return (personalHouse != null) ? personalHouse : "";
    }

    public void setPersonalHouse(String personalHouse) {
        this.personalHouse = personalHouse;
    }

    public String getPersonalIntecrom() {
        return (personalIntecrom != null) ? personalIntecrom : "";
    }

    public void setPersonalIntecrom(String personalIntecrom) {
        this.personalIntecrom = personalIntecrom;
    }

    public String getPersonalName() {
        return (personalName != null) ? personalName : "";
    }

    public void setPersonalName(String personalName) {
        this.personalName = personalName;
    }

    public String getPersonalPhone() {
        return (personalPhone != null) ? personalPhone : "";
    }

    public void setPersonalPhone(String personalPhone) {
        this.personalPhone = personalPhone;
    }

    public String getPersonalPorch() {
        return (personalPorch != null) ? personalPorch : "";
    }

    public void setPersonalPorch(String personalPorch) {
        this.personalPorch = personalPorch;
    }

    public String getPersonalRoom() {
        return (personalRoom != null) ? personalRoom : "";
    }

    public void setPersonalRoom(String personalRoom) {
        this.personalRoom = personalRoom;
    }

    public String getPersonalStreet() {
        return (personalStreet != null) ? personalStreet : "";
    }

    public void setPersonalStreet(String personalStreet) {
        this.personalStreet = personalStreet;
    }

    public int getPersonsNumber() {
        return personsNumber;
    }

    public void setPersonsNumber(int personsNumber) {
        this.personsNumber = personsNumber;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isReceiveNotifications() {
        return receiveNotifications;
    }

    public void setReceiveNotifications(boolean receiveNotifications) {
        this.receiveNotifications = receiveNotifications;
    }

    public City getCurrentCity() {
        return currentCity;
    }

    public void setCurrentCity(City currentCity) {
        EatApplication.realm.beginTransaction();
        this.currentCity = currentCity;
        EatApplication.realm.commitTransaction();
        //EatApplication.dataManager.tokenSent();
    }

}
