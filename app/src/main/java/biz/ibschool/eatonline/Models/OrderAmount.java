package biz.ibschool.eatonline.Models;

import io.realm.RealmObject;

public class OrderAmount extends RealmObject {

    private int amount;

    // связи
    private Dish dish;
    private Order order;

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Dish getDish() {
        return dish;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
}
