package biz.ibschool.eatonline.Models;

import biz.ibschool.eatonline.EatApplication;

import io.realm.RealmList;
import io.realm.RealmObject;

public class Order extends RealmObject {

    private String date;
    private int number;
    private int status;
    private int type;

    // связи
    private RealmList<OrderAmount> amounts;
    private Place place;
    private Table table;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        EatApplication.realm.beginTransaction();
        this.type = type;
        EatApplication.realm.commitTransaction();
    }

    public RealmList<OrderAmount> getAmounts() {
        return amounts;
    }

    public void setAmounts(RealmList<OrderAmount> amounts) {
        this.amounts = amounts;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        EatApplication.realm.beginTransaction();
        this.place = place;
        EatApplication.realm.commitTransaction();
    }

    public Table getTable() {
        return table;
    }

    public void setTable(Table table) {
        this.table = table;
    }
}
