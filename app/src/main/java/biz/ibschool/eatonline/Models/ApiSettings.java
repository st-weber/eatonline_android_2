package biz.ibschool.eatonline.Models;

import com.google.gson.annotations.Expose;

public class ApiSettings {

    @Expose
    private String operator;
    private String owner;
    private String hotline;

    public String getOperator() {
        return operator;
    }

    public String getHotline() {
        return hotline;
    }

    public String getOwner() {
        return owner;
    }
}
