package biz.ibschool.eatonline.Models;

import android.text.Html;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Dish extends RealmObject {

    @PrimaryKey
    private int id;

    private boolean favourite;
    private String img;
    private String bimg;
    private int price;
    private String del;
    private String tkw;
    private String res;
    private int sort;
    private String title;
    private String type;
    private String descr;
    private double weight;
    private String dweight;
    private String units;

    // связи
    private OrderAmount orderAmount;
    private Place place;
    private DishType dishType;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isFavourite() {
        return favourite;
    }

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getBimg() {
        return bimg;
    }

    public void setBimg(String bimg) {
        this.bimg = bimg;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getDel() {
        return del;
    }

    public void setDel(String del) {
        this.del = del;
    }

    public String getTkw() {
        return tkw;
    }

    public void setTkw(String tkw) {
        this.tkw = tkw;
    }

    public String getRes() {
        return res;
    }

    public void setRes(String res) {
        this.res = res;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getDweight() {
        return dweight;
    }

    public void setDweight(String dweight) {
        this.dweight = dweight;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getDescr() {
        return Html.fromHtml(descr).toString();
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public OrderAmount getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(OrderAmount orderAmount) {
        this.orderAmount = orderAmount;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public DishType getDishType() {
        return dishType;
    }

    public void setDishType(DishType dishType) {
        this.dishType = dishType;
    }
}
