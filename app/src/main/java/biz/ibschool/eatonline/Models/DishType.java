package biz.ibschool.eatonline.Models;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class DishType extends RealmObject {

    @PrimaryKey
    private int id;
    private int parent;
    private int category;
    private String icon;
    private String image;
    private int sort;
    private String name;

    // связи
    private RealmList<Dish> dishes;
    private RealmList<Place> places;
    private RealmList<DishType> subtypes;
    private DishType type;

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public int getId() {
        return id;
    }

    public void setId(int identifier) {
        this.id = identifier;
    }

    public int getParent() {
        return parent;
    }

    public void setParent(int parent) {
        this.parent = parent;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RealmList<Dish> getDishes() {
        return dishes;
    }

    public void setDishes(RealmList<Dish> dishes) {
        this.dishes = dishes;
    }

    public RealmList<Place> getPlaces() {
        return places;
    }

    public void setPlaces(RealmList<Place> places) {
        this.places = places;
    }

    public RealmList<DishType> getSubtypes() {
        return subtypes;
    }

    public void setSubtypes(RealmList<DishType> subtypes) {
        this.subtypes = subtypes;
    }

    public DishType getType() {
        return type;
    }

    public void setType(DishType type) {
        this.type = type;
    }
}
