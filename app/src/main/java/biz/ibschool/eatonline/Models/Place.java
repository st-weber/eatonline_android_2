package biz.ibschool.eatonline.Models;

import biz.ibschool.eatonline.EatApplication;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Place extends RealmObject {

    @PrimaryKey
    private int id;

    private String address;
    private String back;
    private int ophour;
    private int clhour;
    private String coordinates;
    private int dfrom;
    private int dprice;
    private boolean favourite;
    private String footNote;
    private String kitchens;
    private String logo;
    private int morder;
    private String payment;
    private String sber;
    private String cterminal;
    private String output;
    private String delivery;
    private String takeaway;
    private String reserv;
    private int sort;
    private String title;
    private String type;
    private String updated;
    private boolean isUpdated;
    private String wtime;

    // связи
    private Action action;
    private City city;
    private RealmList<Dish> dishes;
    private RealmList<DishType> dishTypes;
    private RealmList<Order> orders;
    private RealmList<Table> tables;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBack() {
        return back;
    }

    public void setBack(String back) {
        this.back = back;
    }

    public int getOphour() {
        return ophour;
    }

    public void setOphour(int ophour) {
        this.ophour = ophour;
    }

    public int getClhour() {
        return clhour;
    }

    public void setClhour(int clhour) {
        this.clhour = clhour;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }

    public int getDfrom() {
        return dfrom;
    }

    public void setDfrom(int dfrom) {
        this.dfrom = dfrom;
    }

    public int getDprice() {
        return dprice;
    }

    public void setDprice(int dprice) {
        this.dprice = dprice;
    }

    public boolean isFavourite() {
        return favourite;
    }

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }

    public String getFootNote() {
        return footNote;
    }

    public void setFootNote(String footNote) {
        this.footNote = footNote;
    }

    public String getKitchens() {
        return kitchens;
    }

    public void setKitchens(String kitchens) {
        this.kitchens = kitchens;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logoPath) {
        this.logo = logoPath;
    }

    public int getMorder() {
        return morder;
    }

    public void setMorder(int morder) {
        this.morder = morder;
    }

    public boolean isOnlinePayment() {
        return payment.equals("1");
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getPayment() { return payment; }

    public boolean isOutput() {
        return output.equals("1");
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutput() { return output; }

    public boolean isDelivery() {
        return delivery.equals("1");
    }

    public void setDelivery(String delivery) {
        this.delivery = delivery;
    }

    public String getDelivery() { return delivery; }

    public boolean isTakeaway() {
        return takeaway.equals("1");
    }

    public void setTakeaway(String takeaway) {
        this.takeaway = takeaway;
    }

    public String getTakeaway() { return takeaway; }

    public boolean isReserv() {
        return reserv.equals("1");
    }

    public void setReserv(String reserv) {
        this.reserv = reserv;
    }

    public String getReserv() { return reserv; }

    public boolean isCourierPayment() {
        return cterminal.equals("1");
    }

    public void setCterminal(String cterminal) {
        this.cterminal = cterminal;
    }

    public String getCterminal() { return cterminal; }

    public boolean isSberPayment() {
        return sber.equals("1");
    }

    public void setSber(String sber) {
        this.sber = sber;
    }

    public String getSber() { return sber; }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public boolean getIsUpdated() {
        return isUpdated;
    }

    public void setIsUpdated(boolean isUpdated) {
        EatApplication.realm.beginTransaction();
        this.isUpdated = isUpdated;
        EatApplication.realm.commitTransaction();
    }

    public String getWtime() {
        return wtime;
    }

    public void setWtime(String wtime) {
        this.wtime = wtime;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public RealmList<Dish> getDishes() {
        return dishes;
    }

    public void setDishes(RealmList<Dish> dishes) {
        this.dishes = dishes;
    }

    public RealmList<DishType> getDishTypes() {
        return dishTypes;
    }

    public void setDishTypes(RealmList<DishType> dishTypes) {
        this.dishTypes = dishTypes;
    }

    public RealmList<Order> getOrders() {
        return orders;
    }

    public void setOrders(RealmList<Order> orders) {
        this.orders = orders;
    }

    public RealmList<Table> getTables() {
        return tables;
    }

    public void setTables(RealmList<Table> tables) {
        this.tables = tables;
    }
}