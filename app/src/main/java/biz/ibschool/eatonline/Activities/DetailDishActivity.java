package biz.ibschool.eatonline.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import biz.ibschool.eatonline.Adapters.BasePagerAdapter;
import biz.ibschool.eatonline.DataManager;
import biz.ibschool.eatonline.EatApplication;
import biz.ibschool.eatonline.Fragments.DetailDishFragment;
import biz.ibschool.eatonline.Models.Dish;
import biz.ibschool.eatonline.Models.DishType;
import biz.ibschool.eatonline.R;

import java.util.ArrayList;

public class DetailDishActivity extends AppCompatActivity implements DataManager.DishActionsCallback, DataManager.BasketCountCallback {

    private ActionBar actionBar;
    private int orderType;
    private ViewPager dishesPager;
    private BasePagerAdapter dishesPageAdapter;

    @Override
    protected void onResume() {
        super.onResume();
        EatApplication.dataManager.registerDishActionsListener(this);
        EatApplication.dataManager.registerBasketCountListener(this);
        invalidateOptionsMenu();
    }

    @Override
    protected void onPause() {
        super.onPause();
        EatApplication.dataManager.registerDishActionsListener(null);
        EatApplication.dataManager.registerBasketCountListener(null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_dish);

        Intent intent = getIntent(); // Передача переменной
        int dishId = intent.getIntExtra("dishID", 0);
        orderType = intent.getIntExtra("orderType", 0);

        Dish dish = EatApplication.realm.where(Dish.class).equalTo("id", dishId).findFirst();

        DishType dishType = dish.getDishType();
        ArrayList<Dish> dishes = new ArrayList(dish.getPlace().getDishes().where().equalTo("type", String.valueOf(dishType.getId())).findAll().sort("sort"));

        int currentDish = 0;
        for (int i = 0; i < dishes.size(); i++) {
            if (dishes.get(i).getId() == dish.getId()) {
                currentDish = i;
            }
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setTitle(dishType.getName());
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        dishesPager = (ViewPager) findViewById(R.id.dishes_pager);
        dishesPageAdapter = new BasePagerAdapter(dishes, getSupportFragmentManager(), orderType);
        dishesPager.setAdapter(dishesPageAdapter);

        dishesPager.setOffscreenPageLimit(2);
        final int finalCurrentDish = currentDish;
        dishesPager.post(new Runnable() {
            @Override
            public void run() {
                dishesPager.setCurrentItem(finalCurrentDish);
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.basket_menu, menu);

        MenuItem menuItem = menu.findItem(R.id.basket_icon);
        menuItem.setIcon(EatApplication.dataManager.buildCounterDrawable(true));

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.basket_icon) {
            if (EatApplication.dataManager.getCurrentOrder().getPlace() != null) {
                Intent basketActivity = new Intent(this, BasketActivity.class);
                startActivity(basketActivity);
            }
        }

        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    /*
    private void closeActivity() {
        Intent main = new Intent(this, MainActivity.class);
        main.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        main.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(main);
    }*/

    @Override
    public void dishAmountChanged(Dish dish) {
        DetailDishFragment dishFragment = (DetailDishFragment)dishesPageAdapter.getItem(dishesPager.getCurrentItem());
        dishFragment.checkCount();
    }

    @Override
    public void basketCountChanged() {
        invalidateOptionsMenu();
    }
}
