package biz.ibschool.eatonline.Activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Calendar;

import biz.ibschool.eatonline.ApiInteractor;
import biz.ibschool.eatonline.Interfaces.LoadingTextInterface;
import biz.ibschool.eatonline.R;

public class NavAbout extends AppCompatActivity {

    ActionBar actionBar;
    private TextView pageText;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nav_about);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();

        actionBar.setTitle("О нас");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        ImageView vkView = (ImageView) findViewById(R.id.socials_vk);
        ImageView okView = (ImageView) findViewById(R.id.socials_ok);
        ImageView inView = (ImageView) findViewById(R.id.socials_in);
        ImageView fbView = (ImageView) findViewById(R.id.socials_fb);

        TextView ibsText = (TextView) findViewById(R.id.ibs_text);
        TextView weberText = (TextView) findViewById(R.id.weber_text);

        vkView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uriUrl = Uri.parse("http://vk.com/eatonline");
                Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                startActivity(launchBrowser);
            }
        });

        okView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uriUrl = Uri.parse("http://www.odnoklassniki.ru/group/53221826560100");
                Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                startActivity(launchBrowser);
            }
        });

        inView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uriUrl = Uri.parse("http://instagram.com/eatonline.ru");
                Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                startActivity(launchBrowser);
            }
        });

        fbView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uriUrl = Uri.parse("https://www.facebook.com/eatonline.ru");
                Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                startActivity(launchBrowser);
            }
        });

        ibsText.setText("ООО «МБШ» | 2014 - " + Calendar.getInstance().get(Calendar.YEAR));
        ibsText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uriUrl = Uri.parse("http://ibschool.biz/");
                Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                startActivity(launchBrowser);
            }
        });

        weberText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uriUrl = Uri.parse("http://michanic.ru/");
                Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                startActivity(launchBrowser);
            }
        });

        pageText = (TextView) findViewById(R.id.pageText);
        pageText.setVisibility(View.GONE);

        ApiInteractor apiInteractor = new ApiInteractor();
        apiInteractor.loadPageText("about", new LoadingTextInterface() {
            @Override
            public void onLoaded(String text) {
                pageText.setText(Html.fromHtml(text));
                pageText.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

}
