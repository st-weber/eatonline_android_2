package biz.ibschool.eatonline.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import biz.ibschool.eatonline.Adapters.BaseAdapter.OrdersListAdapter;
import biz.ibschool.eatonline.EatApplication;
import biz.ibschool.eatonline.Interfaces.OrderActions;
import biz.ibschool.eatonline.Models.Order;
import biz.ibschool.eatonline.R;

import java.util.ArrayList;

public class NavOrders extends AppCompatActivity implements OrderActions {

    ActionBar actionBar;
    private OrdersListAdapter ordersListAdapter;
    private ListView listViewOrders;
    private TextView listEmptyLabel;

    @Override
    protected void onResume() {
        super.onResume();
        ordersListAdapter.updateOrders(getOrdersList());
        ordersListAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nav_orders);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();

        actionBar.setTitle("Мои заказы");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        listViewOrders = (ListView) findViewById(R.id.list_orders);
        listEmptyLabel = (TextView) findViewById(R.id.list_empty);

        ArrayList orders = getOrdersList();

        ordersListAdapter = new OrdersListAdapter(getBaseContext(), getOrdersList(), this);
        listViewOrders.setAdapter(ordersListAdapter);

        if (orders.size() > 0) {
            listEmptyLabel.setVisibility(View.GONE);
            listViewOrders.setVisibility(View.VISIBLE);
        } else {
            listEmptyLabel.setVisibility(View.VISIBLE);
            listViewOrders.setVisibility(View.GONE);
        }
    }

    private ArrayList getOrdersList() {
        ArrayList arrayListOrders = new ArrayList(EatApplication.realm.where(Order.class)
                .isNotNull("place")
                .findAll()
                .sort("status"));

        System.out.println("getOrdersList: " + arrayListOrders.size());

        return arrayListOrders;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void checkoutCurrentOrder() {
        Intent checkoutActivity = new Intent(getBaseContext(), CheckoutActivity.class);
        getBaseContext().startActivity(checkoutActivity);
    }

    @Override
    public void orderStatusChecked() {
        ordersListAdapter.notifyDataSetChanged();
    }

    @Override
    public void repeatOrder(Order order) {
        EatApplication.dataManager.repeatOrder(order);
        Intent activityPlace = new Intent(getBaseContext(), PlaceActivity.class);
        activityPlace.putExtra("placeId", order.getPlace().getId());
        getBaseContext().startActivity(activityPlace);
    }

    @Override
    public void orderDeleted() {
        ordersListAdapter.updateOrders(getOrdersList());
        ordersListAdapter.notifyDataSetChanged();

        if (getOrdersList().size() > 0) {
            listEmptyLabel.setVisibility(View.GONE);
            listViewOrders.setVisibility(View.VISIBLE);
        } else {
            listEmptyLabel.setVisibility(View.VISIBLE);
            listViewOrders.setVisibility(View.GONE);
        }
    }


}
