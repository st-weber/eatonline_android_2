package biz.ibschool.eatonline.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import biz.ibschool.eatonline.DataManager;
import biz.ibschool.eatonline.EatApplication;
import biz.ibschool.eatonline.R;

public class NavLeaveReview extends AppCompatActivity {

    private ActionBar actionBar;
    private EditText phoneEditText;
    private EditText messageEditText;
    private Button sendButton;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nav_leave_review);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();

        actionBar.setTitle("Оставить отзыв");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        phoneEditText = (EditText) findViewById(R.id.phone);
        messageEditText = (EditText) findViewById(R.id.message);
        sendButton = (Button) findViewById(R.id.send);
        progressBar = (ProgressBar) findViewById(R.id.progress_indicator);
        phoneEditText.setText(EatApplication.appUtils.getPhoneFormattedString(EatApplication.dataManager.getSettings().getPersonalPhone()));
        phoneEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                checkImputData();
            }
        });

        messageEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                checkImputData();
            }
        });

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendButton.setVisibility(View.GONE);
                sendButton.setClickable(false);
                progressBar.setVisibility(View.VISIBLE);

                CountDownTimer timer = new CountDownTimer(1 * 1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }
                    @Override
                    public void onFinish() {

                        EatApplication.dataManager.sentMessageToDevelopers(phoneEditText.getText().toString(), messageEditText.getText().toString(), new DataManager.RequestInterface() {
                            @Override
                            public void onSuccess() {

                                sendButton.setVisibility(View.VISIBLE);
                                sendButton.setText("Отправлено");
                                progressBar.setVisibility(View.GONE);
                                CountDownTimer timer = new CountDownTimer(1 * 1000, 1000) {
                                    @Override
                                    public void onTick(long millisUntilFinished) {

                                    }

                                    @Override
                                    public void onFinish() {
                                        closeActivity();
                                    }
                                }.start();

                            }

                            @Override
                            public void onFailed() {

                                sendButton.setVisibility(View.VISIBLE);
                                sendButton.setText("Повторить");
                                sendButton.setClickable(true);
                                progressBar.setVisibility(View.GONE);

                            }
                        });

                    }
                }.start();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    private void checkImputData() {
        if (phoneEditText.getText().length() > 3 && messageEditText.getText().length() > 3) {
            sendButton.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorLightGray));
            sendButton.setBackgroundResource(R.drawable.round_corner_enabled);
            sendButton.setClickable(true);
        } else {
            sendButton.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorWhite));
            sendButton.setBackgroundResource(R.drawable.round_corner_disabled);
            sendButton.setClickable(false);
        }
    }

    private void closeActivity() {
        Intent main = new Intent(this, MainActivity.class);
        main.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(main);
    }
}