package biz.ibschool.eatonline.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import biz.ibschool.eatonline.Adapters.BasePagerAdapter;
import biz.ibschool.eatonline.DataManager;
import biz.ibschool.eatonline.EatApplication;
import biz.ibschool.eatonline.Fragments.DetailTableFragment;
import biz.ibschool.eatonline.Models.Place;
import biz.ibschool.eatonline.Models.Table;
import biz.ibschool.eatonline.R;

import java.util.ArrayList;

public class DetailTablesActivity extends AppCompatActivity implements DataManager.TableActionsCallback, DataManager.BasketCountCallback {

    private ActionBar actionBar;
    private ViewPager tablePager;
    private BasePagerAdapter tablesPageAdapter;

    @Override
    protected void onResume() {
        super.onResume();
        EatApplication.dataManager.registerTableActionsListener(this);
        EatApplication.dataManager.registerBasketCountListener(this);
        invalidateOptionsMenu();
    }

    @Override
    protected void onPause() {
        super.onPause();
        EatApplication.dataManager.registerTableActionsListener(null);
        EatApplication.dataManager.registerBasketCountListener(null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_tables);

        Intent intent = getIntent();
        int tableId = intent.getIntExtra("tableId", 0);
        int placeId = intent.getIntExtra("placeId", 0);

        Table table = EatApplication.realm.where(Table.class).equalTo("id", tableId).findFirst();

        ArrayList<Table> tables = new ArrayList<>(EatApplication.realm.where(Place.class).equalTo("id", placeId).findFirst().getTables());

        int currentTable = 0;
        for (int i = 0; i < tables.size(); i++) {
            if (tables.get(i).getId() == table.getId()) {
                currentTable = i;
            }
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setTitle("Выбор столика");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        tablePager = (ViewPager) findViewById(R.id.dishes_pager);
        tablesPageAdapter = new BasePagerAdapter(tables, getSupportFragmentManager());
        tablePager.setAdapter(tablesPageAdapter);

        tablePager.setOffscreenPageLimit(2);
        final int finalCurrentTable = currentTable;
        tablePager.post(new Runnable() {
            @Override
            public void run() {
                tablePager.setCurrentItem(finalCurrentTable);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.basket_menu, menu);

        MenuItem menuItem = menu.findItem(R.id.basket_icon);
        menuItem.setIcon(EatApplication.dataManager.buildCounterDrawable(true));

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.basket_icon) {
            if (EatApplication.dataManager.getCurrentOrder().getPlace() != null) {
                Intent basketActivity = new Intent(this, BasketActivity.class);
                startActivity(basketActivity);
            }
        }

        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void tableReserveChanged() {
        for (int i = 0; i < tablesPageAdapter.getCount(); i++) {

            //Fragment tableFragment = (Fragment)tablesPageAdapter.getItem(i);
            //System.out.println("tableReserveChanged " + tableFragment.getClass().getName());

            DetailTableFragment tableFragment = (DetailTableFragment)tablesPageAdapter.getItem(i);
            tableFragment.checkReserved();
        }
    }

    @Override
    public void basketCountChanged() {
        invalidateOptionsMenu();
    }
}
