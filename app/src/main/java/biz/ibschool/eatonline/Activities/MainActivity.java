package biz.ibschool.eatonline.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;

import biz.ibschool.eatonline.DataManager;
import biz.ibschool.eatonline.EatApplication;
import biz.ibschool.eatonline.Fragments.FavouritesPageFragment;
import biz.ibschool.eatonline.Fragments.TabFragment;
import biz.ibschool.eatonline.R;

import it.sephiroth.android.library.bottomnavigation.BottomNavigation;

import static biz.ibschool.eatonline.Enums.TabFragmentType.ACTIONS_LIST;
import static biz.ibschool.eatonline.Enums.TabFragmentType.DISH_TYPES_LIST;
import static biz.ibschool.eatonline.Enums.TabFragmentType.PLACES_LIST;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, DataManager.BasketCountCallback, DataManager.FavouritesCountCallback {

    private BottomNavigation bottomNavigation;
    private View favouritesTab;

    @Override
    protected void onResume() {
        super.onResume();
        EatApplication.dataManager.registerBasketCountListener(this);
        EatApplication.dataManager.registerFavouritesCountListener(this);
        invalidateOptionsMenu();
        EatApplication.dataManager.refreshTime();
        checkFavouritesTab();
    }

    @Override
    protected void onPause() {
        super.onPause();
        EatApplication.dataManager.registerBasketCountListener(null);
        EatApplication.dataManager.registerFavouritesCountListener(null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        bottomNavigation = (BottomNavigation) findViewById(R.id.bottom_navigation);
        bottomNavigation.setDefaultSelectedIndex(1);
        getSupportFragmentManager().beginTransaction().replace(R.id.content_main, TabFragment.newInstance(PLACES_LIST)).commit();

        bottomNavigation.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                    bottomNavigation.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                else bottomNavigation.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                checkTabs();
            }
        });
    }

    private void checkFavouritesTab() {
        if (favouritesTab != null) {
            favouritesTab.setEnabled(EatApplication.dataManager.getFavouritesCount() > 0);
        }
    }

    private void checkTabs() {

        bottomNavigation.findViewById(R.id.action_poster).setEnabled(EatApplication.dataManager.getActionsCount() > 0);
        favouritesTab = bottomNavigation.findViewById(R.id.action_favorites);
        checkFavouritesTab();

        bottomNavigation.setOnMenuItemClickListener(new BottomNavigation.OnMenuItemSelectionListener() {
            @Override
            public void onMenuItemSelect(@IdRes int i, int i1, boolean b) {
                switch (i) {
                    case R.id.action_poster:


                        getSupportFragmentManager().beginTransaction().replace(R.id.content_main, TabFragment.newInstance(ACTIONS_LIST)).commit();
                        break;
                    case R.id.action_places:
                        getSupportFragmentManager().beginTransaction().replace(R.id.content_main, TabFragment.newInstance(PLACES_LIST)).commit();
                        break;
                    case R.id.action_dishes:
                        getSupportFragmentManager().beginTransaction().replace(R.id.content_main, TabFragment.newInstance(DISH_TYPES_LIST)).commit();
                        break;
                    case R.id.action_favorites:
                        getSupportFragmentManager().beginTransaction().replace(R.id.content_main, new FavouritesPageFragment()).commit();
                        break;
                }
            }

            @Override
            public void onMenuItemReselect(@IdRes int i, int i1, boolean b) {
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);

        MenuItem menuItem = menu.findItem(R.id.basket_main_icon);
        menuItem.setIcon(EatApplication.dataManager.buildCounterDrawable(false));

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.search_icon) {
            Intent searchActivity = new Intent(this, SearchActivity.class);
            startActivity(searchActivity);
        }
        if (id == R.id.basket_main_icon) {

            if (EatApplication.dataManager.getCurrentOrder().getPlace() != null) {
                Intent basketActivity = new Intent(this, BasketActivity.class);
                startActivity(basketActivity);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();
        Intent intent = null;

        if (id == R.id.nav_orders) {
            intent = new Intent(this, NavOrders.class);
        } else if (id == R.id.nav_city) {
            intent = new Intent(this, NavCity.class);
        } else if (id == R.id.nav_personals) {
            intent = new Intent(this, NavPersonals.class);
        } else if (id == R.id.nav_hot_line) {
            Intent callPhone = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:+" + EatApplication.configStorage.getOperatorPhone()));
            startActivity(callPhone);
        } else if (id == R.id.nav_control) {
            Intent callPhone = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:+" + EatApplication.configStorage.getOwnerPhone()));
            startActivity(callPhone);
        } else if (id == R.id.nav_leave_review) {
            intent = new Intent(this, NavLeaveReview.class);
        } else if (id == R.id.nav_about) {
            intent = new Intent(this, NavAbout.class);
        } else if (id == R.id.nav_agreement) {
            intent = new Intent(this, NavTextPage.class);
            intent.putExtra("title", "Пользовательское соглашение");
            intent.putExtra("page_code", "agreement");
        } else if (id == R.id.nav_partners) {
            intent = new Intent(this, NavTextPage.class);
            intent.putExtra("title", "Для партнеров");
            intent.putExtra("page_code", "partnership");
        }

        if (intent != null) {
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    @Override
    public void basketCountChanged() {
        invalidateOptionsMenu();
    }

    @Override
    public void favouritesCountChanged() {
        checkFavouritesTab();
        if (EatApplication.dataManager.getFavouritesCount() == 0) {
            bottomNavigation.setSelectedIndex(1, true);
        }
    }

}