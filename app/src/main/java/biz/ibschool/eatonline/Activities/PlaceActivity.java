package biz.ibschool.eatonline.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import biz.ibschool.eatonline.Adapters.BasePagerAdapter;
import biz.ibschool.eatonline.DataManager;
import biz.ibschool.eatonline.EatApplication;
import biz.ibschool.eatonline.Models.Dish;
import biz.ibschool.eatonline.Models.Place;
import biz.ibschool.eatonline.R;

public class PlaceActivity extends AppCompatActivity implements DataManager.BasketCountCallback {

    ActionBar actionBar;

    private int idPlace;  // Передача переменной
    private Place place;

    private ViewPager pager;
    private TabLayout tabs;
    private FragmentManager fm;

    @Override
    protected void onResume() {
        super.onResume();
        EatApplication.dataManager.registerBasketCountListener(this);
        invalidateOptionsMenu();
    }

    @Override
    protected void onPause() {
        super.onPause();
        EatApplication.dataManager.registerBasketCountListener(null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        actionBar = getSupportActionBar();

        Intent intent = getIntent(); // Передача переменной
        idPlace = intent.getIntExtra("placeId", 0);

        int startTypeId = intent.getIntExtra("startTypeId", 0);
        int startDishId = intent.getIntExtra("startDishId", 0);
        if (startDishId != 0) {
            Dish dish = EatApplication.realm.where(Dish.class).equalTo("id", startDishId).findFirst();
            //System.out.println("start dish " +  dish.getTitle() + " type " + dish.getDishType().getName());
            if (dish.getDishType().getParent() > 0) { // блюдо в подтипе
                startTypeId = dish.getDishType().getParent();
            } else {
                startTypeId = dish.getDishType().getId();
            }
        }

        place = EatApplication.realm.where(Place.class).equalTo("id", idPlace).findFirst();

        actionBar.setTitle(place.getTitle());
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        pager = (ViewPager) findViewById(R.id.base_pager);
        fm = getSupportFragmentManager();

        pager.setAdapter(new BasePagerAdapter(fm, place, startTypeId, startDishId));

        tabs = (TabLayout) findViewById(R.id.base_tabs);
        tabs.setupWithViewPager(pager);
        tabs.getTabAt(0).select();


        //--> Adding tabs
        for (int i = 0; i < tabs.getTabCount(); i++) {
            switch (i) {
                case 0:
                    tabs.getTabAt(i).setText("МЕНЮ");
                    break;
                case 1:
                    tabs.getTabAt(i).setText("ЗОНЫ ДОСТАВКИ");
                    break;
                default:
                    tabs.getTabAt(i).setText("unknown");
                    break;
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.basket_menu, menu);

        MenuItem menuItem = menu.findItem(R.id.basket_icon);
        menuItem.setIcon(EatApplication.dataManager.buildCounterDrawable(false));

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.basket_icon) {

            if (EatApplication.dataManager.getCurrentOrder().getPlace() != null) {
                Intent basketActivity = new Intent(this, BasketActivity.class);
                startActivity(basketActivity);
            }
        }

        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Intent intent = getIntent();
        overridePendingTransition(0, 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);

    }

    @Override
    public void basketCountChanged() {
        invalidateOptionsMenu();
    }

}