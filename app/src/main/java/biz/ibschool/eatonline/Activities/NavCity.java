package biz.ibschool.eatonline.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import biz.ibschool.eatonline.Adapters.BaseAdapter.CitiesListAdapter;
import biz.ibschool.eatonline.DataManager;
import biz.ibschool.eatonline.EatApplication;
import biz.ibschool.eatonline.Models.Action;
import biz.ibschool.eatonline.Models.City;
import biz.ibschool.eatonline.Models.Dish;
import biz.ibschool.eatonline.Models.Order;
import biz.ibschool.eatonline.Models.OrderAmount;
import biz.ibschool.eatonline.Models.Place;
import biz.ibschool.eatonline.Models.Table;
import biz.ibschool.eatonline.R;

import java.util.ArrayList;

public class NavCity extends EatActivity {

    ActionBar actionBar;

    private ListView citiesListView;
    private ProgressBar progressIndicator;
    private ProgressBar loadingIndicator;
    private TextView loadingStatusTextView;
    private Toolbar toolbar;

    private boolean activitiBlocked = false;

    private void loadDishTypes() {

        EatApplication.dataManager.loadDishTypes(new DataManager.RequestInterface() {
            @Override
            public void onSuccess() {
                loadPlacesList();
            }

            @Override
            public void onFailed() {
                showNoConnectionDialog(NavCity.this, new NoConnectionRepeatInterface() {
                    @Override
                    public void repeatPressed() {
                        loadDishTypes();
                    }
                });
            }
        });

    }

    private void loadPlacesList() {

        EatApplication.dataManager.loadPlacesList(new DataManager.RequestInterface() {
            @Override
            public void onSuccess() {
                loadActions();
            }

            @Override
            public void onFailed() {
                showNoConnectionDialog(NavCity.this, new NoConnectionRepeatInterface() {
                    @Override
                    public void repeatPressed() {
                        loadPlacesList();
                    }
                });
            }
        });

    }

    private void loadActions() {
        EatApplication.dataManager.loadActions(new DataManager.RequestInterface() {
            @Override
            public void onSuccess() {
                loadPlacesInfo();
            }

            @Override
            public void onFailed() {
                showNoConnectionDialog(NavCity.this, new NoConnectionRepeatInterface() {
                    @Override
                    public void repeatPressed() {
                        loadActions();
                    }
                });
            }
        });
    }

    private void loadPlacesInfo() {
        final int placesCount = EatApplication.dataManager.getPlacesCount();

        loadingIndicator.setMax(placesCount);
        loadingIndicator.setProgress(0);

        EatApplication.dataManager.loadPlacesInfo(new DataManager.RequestInterface() {
            @Override
            public void onSuccess() {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }

            @Override
            public void onFailed() {
                showNoConnectionDialog(NavCity.this, new NoConnectionRepeatInterface() {
                    @Override
                    public void repeatPressed() {
                        loadPlacesInfo();
                    }
                });
            }
        }, new DataManager.PlacesLoadingInterface() {
            @Override
            public void placeInfoLoaded(Place place, int placeNumber) {
                loadingStatusTextView.setText(place.getTitle() + " " + (placeNumber + 1) + " из " + placesCount);
                loadingIndicator.setProgress(placeNumber + 1);
            }
        });
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nav_city);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setTitle("Выбор города");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        progressIndicator = (ProgressBar) findViewById(R.id.progressIndicator);
        progressIndicator.setVisibility(View.VISIBLE);

        citiesListView = (ListView) findViewById(R.id.citiesListView);
        citiesListView.setVisibility(View.GONE);

        loadingIndicator = (ProgressBar) findViewById(R.id.loadingIndicator);
        loadingIndicator.setVisibility(View.GONE);

        loadingStatusTextView = (TextView) findViewById(R.id.loadingStatusTextView);
        loadingStatusTextView.setVisibility(View.GONE);

        loadCities();
        /*final ArrayList<City> cities = EatApplication.dataManager.sort(EatApplication.realm.where(City.class).findAll());

        CitiesListAdapter citiesListAdapter = new CitiesListAdapter(getApplicationContext(), cities);
        citiesListView.setAdapter(citiesListAdapter);

        citiesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id) {

                City newCity = cities.get(position);
                if (newCity.getId() != EatApplication.dataManager.getSettings().getCurrentCity().getId()) {

                    citiesListView.setVisibility(View.GONE);
                    loadingIndicator.setVisibility(View.VISIBLE);
                    loadingStatusTextView.setVisibility(View.VISIBLE);

                    deleteAllData();
                    toolbar.setNavigationOnClickListener(null);
                    activitiBlocked = true;

                    EatApplication.dataManager.getSettings().setCurrentCity(newCity);

                    loadDishTypes();
                }

            }
        });*/

    }

    @Override
    public void onBackPressed() {
        if (!activitiBlocked) {
            NavCity.super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }


    private void deleteAllData() {

        EatApplication.realm.beginTransaction();
        EatApplication.realm.delete(Action.class);
        EatApplication.realm.delete(Dish.class);
        EatApplication.realm.delete(Place.class);
        EatApplication.realm.delete(Table.class);
        EatApplication.realm.delete(Order.class);
        EatApplication.realm.delete(OrderAmount.class);
        EatApplication.realm.commitTransaction();

    }

    private void loadCities() {
        EatApplication.dataManager.loadCities(new DataManager.RequestInterface() {
            @Override
            public void onSuccess() {

                progressIndicator.setVisibility(View.GONE);
                citiesListView.setVisibility(View.VISIBLE);

                final ArrayList<City> cities = EatApplication.dataManager.sort(EatApplication.realm.where(City.class).findAll());
                CitiesListAdapter citiesListAdapter = new CitiesListAdapter(getApplicationContext(), cities);
                citiesListView.setAdapter(citiesListAdapter);

                citiesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id) {

                        City newCity = cities.get(position);
                        if (newCity.getId() != EatApplication.dataManager.getSettings().getCurrentCity().getId()) {

                            citiesListView.setVisibility(View.GONE);
                            loadingIndicator.setVisibility(View.VISIBLE);
                            loadingStatusTextView.setVisibility(View.VISIBLE);

                            deleteAllData();
                            toolbar.setNavigationOnClickListener(null);
                            activitiBlocked = true;

                            EatApplication.dataManager.getSettings().setCurrentCity(newCity);

                            loadDishTypes();
                        }

                    }
                });

                /*citiesListView.setVisibility(View.VISIBLE);
                selectCityTextView.setVisibility(View.VISIBLE);
                progressIndicator.setVisibility(View.GONE);

                // Города загружены в базу, выводим их в таблицу
                CitiesListAdapter citiesListAdapter;
                citiesListAdapter = new CitiesListAdapter(getApplicationContext(), EatApplication.dataManager.sort(EatApplication.realm.where(City.class).findAll()));
                citiesListView.setAdapter(citiesListAdapter);*/

            }
            @Override
            public void onFailed() {
                showNoConnectionDialog(NavCity.this, new NoConnectionRepeatInterface() {
                    @Override
                    public void repeatPressed() {
                        loadCities();
                    }
                });
            }
        });
    }

}
