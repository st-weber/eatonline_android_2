package biz.ibschool.eatonline.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import biz.ibschool.eatonline.Adapters.BaseAdapter.BasketListAdapter;
import biz.ibschool.eatonline.DataManager;
import biz.ibschool.eatonline.EatApplication;
import biz.ibschool.eatonline.Interfaces.Const;
import biz.ibschool.eatonline.Interfaces.DishActions;
import biz.ibschool.eatonline.Models.Dish;
import biz.ibschool.eatonline.Models.Order;
import biz.ibschool.eatonline.Models.OrderAmount;
import biz.ibschool.eatonline.Models.Place;
import biz.ibschool.eatonline.Models.Table;
import biz.ibschool.eatonline.R;

import java.util.ArrayList;

public class BasketActivity extends AppCompatActivity implements DishActions, DataManager.DishActionsCallback, DataManager.TableActionsCallback {

    private BasketListAdapter basketListAdapter;
    private ArrayList<OrderAmount> orderAmounts;

    private TextView summary;
    private Button checkoutButton;

    @Override
    protected void onResume() {
        super.onResume();
        EatApplication.dataManager.registerDishActionsListener(this);
        EatApplication.dataManager.registerTableActionsListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EatApplication.dataManager.registerDishActionsListener(null);
        EatApplication.dataManager.registerTableActionsListener(null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basket);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar;
        actionBar = getSupportActionBar();

        actionBar.setTitle("Корзина");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        final Order currentOrder = EatApplication.dataManager.getCurrentOrder();
        Place place = currentOrder.getPlace();

        final TabLayout serviceTabs = (TabLayout) findViewById(R.id.base_tabs);
        summary = (TextView) findViewById(R.id.summary_text);
        checkoutButton = (Button) findViewById(R.id.checkout_button);

        if (place.isDelivery()) {
            serviceTabs.addTab(serviceTabs.newTab().setText("ДОСТАВКА").setTag(0), (currentOrder.getType() == 0));
        }
        if (place.isTakeaway()) {
            serviceTabs.addTab(serviceTabs.newTab().setText("НА ВЫНОС").setTag(1), (currentOrder.getType() == 1));
        }
        if (place.isReserv()) {
            serviceTabs.addTab(serviceTabs.newTab().setText("СТОЛИК").setTag(2), (currentOrder.getType() == 2));
        }


        serviceTabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                currentOrder.setType(Integer.parseInt(tab.getTag().toString()));
                basketListAdapter.notifyDataSetChanged();
                checkSumm();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        ((TextView) findViewById(R.id.min_order)).setText("мин. заказ: " + place.getMorder() + " " + Const.PRICE_UNITS);
        ListView listView = (ListView) findViewById(R.id.list_item);

        orderAmounts = new ArrayList<OrderAmount>(currentOrder.getAmounts());
        basketListAdapter = new BasketListAdapter(getApplicationContext(), orderAmounts);
        basketListAdapter.setDishActionsListener(this);
        listView.setAdapter(basketListAdapter);

        checkSumm();

        checkoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean checkoutAvailable = false;
                if (currentOrder.getPlace() != null) {
                    if (currentOrder.getPlace().getMorder() <= EatApplication.dataManager.getSummOrder(currentOrder)) {
                        checkoutAvailable = true;
                    }
                    if (currentOrder.getType() == 2 && currentOrder.getTable() != null) {
                        checkoutAvailable = true;
                    }
                }
                if (checkoutAvailable) {
                    Intent ordering = new Intent(getApplicationContext(), CheckoutActivity.class);
                    startActivity(ordering);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void addPressed(Dish dish) {

    }

    @Override
    public void plusPressed(Dish dish) {
        EatApplication.dataManager.currentOrderChangeDishAmount(dish, EatApplication.dataManager.getCurrentOrderAmountForDish(dish) + 1, getApplicationContext());
    }

    @Override
    public void minusPressed(Dish dish) {
        int oldAmount =  EatApplication.dataManager.getCurrentOrderAmountForDish(dish);
        if (oldAmount > 1) {
            EatApplication.dataManager.currentOrderChangeDishAmount(dish, oldAmount - 1, getApplicationContext());
        }
    }

    @Override
    public void deletePressed(Dish dish) {
        orderAmounts.remove(dish.getOrderAmount());
        EatApplication.dataManager.currentOrderChangeDishAmount(dish, 0, getApplicationContext());
    }

    @Override
    public void dishDetailPressed(Dish dish) {

    }

    @Override
    public void deletePressed(Table table) {
        EatApplication.realm.beginTransaction();
        EatApplication.dataManager.getCurrentOrder().getTable().deleteFromRealm();
        EatApplication.realm.commitTransaction();
        basketListAdapter.notifyDataSetChanged();
        checkSumm();
        checkBasket();
    }

    @Override
    public void dishAmountChanged(Dish dish) {
        basketListAdapter.notifyDataSetChanged();
        checkSumm();
        checkBasket();
    }

    private void checkSumm() {
        Order order = EatApplication.dataManager.getCurrentOrder();
        int summ = EatApplication.dataManager.getSummOrder(order);

        boolean checkoutAvailable = false;
        if (order.getPlace() != null) {
            if (summ >= order.getPlace().getMorder()) {
                checkoutAvailable = true;
            }
            if (order.getType() == 2 && order.getTable() != null) {
                checkoutAvailable = true;
            }
            // order.getAmounts().size() > 0
        }
        if (checkoutAvailable) {
            checkoutButton.setClickable(true);
            checkoutButton.setBackgroundResource(R.drawable.round_corner_enabled);
        } else {
            checkoutButton.setClickable(false);
            checkoutButton.setBackgroundResource(R.drawable.round_corner_disabled);
        }
        summary.setText(summ + " " + Const.PRICE_UNITS);
    }

    private void checkBasket() {
        Order currentOrder = EatApplication.dataManager.getCurrentOrder();
        if (orderAmounts.size() < 1 && currentOrder.getTable() == null) {
            EatApplication.realm.beginTransaction();
            EatApplication.realm.delete(Order.class);
            EatApplication.realm.commitTransaction();
            finish();
        }
    }

    @Override
    public void tableReserveChanged() {
        basketListAdapter.notifyDataSetChanged();
        checkSumm();
        checkBasket();
    }
}
