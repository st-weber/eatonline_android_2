package biz.ibschool.eatonline.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import biz.ibschool.eatonline.DataManager;
import biz.ibschool.eatonline.EatApplication;
import biz.ibschool.eatonline.Interfaces.Const;
import biz.ibschool.eatonline.Interfaces.OrderRequests;
import biz.ibschool.eatonline.Models.Order;
import biz.ibschool.eatonline.Models.Place;
import biz.ibschool.eatonline.Models.Settings;
import biz.ibschool.eatonline.R;

import ru.tinkoff.decoro.FormattedTextChangeListener;
import ru.tinkoff.decoro.watchers.FormatWatcher;

public class CheckoutActivity extends EatActivity implements OrderRequests {

    private ActionBar actionBar;

    private EditText name;
    private EditText phone;
    private LinearLayout address;
    private RelativeLayout delivery;

    private TextView personsCountField;
    private TextView changeField;
    private TextView commentsField;
    private EditText timeForText;

    private int orderSumm;
    private TextView agreementLink;
    private CheckBox agreementCheck;
    private Button orderButton;

    private RadioButton payCourier;
    private RadioButton paySber;

    @Override
    protected void onResume() {
        super.onResume();
        EatApplication.dataManager.registerOrderRequestsSubscriber((OrderRequests) this);
        setOrderButtonEnabled(false);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EatApplication.dataManager.registerOrderRequestsSubscriber(null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();

        actionBar.setTitle("Оформление заказа");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        final Settings settings = EatApplication.dataManager.getSettings();

        name = (EditText) findViewById(R.id.name_edit_text);
        phone = (EditText) findViewById(R.id.phone_edit_text);
        FormatWatcher phoneFormatWatcher = EatApplication.appUtils.setPhoneMask(phone);
        address = (LinearLayout) findViewById(R.id.address);
        final Spinner spinner = (Spinner) findViewById(R.id.city_spinner);
        final EditText street = (EditText) findViewById(R.id.street_edit_text);
        final EditText house = (EditText) findViewById(R.id.adress_house_edit_text);
        final EditText porch = (EditText) findViewById(R.id.adress_porch_edit_text);
        final EditText floor = (EditText) findViewById(R.id.adress_floor_edit_text);
        final EditText room = (EditText) findViewById(R.id.room_edit_text);
        final EditText intercron = (EditText) findViewById(R.id.intercrom_edit_text);
        RadioGroup pay = (RadioGroup) findViewById(R.id.pay_radio);
        payCourier = (RadioButton) findViewById(R.id.pay_courier);
        paySber = (RadioButton) findViewById(R.id.pay_sber);
        TextView timeForLabel = (TextView) findViewById(R.id.time_for_label);
        timeForText = (EditText) findViewById(R.id.time_for_edit_text);
        FormatWatcher timeFormatWatcher = EatApplication.appUtils.setTimeMask(timeForText);
        TextView minusPerson = (TextView) findViewById(R.id.coun_person_minus);
        personsCountField = (TextView) findViewById(R.id.coun_person_text_viev);
        changeField = (EditText) findViewById(R.id.sdacha);
        commentsField = (EditText) findViewById(R.id.comments);
        TextView plusPerson = (TextView) findViewById(R.id.coun_person_plus);
        TextView summ = (TextView) findViewById(R.id.summ);
        delivery = (RelativeLayout) findViewById(R.id.delivery);
        TextView deliverySumm = (TextView) findViewById(R.id.delivery_summ);
        TextView deliveryZone = (TextView) findViewById(R.id.delivery_zone);


        agreementLink = (TextView) findViewById(R.id.agreement_link);
        agreementCheck = (CheckBox) findViewById(R.id.agreement_check);
        orderButton = (Button) findViewById(R.id.save_button);

        final Order currentOrder = EatApplication.dataManager.getCurrentOrder();
        orderSumm = EatApplication.dataManager.getSummOrder(currentOrder);

        name.setText(settings.getPersonalName());
        phone.setText(settings.getPersonalPhone());

        switch (currentOrder.getType()) {
            case 0:
                timeForLabel.setText("Доставить к");
                break;
            case 1:
                timeForLabel.setText("Заберу в");
                break;
            case 2:
                timeForLabel.setText("Подать к");
                break;
        }

        if (currentOrder.getType() != 0) {
            addressVisibilityGone();
        } else {
            EatApplication.appUtils.setSpinner(getBaseContext(), spinner, settings.getPersonalDistrict());
            street.setText(settings.getPersonalStreet());
            house.setText(settings.getPersonalHouse());
            porch.setText(settings.getPersonalPorch());
            floor.setText(settings.getPersonalFloor());
            room.setText(settings.getPersonalRoom());
            intercron.setText(settings.getPersonalIntecrom());
        }

        (findViewById(R.id.pay_courier)).setVisibility(currentOrder.getPlace().isCourierPayment() ? View.VISIBLE : View.GONE);
        (findViewById(R.id.pay_sber)).setVisibility(currentOrder.getPlace().isSberPayment() ? View.VISIBLE : View.GONE);

        timeFormatWatcher.setCallback(new FormattedTextChangeListener() {
            @Override
            public boolean beforeFormatting(String oldValue, String newValue) {
                return false;
            }

            @Override
            public void onTextFormatted(FormatWatcher formatter, String newFormattedText) {
                if (newFormattedText.length() == 5) {

                    String currentTimeFormatted = EatApplication.appUtils.getCurrentTimeFormatted("HH:mm");
                    int currentHour = getHoursOnText(currentTimeFormatted);
                    int currentMinute = getMinutesOnText(currentTimeFormatted);

                    int userHour = getHoursOnText(newFormattedText.toString());
                    if (userHour > 23) {
                        userHour = 23;
                    }
                    if (userHour < currentHour) {
                        userHour = currentHour;
                    }
                    int userMinute = getMinutesOnText(newFormattedText.toString());
                    if (userMinute > 59) {
                        userMinute = 59;
                    }
                    if (userHour <= currentHour && userMinute < currentMinute) {
                        userMinute = currentMinute;
                    }
                    timeForText.setText(((userHour > 9) ? String.valueOf(userHour) : "0" + String.valueOf(userHour)) + ":" + ((userMinute > 9) ? String.valueOf(userMinute) : "0" + String.valueOf(userMinute)));
                }
            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                EatApplication.realm.beginTransaction();
                settings.setPersonalDistrict(spinner.getSelectedItem().toString());
                EatApplication.realm.commitTransaction();
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });

        minusPerson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = Integer.parseInt(personsCountField.getText().toString());
                if (count > 1) {
                    personsCountField.setText(String.valueOf(--count));
                }
            }
        });

        plusPerson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = Integer.parseInt(personsCountField.getText().toString());
                if (count < 99) {
                    personsCountField.setText(String.valueOf(++count));
                }
            }
        });

        summ.setText(orderSumm + " " + Const.PRICE_UNITS);
        final Place place = currentOrder.getPlace();
        String deliveryPrice = "бесплатно";
        if (place.getDprice() > 0) {
            deliveryPrice = "от "+ place.getDprice() + Const.PRICE_UNITS;
        } else {
            if (place.getDfrom() > orderSumm) {
                deliveryPrice = "бесплатно от "+ place.getDfrom() + Const.PRICE_UNITS;
            }
        }
        deliverySumm.setText("Доставка: " + deliveryPrice);

        deliveryZone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent openDeliveryZone = new Intent(getApplicationContext(), DeliveryZone.class);
                startActivity(openDeliveryZone);
            }
        });

        if (orderSumm < place.getMorder()) {
            setOrderButtonEnabled(false);
        }

        agreementCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                setOrderButtonEnabled(isChecked);
            }
        });

        agreementLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAgreement();
            }
        });

        orderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (name.getText().length() < 1) {
                    Toast toast = Toast.makeText(CheckoutActivity.this, "Укажите имя", Toast.LENGTH_SHORT);
                    toast.show();
                    name.requestFocus();
                } else if (phone.getText().length() < 18) {
                    Toast toast = Toast.makeText(CheckoutActivity.this, "Укажите телефон", Toast.LENGTH_SHORT);
                    toast.show();
                    phone.requestFocus();
                } else {
                    EatApplication.realm.beginTransaction();
                    settings.setPersonalName(name.getText().toString());
                    settings.setPersonalPhone(EatApplication.appUtils.getPhoneUnformatted(phone.getText().toString()).substring(1));
                    settings.setPersonalStreet(street.getText().toString());
                    settings.setPersonalHouse(house.getText().toString());
                    settings.setPersonalPorch(porch.getText().toString());
                    settings.setPersonalFloor(floor.getText().toString());
                    settings.setPersonalRoom(room.getText().toString());
                    settings.setPersonalIntecrom(intercron.getText().toString());
                    currentOrder.setDate(EatApplication.appUtils.getCurrentTimeFormatted("dd.MM.yyyy HH:mm"));
                    EatApplication.realm.commitTransaction();

                    setOrderButtonEnabled(false);

                    requestCheckingCode();
                }
            }
        });

    }

    private void requestCheckingCode() {
        EatApplication.dataManager.requestCheckingCode(new DataManager.RequestInterface() {
            @Override
            public void onSuccess() {

            }
            @Override
            public void onFailed() {
                showNoConnectionDialog(CheckoutActivity.this, new NoConnectionRepeatInterface() {
                    @Override
                    public void repeatPressed() {
                        requestCheckingCode();
                    }
                });
            }
        });
    }

    private void showAgreement() {
        Intent intent = new Intent(this, NavTextPage.class);
        intent.putExtra("title", "Пользовательское соглашение");
        intent.putExtra("page_code", "agreement");
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    private void setOrderButtonEnabled (boolean enabled) {
        if (orderButton != null) {
            orderButton.setEnabled(enabled);
            orderButton.setBackgroundResource(enabled ? R.drawable.round_corner_enabled : R.drawable.round_corner_disabled);
        }
    }

    private void addressVisibilityGone() {
        address.setVisibility(View.GONE);
        delivery.setVisibility(View.GONE);
    }

    @Override
    public void requestCheckingCodeResult(String resendDelay, String resendCurrent) {
        Intent confirmationOrder = new Intent(getApplicationContext(), ConfirmationOrderActivity.class);
        confirmationOrder.putExtra("resendDelay", resendDelay);
        confirmationOrder.putExtra("resendCurrent", resendCurrent);

        int currentTimestamp = (int) (System.currentTimeMillis() / 1000L);
        int checkoutTimestamp = currentTimestamp;

        if (timeForText.getText().length() == 5) {

            String currentTimeFormatted = EatApplication.appUtils.getCurrentTimeFormatted("HH:mm");
            int currentHour = getHoursOnText(currentTimeFormatted);
            int currentMinute = getMinutesOnText(currentTimeFormatted);

            int userHour = getHoursOnText(timeForText.getText().toString());
            if (userHour > 23) {
                userHour = 23;
            }

            int userMinute = getMinutesOnText(timeForText.getText().toString());
            if (userMinute > 59) {
                userMinute = 59;
            }

            if (userHour > currentHour) {
                checkoutTimestamp += (userHour - currentHour - 1) * 3600;
                checkoutTimestamp += ((60 - currentMinute) + userMinute) * 60;
            } else if (userHour == currentHour && userMinute > currentMinute) {
                checkoutTimestamp += (userMinute - currentMinute) * 60;
            }
        }

        String paymentType = "0";
        if (payCourier.isChecked()) {
            paymentType = "1";
        }
        if (paySber.isChecked()) {
            paymentType = "2";
        }

        // Конеце проверки времени
        confirmationOrder.putExtra("checkoutTime", String.valueOf(checkoutTimestamp));
        confirmationOrder.putExtra("checkoutComments", commentsField.getText().toString());
        confirmationOrder.putExtra("checkoutPersons", personsCountField.getText().toString());
        confirmationOrder.putExtra("checkoutChange", changeField.getText().toString());
        confirmationOrder.putExtra("checkoutPayment", paymentType);

        startActivity(confirmationOrder);
    }

    @Override
    public void checkCodeResult(String result) {

    }

    private int getHoursOnText(String formattedTime) {
        return Integer.parseInt(formattedTime.substring(0, 2));
    }

    private int getMinutesOnText(String formattedTime) {
        return Integer.parseInt(formattedTime.substring(3, 5));
    }

}
