package biz.ibschool.eatonline.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import biz.ibschool.eatonline.Adapters.BaseAdapter.PlacesListAdapter;
import biz.ibschool.eatonline.EatApplication;
import biz.ibschool.eatonline.Interfaces.FavouritePressed;
import biz.ibschool.eatonline.Models.Dish;
import biz.ibschool.eatonline.Models.DishType;
import biz.ibschool.eatonline.Models.Place;
import biz.ibschool.eatonline.R;

import java.util.ArrayList;

public class DishTypePlaces extends AppCompatActivity implements FavouritePressed {

    ActionBar actionBar;
    private PlacesListAdapter placesListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_places_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();

        Intent intent = getIntent();
        final int dishTypeId = intent.getIntExtra("dishTypeId", 0);
        DishType dishType = EatApplication.realm.where(DishType.class).equalTo("id", dishTypeId).findFirst();

        actionBar.setTitle(dishType.getName());
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        ListView placesListView;
        final ArrayList arrayList;

        placesListView = (ListView) findViewById(R.id.placesListView);

        arrayList = new ArrayList(EatApplication.realm.where(Place.class).equalTo("dishTypes.id", dishTypeId).findAll());
        placesListAdapter = new PlacesListAdapter(getApplicationContext(), arrayList, (FavouritePressed) this);
        placesListView.setAdapter(placesListAdapter);

        placesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Place place = (Place) arrayList.get(position);
                Intent intent = new Intent(getApplicationContext(), PlaceActivity.class);
                intent.putExtra("placeId", place.getId());
                intent.putExtra("startTypeId", dishTypeId);
                startActivity(intent);

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void switchPlaceFavourite(Place place) {
        EatApplication.dataManager.setPlaceToFavourite(place, !place.isFavourite());
        placesListAdapter.notifyDataSetChanged();
    }

    @Override
    public void switchDishFavourite(Dish dish) {

    }
}
