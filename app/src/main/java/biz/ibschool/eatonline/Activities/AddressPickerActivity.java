package biz.ibschool.eatonline.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.webkit.WebView;
import android.widget.ListView;
import android.widget.SearchView;

import biz.ibschool.eatonline.AppUtils;
import biz.ibschool.eatonline.EatApplication;
import biz.ibschool.eatonline.Interfaces.Const;
import biz.ibschool.eatonline.Models.Place;
import biz.ibschool.eatonline.R;

public class AddressPickerActivity extends AppCompatActivity {

    private int idPlace;
    private Place place;
    private String currentAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_picker);

        Intent intent = getIntent(); // Передача переменной
        idPlace = intent.getIntExtra("placeId", 0);
        currentAddress = intent.getStringExtra("currentAddress");
        place = EatApplication.realm.where(Place.class).equalTo("id", idPlace).findFirst();


        WebView webView = (WebView) findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getViewTreeObserver().addOnGlobalLayoutListener(new WebGlobalListenerClass());


        final SearchView searchView = (SearchView) findViewById(R.id.search_view);
        final ListView addressesListView = (ListView) findViewById(R.id.results_list);

        searchView.setFocusable(true);
        searchView.setIconified(false);
        searchView.requestFocusFromTouch();
        searchView.setQueryHint("Ваш адрес");

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                onBackPressed();
                return false;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                searchWithString(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                searchWithString(s);
                return false;
            }
        });

        /*searchListAdapter = new SearchListAdapter(this, this);
        placesAndDishesListView.setAdapter(searchListAdapter);

        placesAndDishesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Intent intent = new Intent(getApplicationContext(), PlaceActivity.class);
                if (searchListAdapter.itemIsPlace(i)) {
                    Place place = places.get(i);
                    intent.putExtra("placeId", place.getId());
                } else {
                    Dish dish = dishes.get(i - places.size());
                    intent.putExtra("placeId", dish.getPlace().getId());
                    intent.putExtra("startDishId", dish.getId());
                }
                startActivity(intent);
            }
        });*/

    }

    private void searchWithString(String searchString) {
        /*if (searchString.length() > 2) {
            places = EatApplication.dataManager.searchPlacesBy(searchString);
            dishes = EatApplication.dataManager.searchDishesBy(searchString);
            searchListAdapter.updateData(places, dishes);
            searchListAdapter.notifyDataSetChanged();
        } else {
            searchListAdapter.updateData(new ArrayList<Place>(), new ArrayList<Dish>());
            searchListAdapter.notifyDataSetChanged();
        }*/
    }

    class WebGlobalListenerClass implements ViewTreeObserver.OnGlobalLayoutListener {
        @Override
        public void onGlobalLayout() {
            WebView webView = (WebView) findViewById(R.id.webView);
            ViewGroup.LayoutParams params = webView.getLayoutParams();
            String w =  Integer.toString(EatApplication.appUtils.pixelsToDp((float) webView.getWidth()));
            String h =  Integer.toString(EatApplication.appUtils.pixelsToDp((float) webView.getHeight()));
            //show ImageView width and height


            String url = Const.NEW_API_URL + "zones.php?place=" + place.getId() + "&width=" + w + "&height=" +  h;

            System.out.println(url);

            webView.loadUrl(url);

        }
    }

}
