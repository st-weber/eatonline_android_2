package biz.ibschool.eatonline.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import biz.ibschool.eatonline.EatApplication;
import biz.ibschool.eatonline.R;


public class NavPersonals extends AppCompatActivity {

    private ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nav_personals);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();

        actionBar.setTitle("Личные данные");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        Button buttonSafe = (Button) findViewById(R.id.save_button);
        final TextView name = (TextView) findViewById(R.id.name_edit_text);
        final EditText phone = (EditText) findViewById(R.id.phone_edit_text);
        EatApplication.appUtils.setPhoneMask(phone);
        final Spinner spinner = (Spinner) findViewById(R.id.city_spinner);
        final EditText street = (EditText) findViewById(R.id.street_edit_text);
        final EditText home = (EditText) findViewById(R.id.home_edit_text);
        final EditText porch = (EditText) findViewById(R.id.porch_edit_text);
        final EditText floor = (EditText) findViewById(R.id.floor_edit_text);
        final EditText room = (EditText) findViewById(R.id.room_edit_text);
        final EditText intecrom = (EditText) findViewById(R.id.intercrom_edit_text);
        final Switch pushSwitch = (Switch) findViewById(R.id.push_switch);

        // Вывод из бд на активити
        name.setText(EatApplication.dataManager.getSettings().getPersonalName());
        if (EatApplication.dataManager.getSettings().getPersonalPhone() != null)
            phone.setText(EatApplication.dataManager.getSettings().getPersonalPhone());
        street.setText(EatApplication.dataManager.getSettings().getPersonalStreet());
        home.setText(EatApplication.dataManager.getSettings().getPersonalHouse());
        porch.setText(EatApplication.dataManager.getSettings().getPersonalPorch());
        floor.setText(EatApplication.dataManager.getSettings().getPersonalFloor());
        room.setText(EatApplication.dataManager.getSettings().getPersonalRoom());
        intecrom.setText(EatApplication.dataManager.getSettings().getPersonalIntecrom());

        EatApplication.appUtils.setSpinner(getBaseContext(), spinner, EatApplication.dataManager.getSettings().getPersonalDistrict());

        buttonSafe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EatApplication.realm.beginTransaction();
                EatApplication.dataManager.getSettings().setPersonalName(name.getText().toString());
                EatApplication.dataManager.getSettings().setPersonalPhone(EatApplication.appUtils.getPhoneUnformatted(phone.getText().toString()).substring(1));
                if (spinner.getSelectedItem() != null) {
                    String selected = spinner.getSelectedItem().toString();
                    EatApplication.dataManager.getSettings().setPersonalDistrict(selected);
                }
                EatApplication.dataManager.getSettings().setPersonalStreet(street.getText().toString());
                EatApplication.dataManager.getSettings().setPersonalHouse(home.getText().toString());
                EatApplication.dataManager.getSettings().setPersonalPorch(porch.getText().toString());
                EatApplication.dataManager.getSettings().setPersonalFloor(floor.getText().toString());
                EatApplication.dataManager.getSettings().setPersonalRoom(room.getText().toString());
                EatApplication.dataManager.getSettings().setPersonalIntecrom(intecrom.getText().toString());
                EatApplication.realm.commitTransaction();
                Toast toast = Toast.makeText(NavPersonals.this, "Сохранено", Toast.LENGTH_SHORT);
                toast.show();

                closeActivity();

            }
        });

        pushSwitch.setChecked(EatApplication.dataManager.getSettings().isReceiveNotifications());
        pushSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                EatApplication.realm.beginTransaction();
                EatApplication.dataManager.getSettings().setReceiveNotifications(isChecked);
                EatApplication.realm.commitTransaction();
                if (EatApplication.dataManager.getSettings().isReceiveNotifications()) {

                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    private void closeActivity() {
        Intent main = new Intent(this, MainActivity.class);
        main.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(main);
    }

}
