package biz.ibschool.eatonline.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import biz.ibschool.eatonline.ApiInteractor;
import biz.ibschool.eatonline.Interfaces.LoadingTextInterface;
import biz.ibschool.eatonline.R;

public class NavTextPage extends AppCompatActivity {

    ActionBar actionBar;

    private ProgressBar loadingIndicator;
    private TextView pageText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();

        Intent intent = getIntent();
        String pageTitle = intent.getStringExtra("title");
        String pageCode = intent.getStringExtra("page_code");

        loadingIndicator = (ProgressBar) findViewById(R.id.progressBarAgreement);
        loadingIndicator.setVisibility(View.VISIBLE);

        actionBar.setTitle(pageTitle);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        pageText = (TextView) findViewById(R.id.pageText);
        pageText.setVisibility(View.GONE);

        ApiInteractor apiInteractor = new ApiInteractor();
        apiInteractor.loadPageText(pageCode, new LoadingTextInterface() {
            @Override
            public void onLoaded(String text) {
                loadingIndicator.setVisibility(View.GONE);
                pageText.setText(Html.fromHtml(text));
                pageText.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

}
