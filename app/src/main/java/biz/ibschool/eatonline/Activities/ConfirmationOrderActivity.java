package biz.ibschool.eatonline.Activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import biz.ibschool.eatonline.DataManager;
import biz.ibschool.eatonline.EatApplication;
import biz.ibschool.eatonline.Interfaces.OrderRequests;
import biz.ibschool.eatonline.R;

public class ConfirmationOrderActivity extends EatActivity implements OrderRequests {

    private Toolbar toolbar;
    private ActionBar actionBar;
    private CountDownTimer timer;

    private EditText codeField;
    private TextView infoText;
    private Button resendButton;
    Button checkCodeButton;

    private ProgressBar progressIndicator;
    private TextView orderConfirmedText;

    private String checkoutTime = "";
    private String checkoutComments = "";
    private String checkoutPersons = "";
    private String checkoutChange = "";
    private String checkoutPayment = "0";

    @Override
    protected void onResume() {
        super.onResume();
        EatApplication.dataManager.registerOrderRequestsSubscriber((OrderRequests) this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EatApplication.dataManager.registerOrderRequestsSubscriber(null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation_order);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();

        actionBar.setTitle("Подтверждение заказа");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        codeField = (EditText) findViewById(R.id.verification_code);
        infoText = (TextView) findViewById(R.id.phone_text_view);
        infoText.setText(infoText.getText() + EatApplication.appUtils.getPhoneFormattedString(EatApplication.dataManager.getSettings().getPersonalPhone()));
        resendButton = (Button) findViewById(R.id.resent_button);
        setResentEnabled(false);
        checkCodeButton = (Button) findViewById(R.id.check_code);


        progressIndicator = (ProgressBar) findViewById(R.id.progress_indicator);
        orderConfirmedText = (TextView) findViewById(R.id.order_confirmed);
        Typeface dukeTypeface = Typeface.createFromAsset(getApplicationContext().getAssets(), "duke.ttf");
        if (dukeTypeface != null) {
            orderConfirmedText.setTypeface(dukeTypeface);
        }
        progressIndicator.setVisibility(View.GONE);
        orderConfirmedText.setVisibility(View.GONE);

        EatApplication.appUtils.setVerificationCode(codeField);

        resendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestCheckingCode();
                setResentEnabled(false);
            }
        });

        checkCodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkCode(codeField.getText().toString());
            }
        });

        Intent intent = getIntent();
        String resendDelay = intent.getStringExtra("resendDelay");
        String resendCurrent = intent.getStringExtra("resendCurrent");

        checkoutTime = intent.getStringExtra("checkoutTime");
        checkoutComments = intent.getStringExtra("checkoutComments");
        checkoutPersons = intent.getStringExtra("checkoutPersons");
        checkoutChange = intent.getStringExtra("checkoutChange");
        checkoutPayment = intent.getStringExtra("checkoutPayment");

        timer = new CountDownTimer((Integer.parseInt(resendDelay) - Integer.parseInt(resendCurrent)) * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
            }
            @Override
            public void onFinish() {
                setResentEnabled(true);
            }
        }.start();

    }

    private void requestCheckingCode() {
        EatApplication.dataManager.requestCheckingCode(new DataManager.RequestInterface() {
            @Override
            public void onSuccess() {

            }
            @Override
            public void onFailed() {
                showNoConnectionDialog(ConfirmationOrderActivity.this, new NoConnectionRepeatInterface() {
                    @Override
                    public void repeatPressed() {
                        requestCheckingCode();
                    }
                });
            }
        });
    }

    private void checkCode(final String code) {
        EatApplication.dataManager.checkCode(code, new DataManager.RequestInterface() {
            @Override
            public void onSuccess() {

            }
            @Override
            public void onFailed() {
                showNoConnectionDialog(ConfirmationOrderActivity.this, new NoConnectionRepeatInterface() {
                    @Override
                    public void repeatPressed() {
                        checkCode(code);
                    }
                });
            }
        });
    }

    protected void onDestroy()
    {
        super.onDestroy();
        timer.cancel();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void requestCheckingCodeResult(String resendDelay, String resendCurrent) {
        timer = new CountDownTimer((Integer.parseInt(resendDelay) - Integer.parseInt(resendCurrent)) * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
            }
            @Override
            public void onFinish() {
                setResentEnabled(true);
            }
        }.start();
    }

    @Override
    public void checkCodeResult(String result) {
        if (result.equals("wrong")) {
            infoText.setText("Не верный код");
        } else {
            toolbar.setVisibility(View.GONE);
            progressIndicator.setVisibility(View.VISIBLE);
            codeField.setVisibility(View.GONE);
            codeField.clearFocus();
            infoText.setVisibility(View.GONE);
            resendButton.setVisibility(View.GONE);
            checkCodeButton.setVisibility(View.GONE);
            checkoutOrder(result, checkoutTime, checkoutComments, checkoutPersons, checkoutChange, checkoutPayment);
        }
    }

    private void checkoutOrder(final String checkoutToken, final String time, final String comments, final String persons, final String change, final String payment) {

        EatApplication.dataManager.checkoutOrder(checkoutToken, time, comments, persons, change, payment, new DataManager.RequestInterface() {
            @Override
            public void onSuccess() {

                timer.cancel();
                timer = new CountDownTimer(1 * 1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                    }
                    @Override
                    public void onFinish() {
                        progressIndicator.setVisibility(View.GONE);
                        orderConfirmedText.setVisibility(View.VISIBLE);
                        new CountDownTimer(1 * 1000, 1000) {
                            @Override
                            public void onTick(long millisUntilFinished) {
                            }
                            @Override
                            public void onFinish() {
                                closeActivity();
                            }
                        }.start();
                    }
                }.start();
            }
            @Override
            public void onFailed() {
                showNoConnectionDialog(ConfirmationOrderActivity.this, new NoConnectionRepeatInterface() {
                    @Override
                    public void repeatPressed() {
                        checkoutOrder(checkoutToken, time, comments, persons, change, payment);
                    }
                });
            }
        });

    }

    private void setResentEnabled(boolean enabled) {
        resendButton.setEnabled(enabled);
        resendButton.setBackgroundResource(enabled ? R.drawable.round_corner_enabled : R.drawable.round_corner_disabled);
    }

    private void closeActivity() {
        Intent mainActivity = new Intent(this, MainActivity.class);
        Intent orders = new Intent(this, NavOrders.class);
        mainActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        TaskStackBuilder
                .create(this)
                .addNextIntent(mainActivity)
                .addNextIntentWithParentStack(orders)
                .startActivities();
    }

}
