package biz.ibschool.eatonline.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;


/**
 * Created by michanic on 07.10.17.
 */

public class EatActivity extends AppCompatActivity {

    protected interface NoConnectionRepeatInterface {
        void repeatPressed();
    }

    protected void showNoConnectionDialog(Context context, final NoConnectionRepeatInterface noConnectionRepeatInterface) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setTitle("Ошибка");
        alertDialogBuilder.setMessage("Отсутствует соединение с сервером");

        alertDialogBuilder.setPositiveButton("Повторить", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                noConnectionRepeatInterface.repeatPressed();
            }
        });
        /*alertDialogBuilder.setNegativeButton("Повторить", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });*/
        AlertDialog alertDialog = (AlertDialog) alertDialogBuilder.create();
        alertDialog.show();
    }

}
