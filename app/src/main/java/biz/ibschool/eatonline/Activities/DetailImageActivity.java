package biz.ibschool.eatonline.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import biz.ibschool.eatonline.Interfaces.Const;
import biz.ibschool.eatonline.R;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * Created by michanic on 15.09.17.
 */

public class DetailImageActivity extends AppCompatActivity {

    private ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_image);

        final Intent intent = getIntent(); // Передача переменной
        String imagePath = intent.getStringExtra("imagePath");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setTitle(intent.getStringExtra("pageTitle"));
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        final ProgressBar imageProgress = (ProgressBar) findViewById(R.id.dish_image_progress);
        PhotoView photoView = (PhotoView) findViewById(R.id.photo_view);
        TextView toPlaceButton = (TextView) findViewById(R.id.to_place_button);

        final int placeId = intent.getIntExtra("placeId", 0);

        if (placeId != 0) {
            toPlaceButton.setVisibility(View.VISIBLE);
            toPlaceButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent placeIntent = new Intent(getApplicationContext(), PlaceActivity.class);
                    placeIntent.putExtra("placeId", placeId);
                    placeIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getApplicationContext().startActivity(placeIntent);
                }
            });

        } else {
            toPlaceButton.setVisibility(View.GONE);
        }

        imageProgress.getIndeterminateDrawable().setColorFilter(
                Color.WHITE, android.graphics.PorterDuff.Mode.SRC_IN);
        imageProgress.setVisibility(View.VISIBLE);

        Picasso.with(getApplicationContext())
                .load(Const.DOMAIN_NAME + imagePath)
                .into(photoView, new Callback() {
                    @Override
                    public void onSuccess() {
                        imageProgress.setVisibility(View.INVISIBLE);
                    }
                    @Override
                    public void onError() {
                        imageProgress.setVisibility(View.INVISIBLE);
                    }
                });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

}
