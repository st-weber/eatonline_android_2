package biz.ibschool.eatonline.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;

import biz.ibschool.eatonline.Adapters.BaseAdapter.SearchListAdapter;
import biz.ibschool.eatonline.EatApplication;
import biz.ibschool.eatonline.Interfaces.FavouritePressed;
import biz.ibschool.eatonline.Models.Dish;
import biz.ibschool.eatonline.Models.Place;
import biz.ibschool.eatonline.R;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity implements FavouritePressed {

    private ArrayList<Place> places;
    private ArrayList<Dish> dishes;
    private SearchListAdapter searchListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        final SearchView searchView = (SearchView) findViewById(R.id.search_view);
        final ListView placesAndDishesListView = (ListView) findViewById(R.id.results_list);

        searchView.setFocusable(true);
        searchView.setIconified(false);
        searchView.requestFocusFromTouch();

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                onBackPressed();
                return false;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                searchWithString(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                searchWithString(s);
                return false;
            }
        });

        searchListAdapter = new SearchListAdapter(this, this);
        placesAndDishesListView.setAdapter(searchListAdapter);

        placesAndDishesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Intent intent = new Intent(getApplicationContext(), PlaceActivity.class);
                if (searchListAdapter.itemIsPlace(i)) {
                    Place place = places.get(i);
                    intent.putExtra("placeId", place.getId());
                } else {
                    Dish dish = dishes.get(i - places.size());
                    intent.putExtra("placeId", dish.getPlace().getId());
                    intent.putExtra("startDishId", dish.getId());
                }
                startActivity(intent);
            }
        });

    }

    private void searchWithString(String searchString) {
        if (searchString.length() > 2) {
            places = EatApplication.dataManager.searchPlacesBy(searchString);
            dishes = EatApplication.dataManager.searchDishesBy(searchString);
            searchListAdapter.updateData(places, dishes);
            searchListAdapter.notifyDataSetChanged();
        } else {
            searchListAdapter.updateData(new ArrayList<Place>(), new ArrayList<Dish>());
            searchListAdapter.notifyDataSetChanged();
        }
    }


    @Override
    public void switchPlaceFavourite(Place place) {
        EatApplication.dataManager.setPlaceToFavourite(place, !place.isFavourite());
        searchListAdapter.notifyDataSetChanged();
    }

    @Override
    public void switchDishFavourite(Dish dish) {
        EatApplication.dataManager.setDishToFavourite(dish, !dish.isFavourite());
        searchListAdapter.notifyDataSetChanged();
    }
}
