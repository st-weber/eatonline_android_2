package biz.ibschool.eatonline.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import biz.ibschool.eatonline.DataManager;
import biz.ibschool.eatonline.EatApplication;
import biz.ibschool.eatonline.Interfaces.Const;
import biz.ibschool.eatonline.R;

public class SelectTable extends AppCompatActivity implements DataManager.BasketCountCallback {

    private ActionBar actionBar;
    private int placeID;

    public class SchemeWebViewClient extends WebViewClient {

        @Override
        public void onLoadResource (WebView view, String url) {
            if (url.contains("eatonline.ru/t")) {

                view.stopLoading();
                int tableId = Integer.parseInt(url.substring(url.lastIndexOf("t")+1));
                Intent detailTable = new Intent(getApplicationContext(), DetailTablesActivity.class);
                detailTable.putExtra("tableId", tableId);
                detailTable.putExtra("placeId", placeID);
                startActivity(detailTable);

            }
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            view.loadUrl(Const.DOMAIN_NAME);
            return false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        EatApplication.dataManager.registerBasketCountListener(this);
        invalidateOptionsMenu();
    }

    @Override
    protected void onPause() {
        super.onPause();
        EatApplication.dataManager.registerBasketCountListener(null);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_table);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();

        actionBar.setTitle("Выбор столика");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        Intent intent = getIntent(); // Передача переменной
        placeID = intent.getIntExtra("placeID", 0);

        String url = Const.API_URL + "places.php?req=scheme&place=" + placeID+"&add_domain=true";
        WebView webView = (WebView) findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.setWebViewClient(new SchemeWebViewClient());

        webView.loadUrl(url);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.basket_menu, menu);

        MenuItem menuItem = menu.findItem(R.id.basket_icon);
        menuItem.setIcon(EatApplication.dataManager.buildCounterDrawable(false));

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.basket_icon) {
            if (EatApplication.dataManager.getCurrentOrder().getPlace() != null) {
                Intent basketActivity = new Intent(this, BasketActivity.class);
                startActivity(basketActivity);
            }
        }

        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void basketCountChanged() {
        invalidateOptionsMenu();
    }
}
