package biz.ibschool.eatonline.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import biz.ibschool.eatonline.Adapters.BaseAdapter.CitiesListAdapter;
import biz.ibschool.eatonline.ApiInteractor;
import biz.ibschool.eatonline.DataManager;
import biz.ibschool.eatonline.EatApplication;
import biz.ibschool.eatonline.Interfaces.LoadingTextInterface;
import biz.ibschool.eatonline.Models.City;
import biz.ibschool.eatonline.Models.Place;
import biz.ibschool.eatonline.R;

import java.util.ArrayList;

public class StartScreen extends EatActivity {

    private ListView citiesListView;
    private TextView selectCityTextView;
    private ProgressBar progressIndicator;
    private ProgressBar loadingIndicator;
    private TextView loadingStatusTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_screen);

        EatApplication.setAppContext(getApplicationContext());

        progressIndicator = (ProgressBar) findViewById(R.id.progressIndicator);
        progressIndicator.setVisibility(View.GONE);

        citiesListView = (ListView) findViewById(R.id.citiesListView);
        citiesListView.setVisibility(View.GONE);

        selectCityTextView = (TextView) findViewById(R.id.selectCityTextView);
        selectCityTextView.setVisibility(View.GONE);

        loadingIndicator = (ProgressBar) findViewById(R.id.loadingIndicator);
        loadingIndicator.setVisibility(View.GONE);

        loadingStatusTextView = (TextView) findViewById(R.id.loadingStatusTextView);
        loadingStatusTextView.setVisibility(View.GONE);

        if (EatApplication.dataManager.getSettings().getCurrentCity() != null) {
            //System.out.println("city " + EatApplication.dataManager.getSettings().getCurrentCity().getName() + " already selected, load data");
            loadSettings();

            progressIndicator.setVisibility(View.GONE);
            loadingIndicator.setVisibility(View.VISIBLE);
            loadingStatusTextView.setVisibility(View.VISIBLE);

            citiesListView.setVisibility(View.GONE);
        } else {
            progressIndicator.setVisibility(View.VISIBLE);
            loadCities();
        }

        citiesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id) {
                citiesListView.setVisibility(View.GONE);
                selectCityTextView.setVisibility(View.GONE);
                loadingIndicator.setVisibility(View.VISIBLE);
                loadingStatusTextView.setVisibility(View.VISIBLE);

                ArrayList<City> cities = EatApplication.dataManager.sort(EatApplication.realm.where(City.class).findAll());
                EatApplication.dataManager.getSettings().setCurrentCity(cities.get(position));
                //System.out.println("set city " + EatApplication.dataManager.getSettings().getCurrentCity().getName() + ", load data");
                loadSettings();

            }
        });
    }

    private void loadSettings() {
        ApiInteractor apiInteractor = new ApiInteractor();
        apiInteractor.loadSettings(new DataManager.RequestInterface() {
            @Override
            public void onSuccess() {
                loadDishTypes();
            }

            @Override
            public void onFailed() {
                showNoConnectionDialog(StartScreen.this, new NoConnectionRepeatInterface() {
                    @Override
                    public void repeatPressed() {
                        loadSettings();
                    }
                });
            }
        });
    }

    private void loadCities() {
        EatApplication.dataManager.loadCities(new DataManager.RequestInterface() {
            @Override
            public void onSuccess() {
                citiesListView.setVisibility(View.VISIBLE);
                selectCityTextView.setVisibility(View.VISIBLE);
                progressIndicator.setVisibility(View.GONE);

                // Города загружены в базу, выводим их в таблицу
                CitiesListAdapter citiesListAdapter;
                citiesListAdapter = new CitiesListAdapter(getApplicationContext(), EatApplication.dataManager.sort(EatApplication.realm.where(City.class).findAll()));
                citiesListView.setAdapter(citiesListAdapter);

            }
            @Override
            public void onFailed() {
                showNoConnectionDialog(StartScreen.this, new NoConnectionRepeatInterface() {
                    @Override
                    public void repeatPressed() {
                        loadCities();
                    }
                });
            }
        });
    }

    private void loadDishTypes() {
        EatApplication.dataManager.loadDishTypes(new DataManager.RequestInterface() {
            @Override
            public void onSuccess() {
                loadPlacesList();
            }

            @Override
            public void onFailed() {
                showNoConnectionDialog(StartScreen.this, new NoConnectionRepeatInterface() {
                    @Override
                    public void repeatPressed() {
                        loadDishTypes();
                    }
                });
            }
        });
    }

    private void loadPlacesList() {
        EatApplication.dataManager.loadPlacesList(new DataManager.RequestInterface() {
            @Override
            public void onSuccess() {
                loadActions();
            }
            @Override
            public void onFailed() {
                showNoConnectionDialog(StartScreen.this, new NoConnectionRepeatInterface() {
                    @Override
                    public void repeatPressed() {
                        loadPlacesList();
                    }
                });
            }
        });
    }

    private void loadActions() {
        EatApplication.dataManager.loadActions(new DataManager.RequestInterface() {
            @Override
            public void onSuccess() {
                loadPlacesInfo();
            }

            @Override
            public void onFailed() {
                showNoConnectionDialog(StartScreen.this, new NoConnectionRepeatInterface() {
                    @Override
                    public void repeatPressed() {
                        loadActions();
                    }
                });
            }
        });

    }

    private void loadPlacesInfo() {

        final int placesCount = EatApplication.dataManager.getPlacesCount();
        loadingIndicator.setMax(placesCount);
        loadingIndicator.setProgress(0);

        EatApplication.dataManager.loadPlacesInfo(new DataManager.RequestInterface() {
            @Override
            public void onSuccess() {

                    Intent mainActivity = new Intent(getApplicationContext(), MainActivity.class);
                    mainActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(mainActivity);

            }

            @Override
            public void onFailed() {
                showNoConnectionDialog(StartScreen.this, new NoConnectionRepeatInterface() {
                    @Override
                    public void repeatPressed() {
                        loadPlacesInfo();
                    }
                });
            }
        }, new DataManager.PlacesLoadingInterface() {
            @Override
            public void placeInfoLoaded(Place place, int placeNumber) {
                loadingStatusTextView.setText(place.getTitle() + " " + (placeNumber + 1) + " из " + placesCount);
                loadingIndicator.setProgress(placeNumber + 1);
            }
        });

    }

}
